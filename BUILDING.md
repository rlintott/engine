## Build Instructions
**Requires CMake 3.8+ and Python 3+ to build. Stable versions are in the master branch.**
Recommend Visual Studio 2019 or Visual Studio Code with CMake Tools extension.

**Update:** In addition to the following steps, building for Linux and Windows requires some external dependencies:
- Windows: Need to install libsodium using [vcpkg](https://github.com/Microsoft/vcpkg). Make sure to pass their toolchain file to CMake.
- Linux: Need libsodium and mbedtls using system package manager. If using Ubuntu, see script in tools folder.
- macOS (Untested): Requires same depedencies but will need minor modifications to CMakeLists.txt. 

1. Get the [Vulkan SDK](https://vulkan.lunarg.com/).
2. Create project repository for your app.
3. Inside that repository, ```git submodule add https://gitlab.com/magtoolkit/engine.git```.
4. Inside your project (top-level) repository, create a `modules` folder and run ```git submodule add https://gitlab.com/magtoolkit/base.git```. Pull and initialize submodules with ```git submodule update --init --recursive```. Switch to master branch if your project requires it with ```git checkout master```.
5. When building [shaderc](https://github.com/google/shaderc), you will need to download dependencies with `python ./engine/external/shaderc/utils/git-sync-deps`.
6. When configuring your debugger, set your working directory to the folder containing `modules`, as the binaries access this folder with a relative path.
7. Build and run the `AssetTool` target to prepare the included assets. It should generate .mag files inside `modules/base`.
8. Example Usage:
CMakeLists.txt (goes into project root folder)
```cmake
cmake_minimum_required (VERSION 3.8)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
project(Game VERSION 0.0.1 DESCRIPTION "A game.")

add_subdirectory(engine)

add_executable(game src/main.cpp)
target_link_libraries(game mag)
```


```cpp
/*
	MyGame.h
*/

#pragma once
#include <mag/Game.h>

class MyGame : public mag::Game {
public:
	/*
		Override virtual methods.
		Default engine systems are automatically initialized.
	*/
};

```

```cpp
/*
	main.cpp
*/
#include <iostream>
#include <mag/App.h>
#include "MyGame.h"

int main() {
	
	mag::log::setLevel(mag::log::Level::Debug);
	mag::App<MyGame>().run();

    return 0;
}
```

## Documentation

This project uses doxygen for documentation. It can also be built with an alternate theme (recommended).

### Standard
```
cd engine
doxygen
```

### Alternate
```
pip install jinja2 Pygments
cd engine
python external/m.css/documentation/doxygen.py Doxyfile-mcss
```
**Note: **On Linux, use `pip3` and `python3` instead.