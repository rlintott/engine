#pragma once
#include "Asset.h"
#include <memory>
#include <string>
//#include <MaterialDesigner/MaterialDesigner.h>
namespace mag {

	//struct MeshImportData {
	//	std::shared_ptr<AMesh> meshAsset;
	//	std:
	//};

	class MeshImporter {
		public:
			std::shared_ptr<AssetPackage> LoadGLTF(std::string path);
			gfx::Material GetPBRMaterial();
			//MaterialDesigner materialDesigner;
	};
}
