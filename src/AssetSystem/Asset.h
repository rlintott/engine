#pragma once
#include <memory>
#include <unordered_map>
#include <string>



#include <mag/gfx/Material.h>
//#include <RenderSystem/GFX/Texture.h>
#include <mag/types/Byte.h>
#include "Version.h"
#include <mag/gfx/Mesh.h>
namespace mag {

	/*
	Game Asset File Package Format. 
	Corresponds to one asset file, which may contain multiple assets (ex textures, materials in gltf)
	*/
	class AssetPackage {
	public:

		std::unordered_map<std::string, gfx::Mesh> meshes;
		std::unordered_map<std::string, gfx::Material> materials;
		std::unordered_map<std::string, gfx::MaterialInstanceCreateInfo> material_instances;
		std::unordered_map<std::string, gfx::Texture2DAsset> texture2D;

		void merge(std::shared_ptr<AssetPackage> package) {
			if (!package->meshes.empty()) {
				meshes.merge(package->meshes);
			}
			if (!package->materials.empty()) {
				materials.merge(package->materials);
			}
			if (!package->material_instances.empty()) {
				material_instances.merge(package->material_instances);
			}
			if (!package->texture2D.empty()) {
				texture2D.merge(package->texture2D);
			}
		}

		//template<class Archive>
		//void serialize(Archive & ar)
		//{
		//	ar(meshes, materials, material_instances, texture2D);
		//}
	};
}