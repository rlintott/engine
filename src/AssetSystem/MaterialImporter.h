#pragma once
#include <mag/gfx/Material.h>
#include <memory>
#include <string>
#include <filesystem>
//#include <AssetSystem/ShaderImporter.h>
namespace mag {
	/*
		Read .material file. Import appropriate shaders. Fill in the blanks/override when necessary.
	*/
	class AssetPackage;
	class MaterialImporter {
	public:
		//ShaderImporter shaderImporter;

		std::shared_ptr<AssetPackage> Load(std::string material_file, std::string name, std::filesystem::path path);
		std::shared_ptr<AssetPackage> LoadInstance(std::string instance_file);
	};
}