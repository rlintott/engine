#pragma once

namespace mag {
/*
Semantic versioning class, move somewhere else later.
*/
	class Version {
	public:
		unsigned int major, minor, patch;
	};
}