#include "MaterialImporter.h"
#include <fstream>
#include <sstream>
//#include <nlohmann/json.hpp>
#include <filesystem>
#include "Asset.h"
#include <shaderc/shaderc.hpp>
#include <spirv_cross.hpp>
#include <cereal/archives/json.hpp>
//#include <Serialization/gfx.h>
//#include <RenderSystem/GFX/Mesh.h>
#include <Serialization/Material.h>

namespace mag {
	std::shared_ptr<AssetPackage> MaterialImporter::Load(std::string material_file_path, std::string name, std::filesystem::path path)
	{
		std::shared_ptr<AssetPackage> package(new AssetPackage());

		gfx::Material material;
		gfx::MaterialInstanceCreateInfo mici;
		gfx::ShaderInfo shader;

		material.name = name;

		std::ifstream material_file(material_file_path, std::ifstream::binary);
		std::stringstream m_buffer;

		// first we read the files into buffers
		if (material_file.is_open()) {
			m_buffer << material_file.rdbuf();
		}
		else {
			throw(std::runtime_error("Could not find material shader file!"));
		}
		material_file.close();



		// convert buffer to c style string ( char *)
		// actually the glShaderSource can take an array of pointers to strings (char ** I believe)
		// which is why we send the address, however for now we are only compiling one type of shader at a time
		// warning! doing this in one line causes it to randomly fail because str() is temporary! :(((((
		auto m_str = m_buffer.str();

		// using shaderc, convert glsl to spirv
		// Compilation
		shaderc::Compiler compiler;

		shaderc::CompileOptions vertOptions;
		vertOptions.AddMacroDefinition("VERTEX");

		shaderc::CompileOptions fragOptions;
		fragOptions.AddMacroDefinition("FRAGMENT");

		auto vertexResult = compiler.CompileGlslToSpv(m_str, shaderc_glsl_vertex_shader, material_file_path.c_str(), vertOptions); // according to the docs, input file name is used in case of errors, does not actually need to be a file name
		auto fragmentResult = compiler.CompileGlslToSpv(m_str, shaderc_glsl_fragment_shader, material_file_path.c_str(), fragOptions);

		std::vector<std::vector<uint32_t>> spirv_code;
		std::vector<uint32_t> spirv;
		spirv.assign(vertexResult.begin(), vertexResult.end());
		spirv_code.push_back(spirv);

		spirv.assign(fragmentResult.begin(), fragmentResult.end());
		spirv_code.push_back(spirv);


		// Reflection
		unsigned int i = 0;
		for (auto & code : spirv_code) {
			//std::vector<uint32_t> spirv;
			//spirv.assign(code.begin(), code.end());
			spirv_cross::Compiler comp(code);

			auto resources = comp.get_shader_resources();

			// generate texture 2d inputs (non-array)
			for (auto & res : resources.sampled_images) {
				auto type = comp.get_type(res.type_id);
				//std::cout << type.array.size(); // 1, because it's one dimension.
				if (type.vecsize == 1) {
					// if non-array, save to material definition
					unsigned int binding = comp.get_decoration(res.id, spv::DecorationBinding);
					material.textureSlots[res.name] = binding;
					mici.texture2D[res.name] = "";

					// shader
					shader.textures[res.name] = binding;
				}
			}

			// uniform buffers
			for (auto & res : resources.uniform_buffers) {
				auto binding = comp.get_decoration(res.id, spv::DecorationBinding);
				shader.uniformBuffers[res.name].binding = binding;
				shader.uniformBuffers[res.name].size = comp.get_declared_struct_size(comp.get_type(res.base_type_id));
				//comp.get_declared_struct_size();
			}

			// samplers
			for (auto & res : resources.separate_samplers) {
				auto binding = comp.get_decoration(res.id, spv::DecorationBinding);
				shader.samplers[res.name] = binding;
			}

			// vertices
			if (i == 0) {
				for (auto & res : resources.stage_inputs) {
					auto location = comp.get_decoration(res.id, spv::DecorationLocation);
					if (res.name == "Position") {
						shader.vertexAttributes[gfx::VertexAttribute::Position] = location;
					}
					else if (res.name == "Normal") {
						shader.vertexAttributes[gfx::VertexAttribute::Normal] = location;
					}
					else if (res.name == "UV0") {
						shader.vertexAttributes[gfx::VertexAttribute::UV0] = location;
					}
					else if (res.name == "Tangent") {
						shader.vertexAttributes[gfx::VertexAttribute::Tangent] = location;
					}
					else if (res.name == "Transform") {
						shader.vertexAttributes[gfx::VertexAttribute::Transform] = location;
					}
				}
			}
			
			i++;
		}

		shader.vertCode.assign(vertexResult.begin(), vertexResult.end());
		shader.fragCode.assign(fragmentResult.begin(), fragmentResult.end());

		material.shaderInfo = shader;

		material.castShadows = true;
		material.receiveShadows = true;

		package->materials[material.name] = material;

		// material instance config

		mici.name = material.name + "_default_instance";

		// get module material path
		auto path2 = path;
		path2.replace_extension(".mag");
		std::string module_path = path2.generic_string();//material_file_path;
		//module_path.replace("\\", "/");
		auto pos = module_path.find("modules/");
		pos += 7;
		module_path = module_path.substr(pos + 1); // , module_path.size() - pos);

		//pos = module_path.find(".glsl");
		mici.material = material.name;
		mici.package = module_path;
		// write to json file
		
		std::ofstream ofs;
		auto newfilepath = path.parent_path().generic_string() + "/" + mici.name + ".mici";
		ofs.open(newfilepath, std::ofstream::binary);
		{
			cereal::JSONOutputArchive oarchive(ofs);
			oarchive(mici);
		}
		ofs.close();

		return package;
	}
	std::shared_ptr<AssetPackage> MaterialImporter::LoadInstance(std::string instance_file)
	{
		std::shared_ptr<AssetPackage> package(new AssetPackage());

		gfx::MaterialInstanceCreateInfo mici;

		// read json file
		std::ifstream ifs;
		ifs.open(instance_file, std::ifstream::binary);
		//std::stringstream buffer;
		//buffer << ifs.rdbuf();
		if (!ifs.is_open()) {
			throw std::runtime_error("\nReading .mici file failed!");
		}
		cereal::JSONInputArchive iarchive(ifs);
		iarchive(mici);
		ifs.close();

		package->material_instances[mici.name] = mici;

		return package;
	}
}