#pragma once
#include <memory>
#include <string>
namespace mag {
	class AssetPackage;
	class ImageImporter {
	public:
		std::shared_ptr<AssetPackage> Load(std::string path, std::string name);
	};
}