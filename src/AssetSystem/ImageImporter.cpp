#include "ImageImporter.h"
#include "Asset.h"
//#ifndef STBI_IMAGE_IMPLEMENTATION
//#define STB_IMAGE_IMPLEMENTATION
//#endif
#include <stb/stb_image.h>
namespace mag {
	std::shared_ptr<AssetPackage> ImageImporter::Load(std::string path, std::string name)
	{
		std::shared_ptr<AssetPackage> package(new AssetPackage());
		int x, y, channels;
		auto pixels = stbi_load_16(path.c_str(), &x, &y, &channels, 0);

		gfx::Texture2DAsset tex;
		int byteDepth = 2; // 16 bits per channel
		size_t imgSize = x * y*channels * byteDepth;
		tex.data.assign((mag::byte*)pixels, (mag::byte*)pixels + imgSize);
		tex.height = y;
		tex.width = x;
		tex.name = name;
		tex.numChannels = channels;
		package->texture2D[name] = tex;

		free(pixels);


		return package;
	}
}