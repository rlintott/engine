#include "MeshImporter.h"
#define TINYGLTF_IMPLEMENTATION
#ifndef STBI_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif
#ifndef STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#endif
#include <tiny_gltf.h>
#include <optional>
//#include <math/Vector.h>
#include <Serialization/gfx.h>
namespace mag {
	std::shared_ptr<AssetPackage> MeshImporter::LoadGLTF(std::string path)
	{
		tinygltf::Model model;
		tinygltf::TinyGLTF loader;
		std::string err;
		std::string warn;
		std::vector<gfx::Mesh> meshes;

		bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, path);

		if (!err.empty() || !ret) {
			throw std::runtime_error("Failed to load gltf model: " + path);
		}

		// process model into common engine mesh asset structure (hierarchical)
		if (model.scenes.size() == 0) {
			return nullptr;
		}


		for (auto & mesh : model.meshes) {

			gfx::Mesh eMesh;
			std::cout << "\nMesh: " << mesh.name;
			std::vector<gfx::MeshPrimitive> primitives;
			for (auto & prim : mesh.primitives) {
				// force standard vertex structure for pipeline reusability between different meshes when you want to use a material from another model
				gfx::MeshPrimitive mPrim;


				// index buffer
				auto ibufferView = model.bufferViews[model.accessors[prim.indices].bufferView];
				gfx::DataType indexType = gfx::uint;
				std::vector<mag::byte> indices;
				size_t indexSize = 0;
				auto itype = model.accessors[prim.indices].componentType;
				if (itype == 5121) { // unsigned byte
					// need to convert to uint16_t
					indexSize = sizeof(unsigned char);
					indexType = gfx::byte;
				}
				else if (itype == 5123) {
					indexSize = sizeof(uint16_t);
					indexType = gfx::ushort;
				}
				else if (itype == 5125) {
					indexSize = sizeof(uint32_t);
					indexType = gfx::uint;
				}
				else {
					throw std::runtime_error("Invalid index buffer type found in this mesh!");
				}

				size_t start = ibufferView.byteOffset;
				size_t length = 0;

				for (length = 0; length < ibufferView.byteLength; length += sizeof(mag::byte)) {
					if (indexType == gfx::byte) {
						uint16_t index = *(model.buffers[ibufferView.buffer].data.data() + start + length);
						void * ptr = &index;
						indices.push_back( *static_cast<mag::byte*>(ptr));
						indices.push_back( *(static_cast<mag::byte*>(ptr) + 1 ) );
					}
					else {
						indices.push_back(*(model.buffers[ibufferView.buffer].data.data() + start + length));
					}
				}

				if (indexType == gfx::byte) {
					indexType = gfx::ushort;
				}

				mPrim.indices.Set(indexType, indices.data(), indices.size());
				//mPrim.indices.Set(gfx::uint, model.buffers[ibufferView.buffer].data.data() + ibufferView.byteOffset, ibufferView.byteLength);

				for (auto & attribute : prim.attributes) {
					// position
					auto index = attribute.second;
					auto accessor = model.accessors[index];
					// assume vec3
					auto bufferView = model.bufferViews[accessor.bufferView];
					// ignore all sparse accessors for now
					if (attribute.first == "POSITION") {
						//gfx::VertexAttribute position;
						mPrim.attributes[gfx::VertexAttribute::Position].Set(gfx::vec3, model.buffers[bufferView.buffer].data.data() + bufferView.byteOffset, bufferView.byteLength);
					}

					else if (attribute.first == "NORMAL") {
						mPrim.attributes[gfx::VertexAttribute::Normal].Set(gfx::vec3, model.buffers[bufferView.buffer].data.data() + bufferView.byteOffset, bufferView.byteLength);
					}
					else if (attribute.first == "TEXCOORD_0") {
						mPrim.attributes[gfx::VertexAttribute::UV0].Set(gfx::vec2, model.buffers[bufferView.buffer].data.data() + bufferView.byteOffset, bufferView.byteLength);
					}
					else if (attribute.first == "TANGENT") {
						mPrim.attributes[gfx::VertexAttribute::Tangent].Set(gfx::vec3, model.buffers[bufferView.buffer].data.data() + bufferView.byteOffset, bufferView.byteLength);

					}
					// however still allow custom attributes if necessary
				}
				primitives.push_back(mPrim);
			}
			eMesh.primitives = primitives;
			eMesh.name = mesh.name;
			meshes.push_back(eMesh);
		}


		/*
			Iterate through materials and package.
			We need to create a material instance for each material, and store it in the bundle.
			Materials will be an instance of either opaque or translucent pbr material.
			Create material template too?
		*/

		std::vector<gfx::Material> materials;

		for (auto & material : model.materials) {
			/*
				Create a material. Let function that loads mesh create a material instance when necessary.
			*/

			//gfx::Material mat;
			//mat.name = material.name;

			//gfx::MaterialInstance instance;
			//instance.name = "material_instance";


			////gfx::MaterialInstanceData data;

			//// for each material json property:

			//for (auto & prop : material.values) {
			//	if (prop.first == "pbrMetallicRoughness") {
			//		//for (auto & param: pr
			//		auto color = prop.second.ColorFactor();
			//		gfx::MaterialParameter pcol;
			//		pcol.name = "Diffuse";
			//		pcol.type = gfx::vec4;

			//		//pcol.name
			//		//math::vec3 color = prop.second.bool_value
			//	}
			//}


		}

		std::shared_ptr<AssetPackage> package(new AssetPackage());
		for (auto & mesh : meshes) {
			package->meshes[mesh.name] = mesh;
		}
		//return std::shared_ptr<AMesh>();
		return package;
	}

	gfx::Material MeshImporter::GetPBRMaterial() {
		gfx::Material material;
		//material.
		return material;
	}
}
