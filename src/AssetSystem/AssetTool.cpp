//#include "AssetDatabase.h"
#include <filesystem>
#include <iostream>
#include <sstream>
#include <fstream>
#include <cereal/archives/binary.hpp>
#include "MeshImporter.h"
#include "ImageImporter.h"
#include "MaterialImporter.h"
#include <Serialization/Asset.h>
/*
	Compiles into executable for processing raw assets into .myth files.
	For now is very dumb, creates/overwrites .myth files and creates/overwrites new database.
*/

//myth::ShaderImporter shaderImporter;
mag::MaterialImporter materialImporter;
mag::MeshImporter meshImporter;
mag::ImageImporter imageImporter;
//std::unordered_map
int main() {
	// load up / create database

	// for every file below root folder
	// prepare an asset
	// write asset to .myth file in same folder source asset file was found
	std::filesystem::path path("./modules/base/"); // rootFolder

	//path = std::filesystem::path("C:\\Projects\\mythscape\\engine\\data");

	std::cout << "\nMAG Engine Asset Tool";
	// std::filesystem::path testpath = std::filesystem::current_path();
	// std::cout << testpath;
	// return 0;
	// std::filesystem::path path = std::filesystem::current_path();

	if (std::filesystem::exists(path)) {
		std::cout << "\nFound data folder!";
	}
	else {
		std::cout << "\nDid not find data folder!";
		system("PAUSE");
		return 0;
	}

	//std::shared_ptr<myth::AssetDatabase> assetdb(new myth::AssetDatabase);
	std::unordered_map<std::string, std::shared_ptr<mag::AssetPackage>> packages;

	for (const auto & entry : std::filesystem::recursive_directory_iterator(path)) {
		std::cout << "\n" << entry.path();
		//if (entry.has_filename()) {
		//	std::cout << "\n" + entry.filename().string();
		//}

		/*
			For each raw asset file, import, convert to engine format, save, and add to database.
		*/

		if (entry.is_regular_file()) {
			auto fpath = std::filesystem::path(entry.path());
			//fpath.make_preferred();
			if (fpath.has_filename() && fpath.has_extension()) {
				// check if shader
				/*
				if (fpath.extension() == ".glsl") {
					auto material = shaderImporter.Load(fpath.stem().string(), fpath.string());
					if (!material) {
						continue;
					}
					// package
					//std::shared_ptr<myth::AssetPackage> package(new myth::AssetPackage);
					//package->assets.push_back(material);
					//std::stringstream ss;
					//std::ofstream ofs;
					auto newfilepath = fpath.parent_path().string() + "/" + fpath.stem().string() + ".myth";


					// search map of packages for key

					if (packages.find(newfilepath) == packages.end()) {
						packages[newfilepath] = myth::AssetPackage();
					}
					packages[newfilepath].assets.push_back(material);


					//ofs.open(newfilepath);
					//cereal::BinaryOutputArchive oarchive(ofs);
					//oarchive(package);
					//ofs.close();

					// add 


				}
				*/
				auto newfilepath = fpath.parent_path().string() + "/" + fpath.stem().string() + ".mag";
				std::shared_ptr<mag::AssetPackage> package(nullptr);
				//if (fpath.extension() == ".material") {
				//	//auto material = materialImporter.Load(fpath.string());
				//	//if (material) {
				//	//	// put inside asset package
				//	//// search map of packages for key
				//	//	auto newfilepath = fpath.parent_path().string() + "/" + fpath.stem().string() + ".myth";
				//	//	if (packages.find(newfilepath) == packages.end()) {
				//	//		packages[newfilepath] = myth::AssetPackage();
				//	//	}

				//	//	std::shared_ptr<myth::AMaterial> asset(new myth::AMaterial());
				//	//	asset->material = *material;
				//	//	packages[newfilepath].assets.push_back(asset);
				//	//}
				//}

				auto fstring = fpath.generic_string();
				auto fstem = fpath.stem().generic_string();

				if (fpath.extension() == ".glsl") {
					package = materialImporter.Load(fstring, fstem, fpath);
				}
				else if (fpath.extension() == ".mici") {
					package = materialImporter.LoadInstance(fstring);
				}
				else if (fpath.extension() == ".gltf") {
					package = meshImporter.LoadGLTF(fstring);
				}
				else if (fpath.extension() == ".png" || fpath.extension() == ".jpg") {
					package = imageImporter.Load(fstring, fstem);
				}

				if (package) {
					if (packages.find(newfilepath) != packages.end()) {
						packages[newfilepath]->merge(package);
					}
					else {
						packages[newfilepath] = package;
					}
				}
			}
		}
	}

	/*
		Write out all packages
	*/

	for (auto & package : packages) {

		std::ofstream ofs(package.first, std::ofstream::binary);
		cereal::BinaryOutputArchive oarchive(ofs);
		oarchive(package.second);
		ofs.close();
	}

	std::cout << "\nAsset processing complete." << "\n";

	//system("PAUSE");
	return 0;
}