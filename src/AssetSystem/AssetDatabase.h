#pragma once
#include  "Asset.h"
#include <unordered_map>
namespace mag {
	/*
		Record stored in asset index.
		Contains file name, content path, date, type
	*/
	class AssetRecord {
	public:
		Asset::AssetType type;
		std::string path; // path relative to root of the content folder
	};

	class AssetDatabase {
	public:
		std::unordered_map<std::string, AssetRecord> assets;
	};
}