#include <iostream>

#include <mag/Game.h>
#ifdef MAG_RENDERER_VULKAN
#include <mag/WindowSystemGLFW.h>
#endif

#include <mag/RenderSystem/RenderSystem.h>
#include <mag/InputSystem.h>
#include <mag/JobSystem.h>
#include <mag/EntitySystem.h>
// ECS
#include <Systems/TransformSystem.h>
#include <mag/components/Camera.h>
// Other 
#include <random>

#include <mag/log/Log.h>

namespace mag {

	Game::Game():
	job_system(new JobSystem()),
#ifdef MAG_RENDERER_VULKAN
	window_system(new WindowSystemGLFW()),
#endif
	input_system(new InputSystem(window_system.get())),
	entity_system(new EntitySystem()),
	render_system(new RenderSystem(job_system, window_system, entity_system)),
	transform_system(new TransformSystem(entity_system, job_system))

	{
		auto core_info = "Num Cores: " + std::to_string(std::thread::hardware_concurrency());
		log::info(core_info.c_str());
		log::info("Base client init!");
	}


	Game::~Game()
	{
		// other
		log::info("Base client deinit!");
	}

	void Game::engineFixedUpdate(FrameConstants frameConstants)
	{
		transform_system->FixedUpdate(frameConstants);
	}

	void Game::engineRenderUpdate(FrameConstants frameConstants)
	{
		if (input_system->Update()) {
			exit();
		}
		render_system->Update(frameConstants);
	}

	void Game::fixedUpdate(FrameConstants frameConstants) {}

	void Game::renderUpdate(FrameConstants frameConstants) {}

	void Game::setExitFunction(std::function<void()> function)
	{
		exit = function;
	}
}