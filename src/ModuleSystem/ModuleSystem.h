#pragma once
#include "Module.h"
#include <unordered_map>
#include  <string>
/*
	Modules are content packs. They add engine assets, and also new items, lore, and interactions. 
	Modules can be used for mods. The base game will be a module. Players will be allowed to pick and choose which 
	parts of each module will be incorporated into their game via some sort of template.
*/

namespace mag {
	class ModuleSystem {
	public:
}		std::unordered_map<std::strcat, Module>  modules;
		void StartUp();
		void ShutDown();
	};
}