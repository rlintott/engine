#include <mag/InputSystem.h>
// #include <InputSystem/KeyboardInterface.h>

#ifdef MAG_INPUT_GLFW
#include <InputSystem/KeyboardGLFW.h>
#include <InputSystem/MouseGLFW.h>
#endif

namespace mag {
	void InputSystem::resetActions()
	{
		for (auto it = actions.begin(); it != actions.end(); it++) {
			it.value().axis = 0.0f;
			it.value().button_state = input::ButtonState::Released;
		}
	}

	double InputSystem::getAxis(std::string action_name) 
	{
		auto it = action_names.find(action_name);
		if (it != action_names.end()) 
		{
			return actions[it.value()].axis;
		}
		return 0.0;
	}


	void InputSystem::evaluateKeyEvents()
	{
		// poll keyboard
		auto key_events = keyboard_interface->getKeyEvents();
		// convert key event to action using key bindings
		for (auto& key_event : key_events) {

			for (auto& binding : key_bindings[key_event.key]) {

				float axis = 0.0f;
				if ((key_event.button_state == input::ButtonState::Pressed || key_event.button_state == input::ButtonState::Held)) {
					if (binding.inverted) {
						axis = -1.0f;
					}
					else {
						axis = 1.0f;
					}
				}

				actions[binding.action] = { key_event.button_state, axis };
				break;
			}
		}
	}

	void InputSystem::evaluateMouseEvents()
	{
		// poll keyboard
		auto mouse_events = mouse_interface->getMouseEvents();
		// convert key event to action using key bindings
		for (auto& mouse_event : mouse_events) {

			for (auto& binding : mouse_button_bindings[mouse_event.button]) {

				float axis = 0.0f;
				if (mouse_event.button_state == input::ButtonState::Pressed || mouse_event.button_state == input::ButtonState::Held) {
					if (binding.inverted) {
						axis = -1.0f;
					}
					else {
						axis = 1.0f;
					}
				}
				actions[binding.action] = { mouse_event.button_state, axis };
				break;
			}
		}
	}

	input::ActionHandle InputSystem::getAction(std::string action_name)
	{
		auto it = action_names.find(action_name);
		if (it != action_names.end()) {
			return it.value();
		}

		auto handle = action_handles.get();
		action_names[action_name] = handle;
		return handle;

	}

	InputSystem::InputSystem(WindowSystem* window_system)
	{
		this->window_system = window_system;

#ifdef MAG_INPUT_GLFW
		keyboard_interface = std::unique_ptr<KeyboardInterface>(new KeyboardGLFW(window_system));
		mouse_interface = std::unique_ptr<MouseInterface>(new MouseGLFW(window_system));
#endif

		createKeyActionBinding("Exit", input::Key::Escape);
		createMouseButtonActionBinding("Mouse Action", input::MouseButton::RightMouse);
		createKeyActionBinding("Forward", input::Key::W);
		createKeyActionBinding("Forward", input::Key::S, true);
		createKeyActionBinding("Right", input::Key::D);
		createKeyActionBinding("Right", input::Key::A, true);
		// ReloadControls();

	}

	InputSystem::~InputSystem() {
		keyboard_interface.reset();
	}

	void InputSystem::ReloadControls()
	{
		actions.clear();
	}

	input::ButtonState InputSystem::getButtonState(std::string action_name) {
		// temporary code to refactor later
		auto it = action_names.find(action_name);
		if (it != action_names.end()) {
			auto action_it = actions.find(it.value());
			if (action_it != actions.end()) {
				return action_it.value().button_state;
			}
		}
		return input::ButtonState::Released;
	}

	mag::dvec2 InputSystem::getMousePosition()
	{
		return mag::dvec2(mouse_interface->getX(), mouse_interface->getY());
	}

	bool InputSystem::Update()
	{
		// put all actions to rest
		// resetActions();
		keyboard_interface->preUpdate();
		mouse_interface->preUpdate();

		// evaluate device events
		evaluateKeyEvents();
		evaluateMouseEvents();

		mouse_interface->postUpdate();
		keyboard_interface->postUpdate();

		if (getButtonState("Exit") == input::ButtonState::Pressed || getButtonState("Exit") == input::ButtonState::Held) {
			return true;
		}
		if (getButtonState("Mouse Action") == input::ButtonState::Held) {
			std::cout << "\nMouse Action fired!";
		}
		return false;
	}
	void InputSystem::createKeyActionBinding(std::string action_name, input::Key key, bool invert)
	{
		// check for existing action handle
		auto action = getAction(action_name);
		// bind key to action		
		auto action_binding = input::KeyBinding{ key, action, invert };
		key_bindings[key].push_back(action_binding);
	}

	void InputSystem::createMouseButtonActionBinding(std::string action_name, input::MouseButton mouse_button, bool invert)
	{
		auto action = getAction(action_name);
		mouse_button_bindings[mouse_button].push_back(input::MouseButtonBinding{ mouse_button, action, invert });
	}
}