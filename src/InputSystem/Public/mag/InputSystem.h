#include <memory>
#include <unordered_map>
#include <mag/input/KeyboardInterface.h>
#include <mag/input/MouseInterface.h>
#include <tsl/robin_map.h>
#include <mag/input/Input.h>
#include <mag/math/Vector.h>
#include <string>
namespace mag {

	class WindowSystem;
	// class KeyboardInterface;

	class InputSystem {
	private:
		WindowSystem* window_system;
		HandleManager<input::ActionHandle> action_handles;
		std::unique_ptr<KeyboardInterface> keyboard_interface;
		std::unique_ptr<MouseInterface> mouse_interface;
		tsl::robin_map<input::Key, std::vector<input::KeyBinding>> key_bindings;
		tsl::robin_map<input::MouseButton, std::vector<input::MouseButtonBinding>> mouse_button_bindings;
		tsl::robin_map<std::string, input::ActionHandle> action_names;
		tsl::robin_map<input::ActionHandle, input::Action, HandleHasher<input::ActionHandle>> actions;

		// private methods
		void resetActions();
		void evaluateKeyEvents();
		void evaluateMouseEvents();
		/*
			Checks if action associated with action_name already exists and returns the handle.
			If a handle does not exist, it creates the action.
		*/
		input::ActionHandle getAction(std::string action_name);
	public:
		InputSystem(WindowSystem* window_system);
		~InputSystem();
		void ReloadControls();
		bool Update();
		void createKeyActionBinding(std::string action_name, input::Key key, bool invert = false);
		void createMouseButtonActionBinding(std::string action_name, input::MouseButton mouse_button, bool invert = false);
		input::ButtonState getButtonState(std::string action_name);
		mag::dvec2 getMousePosition();
		double getAxis(std::string action_name);
	};
}