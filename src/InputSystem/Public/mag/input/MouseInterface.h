#pragma once
#include <vector>
#include <mag/input/Input.h>
namespace mag {
	class MouseInterface {
	public:
		virtual ~MouseInterface() {};
		virtual const std::vector<input::MouseEvent>& getMouseEvents() = 0;
		virtual void postUpdate() = 0;
		virtual void preUpdate() = 0;
		virtual double getX() = 0;
		virtual double getY() = 0;
	};
}