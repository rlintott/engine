#pragma once
#include <memory>
#include <vector>
#include <mag/input/Input.h>
namespace mag {

	class KeyboardInterface {
	public:

		virtual ~KeyboardInterface() {};
		virtual const std::vector<input::KeyEvent>& getKeyEvents() = 0;
		virtual void preUpdate() = 0;
		virtual void postUpdate() = 0;
	};


}