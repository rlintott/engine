#pragma once

namespace mag {
    namespace input {

        /*
            Mouse buttons used for input binding.
        */
        enum class MouseButton {
            LeftMouse,
            RightMouse,
            MiddleMouse,
			Unknown
        };

    }
}