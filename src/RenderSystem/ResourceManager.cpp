#include <mag/ResourceManager.h>
#include <cereal/archives/binary.hpp>
#include <memory>
#include <fstream>
#include <AssetSystem/Asset.h>
#include <mag/RenderSystem/RenderSystem.h>
#include <Serialization/Asset.h>

namespace mag {
	std::optional<gfx::Texture2DHandle> ResourceManager::loadTexture2D(std::string & package_name, std::string & texture_name)
	{
			/*
		Load file
			*/
		std::shared_ptr<AssetPackage> package;

		{
			std::string filename = "./modules/" + package_name;

			std::ifstream ifs(filename);

			if (!ifs.is_open()) {
				std::cout << "\nTexture2D " + texture_name + " file could not be opened at " + filename;
				return std::optional<gfx::Texture2DHandle>();
			}

			cereal::BinaryInputArchive archive(ifs);
			archive(package);
		}

		if (package->texture2D.find(texture_name) != package->texture2D.end()) {
			auto handle = texture2DHandles.get();
			if (!render_system.createTexture2D(handle, package->texture2D[texture_name])) {
				texture2DHandles.RecycleHandle(handle);
				return std::optional<gfx::Texture2DHandle>();
			}
			return std::optional<gfx::Texture2DHandle>(handle);

		}

		return std::optional<gfx::Texture2DHandle>();
	}
	ResourceManager::ResourceManager(RenderSystem& render_system): render_system(render_system)
	{
	}
}