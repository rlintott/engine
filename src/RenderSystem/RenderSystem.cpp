#include <mag/RenderSystem/RenderSystem.h>
#include <iostream>
#include <mag/JobSystem.h>
#include <mag/EntitySystem.h>
#include <mag/components/Transform.h>
#include <mag/components/MeshInstance.h>
#include <mag/components/Camera.h>
#include <mag/MaterialSystem.h>
#include <mag/JobGraph.h>
#include <mag/utility/Subrange.h>
#include <mag/log/Log.h>
#include <mag/math/Vector.h>
//#include <mag/math/Matrix.h>
#include <mag/math/Common.h>
#include <mag/WindowSystem.h>

#ifdef MAG_RENDERER_VULKAN
#include <RenderSystem/VK/Renderer_VK.h>
#endif

namespace mag {

	void RenderSystem::BuildDrawCommands(FrameConstants frameConstants, std::shared_ptr<JobGraph> & jobGraph)
	{
		/*
			Clear all previous draw command data
		*/
		auto clearFunc = [&]() {
			for (auto & it : workerMap) {
				it.second.drawCommands.clear();
				it.second.drawCommandMap.clear();
			}
		};

		auto clearJob = jobGraph->add(clearFunc);

		gfx::ResourceBinding res1;
		res1.binding = 0;
		res1.type = gfx::ResourceBinding::UniformBuffer;
		res1.uniformBuffer = frameConstantsBuffer;

		gfx::ResourceBinding res2;
		res2.binding = 1;
		res2.type = gfx::ResourceBinding::UniformBuffer;
		res2.uniformBuffer = viewBuffer;

		// spawn jobs
		auto view = entity_system->registry.view<Transform, MeshInstance>();

		auto work2 = [&] () {

			for (auto & it : workerMap) {
				gfx::DrawCommand cmd;

				auto threadID = it.first;
				auto & drawCommandMap = workerMap[threadID].drawCommandMap;
				auto & drawCommands = workerMap[threadID].drawCommands;

				// now sort out non-zero commands into worker-specific vector of draw commands
				for (auto it = drawCommandMap.begin(); it != drawCommandMap.end(); it++) {
					auto & drawInfo = it.value();
					if (drawInfo.command.count > 0) {
						// prepare transformation buffers (THIS CAN BE COMBINED WITH ABOVE JOBS IF THREAD SAFE TRANSFORMATION BUFFER CREATION CAN OCCUR (USE MUTEX? - WHAT ABOUT LOCK CONTENTION?)
						drawInfo.command.transform = renderer->setTransformData(drawInfo.transforms.data(), drawInfo.transforms.size() * sizeof(mat4));
						// copy into vector for each worker
						drawCommands.push_back(drawInfo.command);
					}
				}
			}
		};

		auto prepareDrawCommandsJob = jobGraph->add(work2);


		jobGraph->parallelFor(
			view.begin(),
			view.end(),
			[&, view, frameConstants, res1, res2](unsigned int& entity) {
				gfx::DrawCommand key;

				auto threadID = std::this_thread::get_id();
				auto& drawCommandMap = workerMap[threadID].drawCommandMap;

				auto& instance = view.get<MeshInstance>(entity);
				auto& transform = view.get<Transform>(entity);

				key.primitive = instance.primitive;
				key.pipeline = material_system.getPipeline(instance.materialInstance);
				key.resourceSetInitializer = material_system.getResourceSet(instance.materialInstance);

				// append buffers to resourceSetInitializer (we may have multiple cameras!)
				key.resourceSetInitializer.resources.push_back(res1);
				key.resourceSetInitializer.resources.push_back(res2);



				auto& drawInfo = drawCommandMap[key];
				drawInfo.command.count++;
				drawInfo.command.primitive = key.primitive;
				drawInfo.command.pipeline = key.pipeline;
				drawInfo.command.resourceSetInitializer = key.resourceSetInitializer;

				// ORDER: T*R*S
				dmat4 transformation(1.0);
				transformation = math::translate(transformation, glm::mix(transform.lastPosition, transform.position, frameConstants.alpha));
				transformation *= dmat4(math::slerp(transform.lastRotation, transform.rotation, frameConstants.alpha));
				transformation *= math::scale(transformation, glm::mix(transform.lastScale, transform.scale, frameConstants.alpha));

				drawInfo.transforms.push_back(transformation);
			},
			{ clearJob },
			{ prepareDrawCommandsJob }
		);

		auto startRecordingJob = jobGraph->add([&]{renderer->RecordingSetup(); });
		startRecordingJob->requires(prepareDrawCommandsJob);
		auto finishRecordingJob = jobGraph->add([&]() {renderer->RecordingFinish(); });

		for (auto & it : workerMap) {
			auto recordCommandJob = jobGraph->add([&] {renderer->RecordDrawCommands(it.second.drawCommands);});
			recordCommandJob->requires(startRecordingJob);
			finishRecordingJob->requires(recordCommandJob);
		}
	}

	RenderSystem::RenderSystem(std::shared_ptr<JobSystem> pJobSystem, std::shared_ptr<WindowSystem> pWindowSystem, std::shared_ptr<EntitySystem> pEntitySystem): 
		job_system(pJobSystem), windowSystem(pWindowSystem), entity_system(pEntitySystem), material_system(*this), mesh_system(*this), resource_manager(*this)
	{
		StartUp();
	}

	void RenderSystem::StartUp()
		
	{

		// Initialize the render system and its subsystems
		log::info("RenderSystem running!");


		auto window_result = windowSystem->createWindow(640, 480);
		if (window_result.ok()) {
			activeWindow = window_result.value();
			windowSystem->setTitle(window_result.value(), "MAG");
		}

		// initialize rendering backend (renderer)
		std::vector<std::thread::id> threadIDs;
		for (auto & worker : job_system->workGroup->workers) {
			workerMap[worker->getThreadID()] = {};
			threadIDs.push_back(worker->getThreadID());
		}
		workerMap[std::this_thread::get_id()] = {};
		threadIDs.push_back(std::this_thread::get_id());
		renderer = std::make_unique<Renderer>(threadIDs, activeWindow, windowSystem);


		viewBuffer = createUniformBuffer(sizeof(glm::mat4)).value();
		frameConstantsBuffer = createUniformBuffer(sizeof(glm::mat4)).value();

		width = 4;
		height = 3;
	}

	void RenderSystem::Update(FrameConstants frameConstants)
	{


		// update view buffer
		mag::dmat4 viewMatrix;
		mag::dmat4 projectionMatrix;

		auto view2 = entity_system->registry.view<Camera, Transform>();
		for (auto entity : view2) {
			auto & transform = view2.get<Transform>(entity);
			auto & camera = view2.get<Camera>(entity);
			constexpr auto fwd = mag::dvec3(0, 0, 1);
			mag::dvec3 lookDir = math::slerp(transform.lastRotation, transform.rotation, frameConstants.alpha)*fwd;
			viewMatrix = math::viewVulkan(math::lerp(transform.lastPosition, transform.position, frameConstants.alpha), lookDir);

			// this needs to be moved to the renderer - later add a function for generating a perspective matrix
			//projectionMatrix = math::perspectiveVulkan(glm::radians(90.0), static_cast<double>(width) / static_cast<double>(height), camera.farPlane, camera.nearPlane);
			projectionMatrix = math::InfinitePerspectiveVulkan(glm::radians(90.0), static_cast<double>(width) / static_cast<double>(height), camera.nearPlane);
			break;
		}
		glm::mat4 ProjectionView = static_cast<mag::mat4>(projectionMatrix) * static_cast<mag::mat4>(viewMatrix);
		renderer->setUniformBuffer(viewBuffer, math::get_ptr(ProjectionView));

		// prepare data for drawing
		drawCommands.clear();

		auto jobGraph = job_system->createGraph();

		BuildDrawCommands(frameConstants, jobGraph);

		//mag::log::info("Submitting Render Jobs");
		job_system->submit();
		jobGraph->wait();
		//mag::log::info("Render Jobs completed!");


		// attempt command buffers
		auto rs = renderer->Update();
		height = rs.height;
		width = rs.width;
	}

	PrimitiveResult<gfx::MeshPrimitiveHandle> RenderSystem::uploadPrimitive(const gfx::MeshPrimitive & primitive, std::optional<gfx::MeshPrimitiveHandle> opt_handle)
	{
		gfx::MeshPrimitiveHandle handle;
		//auto handle = opt_handle.value_or(meshPrimitiveHandles.get());
		if (opt_handle)
		{
			handle = opt_handle.value();
		}
		else
		{
			handle = meshPrimitiveHandles.get();
		}
		auto result = renderer->uploadPrimitive(handle,primitive);
		if (!result) {
			meshPrimitiveHandles.RecycleHandle(handle);
		}
		return result;
	}

	void RenderSystem::destroyPrimitive(gfx::MeshPrimitiveHandle handle)
	{
		renderer->destroyPrimitive(handle);
		meshPrimitiveHandles.RecycleHandle(handle);
	}

	bool RenderSystem::createGraphicsPipeline(gfx::GraphicsPipelineStateInitializer & initializer) {
		initializer.pipeline = graphicsPipelineHandles.get();
		if (!renderer->createGraphicsPipeline(initializer)) {
			graphicsPipelineHandles.RecycleHandle(initializer.pipeline);
			return false;
		}
		return true;
	}
	bool RenderSystem::createTexture2D(gfx::Texture2DHandle handle, gfx::Texture2DAsset & texture)
	{
		renderer->InsertTexture(handle, &texture);
		return true;
	}
	
	MaterialSystem& RenderSystem::materials()
	{
		return material_system;
	}
	
	MeshSystem& RenderSystem::meshes()
	{
		return mesh_system;
	}

	std::optional<gfx::UniformBufferHandle> RenderSystem::createUniformBuffer(size_t size)
	{
		auto handle = uniformBufferHandles.get();
		if (renderer->createUniformBuffer(handle, size)) {
			return std::optional<gfx::UniformBufferHandle>(handle);
		}
		uniformBufferHandles.RecycleHandle(handle);
		return std::optional<gfx::UniformBufferHandle>();
	}

	RenderSystem::~RenderSystem()
	{

	}

}