#include <memory>
#include <vector>
#include <unordered_map>
//#include <optional>
#include <mag/gfx/Texture.h>
#include <mag/gfx/Material.h>
#include <mag/gfx/Mesh.h>
#include <mag/gfx/DrawCommand.h>
#include <mag/containers/HandleManager.h>
#include <optional>
#include <thread>
#include <unordered_set>
#include <mag/math/Matrix.h>
#include <mag/FrameConstants.h>
#include <mag/types/Result.h>
#include <tsl/robin_map.h>
#include <mag/Window.h>
#include <mag/MaterialSystem.h>
#include <mag/MeshSystem.h>
#include <mag/ResourceManager.h>

namespace mag {

	class WindowSystem;
	class Renderer;
	class EntitySystem;
	class JobSystem;
	
	struct DrawInfo {
		gfx::DrawCommand command;
		std::vector<mat4> transforms;
	};
	
	struct WorkerData {
		std::vector<gfx::DrawCommand> drawCommands;
		tsl::robin_map<gfx::DrawCommand, DrawInfo, HandleHasher<gfx::DrawCommand>> drawCommandMap;
		std::vector<DrawInfo> drawInfo;
	};

	class JobGraph;



    class RenderSystem {
		private:

			// engine systems
			std::shared_ptr<JobSystem> job_system;
			std::shared_ptr<EntitySystem> entity_system;
			MaterialSystem material_system;
			MeshSystem mesh_system;
			ResourceManager resource_manager;
			std::unique_ptr<Renderer> renderer;

			std::unordered_map<std::string, gfx::Texture2DHandle> texture2D;
			std::unordered_map<std::thread::id, WorkerData> workerMap;

			// handles
			HandleManager<gfx::GraphicsPipelineHandle> graphicsPipelineHandles;
			HandleManager<gfx::UniformBufferHandle> uniformBufferHandles;
			HandleManager<gfx::TransformBufferHandle> transformBufferHandles;
			HandleManager<gfx::MeshPrimitiveHandle> meshPrimitiveHandles;
			HandleManager<WindowHandle> windowHandles;

			// will need to refactor soon after (to account for different render passes)
			int width, height;
			WindowHandle activeWindow;
			
			std::vector<gfx::DrawCommand> drawCommands;
			std::shared_ptr<WindowSystem> windowSystem;

			void BuildDrawCommands(FrameConstants frameConstants, std::shared_ptr<JobGraph> & jobGraph);
			void StartUp();
        public:

			RenderSystem::RenderSystem(std::shared_ptr<JobSystem> pJobSystem, std::shared_ptr<WindowSystem> pWindowSystem, std::shared_ptr<EntitySystem> pEntitySystem);
			~RenderSystem();

			// needs to be refactored so that draw commands and passes can be built externally
			void Update(FrameConstants frameConstants);
			PrimitiveResult<gfx::MeshPrimitiveHandle> uploadPrimitive(const gfx::MeshPrimitive& primitive, std::optional<gfx::MeshPrimitiveHandle> opt_handle = std::optional<gfx::MeshPrimitiveHandle>());
			void destroyPrimitive(gfx::MeshPrimitiveHandle handle);

			bool createGraphicsPipeline(gfx::GraphicsPipelineStateInitializer & initializer);
			bool createTexture2D(gfx::Texture2DHandle handle, gfx::Texture2DAsset & texture);
			MaterialSystem& materials();
			MeshSystem& meshes();
			std::optional<gfx::UniformBufferHandle> createUniformBuffer(size_t size);

			// Material System
			gfx::UniformBufferHandle frameConstantsBuffer;
			gfx::UniformBufferHandle viewBuffer;

	};
}