#pragma once
#include <mag/gfx/Material.h>
#include <mag/containers/HandleManager.h>
#include <mag/gfx/PipelineState.h>
#include <mag/gfx/ResourceSet.h>
#include <optional>
#include <tsl/robin_map.h>
namespace mag {

	class RenderSystem;

	class MaterialSystem {
	private:
		// handles
		HandleManager<gfx::MaterialHandle> materialHandles;
		HandleManager<gfx::MaterialInstanceHandle> instanceHandles;
		// Material members
		std::unordered_map<std::string, gfx::MaterialHandle> materialLookup;
		std::unordered_map<std::string, gfx::MaterialInstanceHandle> instanceLookup;
		std::unordered_map<gfx::MaterialHandle, gfx::Material, gfx::MaterialHandleHasher> materials;
		std::unordered_map<gfx::MaterialInstanceHandle, gfx::MaterialInstance, gfx::MaterialInstanceHandleHasher> materialInstances;
		tsl::robin_map<gfx::MaterialHandle, gfx::GraphicsPipelineHandle, gfx::MaterialHandleHasher> pipelineLookup;

		RenderSystem& render_system;

	public:
		MaterialSystem(RenderSystem& render_system);// std::shared_ptr<RenderSystem> pRenderSystem);
		std::optional<gfx::MaterialInstanceHandle> LoadMaterialInstance(std::string package_name, std::string instance_name);
		bool isMaterialLoaded(std::string & material_identifer);
		bool isInstanceLoaded(std::string & material_instance_identifier);
		bool LoadMaterial(std::string & package_name, std::string & material_name);
		gfx::GraphicsPipelineHandle getPipeline(gfx::MaterialInstanceHandle handle);
		gfx::ResourceSetInitializer getResourceSet(gfx::MaterialInstanceHandle handle);
	};
}