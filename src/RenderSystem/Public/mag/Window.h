#pragma once
#include <mag/types/Handle.h>
#include <mag/types/Result.h>
#include <mag/types/Error.h>
namespace mag {

    class WindowHandle: public Handle<> {};

	enum class WindowError { Success, NoWindowsExist, Failed};

    template <typename T>
	using WindowResult = result::Result<T, error::Status<WindowError>>;
}