#pragma once
#include <mag/components/MeshInstance.h>
namespace mag {
	class RenderSystem;
	class MeshRecord {
	public:
		std::unordered_map<std::string, std::vector<gfx::MeshPrimitiveHandle>> meshes;
	};

	class MeshSystem {
	private:
		RenderSystem& render_system;
	public:
		MeshSystem(RenderSystem& render_system);
		std::unordered_map<std::string, MeshRecord> records;

		/*
			Loads all meshes from asset file if second parameter is empty. Otherwise only loads primitives from specified mesh.
		*/
		void LoadMesh(std::string asset_name, std::string mesh_name = std::string());
	};
}