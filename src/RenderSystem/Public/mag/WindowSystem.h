#pragma once
#include <mag/Window.h>
#include <functional>
namespace mag {

    class WindowSystem {
        public:

        virtual WindowResult<WindowHandle> createWindow(int width, int height) = 0;
        virtual void destroyWindow(WindowHandle) = 0;
        virtual void setTitle(WindowHandle, std::string title) = 0;
        virtual WindowResult<WindowHandle> getActiveWindow() = 0;
		virtual size_t numWindows() const = 0;
		//virtual WindowHandle getFocusedWindow();
		virtual void registerCreateWindowCallback(std::function<void(WindowHandle)> callback) = 0;
    };
}