#pragma once
#include <unordered_map>
#include <optional>
#include <mag/containers/HandleManager.h>
#include <mag/gfx/Texture.h>
#include <string>
#include <memory>
namespace mag {
	class RenderSystem;
	class ResourceManager {
	private:
		HandleManager<gfx::Texture2DHandle> texture2DHandles;
		RenderSystem& render_system;
	public:
		std::unordered_map<std::string, gfx::Texture2DHandle> textureLookup;
		std::optional<gfx::Texture2DHandle> loadTexture2D(std::string & package_name, std::string & texture_name);
		ResourceManager(RenderSystem& render_system);
	};
}