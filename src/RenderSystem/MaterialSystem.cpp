#include <mag/MaterialSystem.h>
#include <fstream>
#include <cereal/archives/binary.hpp>
#include <AssetSystem/Asset.h>
#include <mag/RenderSystem/RenderSystem.h>
#include <cereal/archives/json.hpp>
#include <Serialization/Asset.h>
#include <mag/log/Log.h>
namespace mag {
	std::optional<gfx::MaterialInstanceHandle> MaterialSystem::LoadMaterialInstance(std::string package_name, std::string instance_name)
	{

		std::optional<gfx::MaterialInstanceHandle> result;

		/*
			If the material instance is already loaded, return the handle
		*/
		auto materialInstanceString = package_name + ":" + instance_name;
		if (isInstanceLoaded(materialInstanceString)) {
			const auto instance_handle = instanceLookup[materialInstanceString];
			return std::optional<gfx::MaterialInstanceHandle>(instance_handle);
		}

		/*
			Otherwise, load the material instance creation info data (mici)
		*/
		std::shared_ptr<AssetPackage> package;

		std::string filename = "./modules/" + package_name;
			std::ifstream ifs(filename, std::ifstream::binary);

			if (!ifs.is_open()) {
				mag::log::error("Material instance " + instance_name + " file could not be opened at {}", filename);
				return std::optional<gfx::MaterialInstanceHandle>();
			}

			{
				cereal::BinaryInputArchive archive(ifs);
				archive(package);
			}
		//}

		/*
			Check that the material instance is actually in this file.
		*/
		if (package->material_instances.find(instance_name) == package->material_instances.end()) {
			//return false;
			return result;
		}

		/*
			Read the mici data.
		*/
		gfx::MaterialInstanceCreateInfo mici = package->material_instances[instance_name];

		/*
			Check if the base material is loaded, and if not, load it
		*/
		auto materialID = mici.package + ":" + mici.material;
		if (!isMaterialLoaded(materialID)) {
			if (!LoadMaterial(mici.package, mici.material)) {
				//return false;
				return result;
			}
		}
		auto materialHandle = materialLookup[materialID];

		gfx::Material & material = materials[materialHandle];

		/*
			Now we need to create the resource bindings (descriptor sets) for the material instance.
			Create a ResourceSetInitializer.
		*/

		gfx::ResourceSetInitializer resInit;

		for (auto & it : mici.texture2D) {
			//resInit.textureBindings.push_back()
		}

		/*
			Create the buffer for material parameters
		*/
		for (auto & it : material.shaderInfo.uniformBuffers) {
			if (it.second.binding == 2) {
				auto ubHandle = render_system.createUniformBuffer(it.second.size);
				if (!ubHandle.has_value()) {
					//return false;
					return result;
				}
				gfx::ResourceBinding ubBinding;
				ubBinding.type = gfx::ResourceBinding::UniformBuffer;
				ubBinding.binding = it.second.binding;
				ubBinding.uniformBuffer = ubHandle.value();
				resInit.resources.push_back(ubBinding);
				break;
			}
		}

		resInit.pipeline = pipelineLookup[materialHandle];

		/*

		if (resInit.getResourceCount(gfx::ResourceBinding::Texture2D) != material.shaderInfo.textures.size()) {
			std::cout << "\nError: Material instance does not have same number of texture bindings as shader!";
			throw std::runtime_error("Instance vs Shader number of texture mismatch!");
		}

		if (resInit.getResourceCount(gfx::ResourceBinding::Sampler) != material.shaderInfo.samplers.size()) {
			std::cout << "\nError: Material instance does not have same number of samplers as shader!";
			throw std::runtime_error("Instance vs Shader number of samplers mismatch!");
		}

		if (resInit.getResourceCount(gfx::ResourceBinding::UniformBuffer) != material.shaderInfo.uniformBuffers.size()) {
			std::cout << "\nError: Material instance does not have same number of uniform buffers as shader!";
			throw std::runtime_error("Instance vs Shader number of uniform buffers mismatch!");
		}
		*/
		

		auto instanceHandle = instanceHandles.get();

		instanceLookup[package_name + ":" + instance_name] = instanceHandle;
		//instanceLookup[file_name] = instanceHandle;


		gfx::MaterialInstance instance;
		instance.name = mici.name;
		instance.materialHandle = materialHandle;
		instance.resourceSetInitializer = resInit;
		//instance.resourceSet = 
		materialInstances[instanceHandle] = instance;
		//return true;
		result = instanceHandle;
		return result;
	}
	bool MaterialSystem::isMaterialLoaded(std::string & material_identifer)
	{
		if (materialLookup.find(material_identifer) == materialLookup.end()) {
			return false;
		}
		return true;
	}
	bool MaterialSystem::isInstanceLoaded(std::string & material_instance_identifier)
	{
		if (instanceLookup.find(material_instance_identifier) == instanceLookup.end()) {
			return false;
		}
		return true;
	}
	bool MaterialSystem::LoadMaterial(std::string & package_name, std::string & material_name)
	{
		/*
			Load the material file.
		*/
		std::shared_ptr<AssetPackage> package;
		{
			std::string filename = "./modules/" + package_name;

			std::ifstream ifs(filename, std::ifstream::binary);

			if (!ifs.is_open()) {
				mag::log::error("Material file {} could not be opened at {}", material_name, filename);
				return false;
			}

			{
				cereal::BinaryInputArchive archive(ifs);
				archive(package);
			}
		}
		/* 
			Check if file contains the material we are looking for.
		*/
		auto it = package->materials.find(material_name);
		if (it == package->materials.end()) {
			return false;
		}

		/*
			Otherwise load material and create the graphics pipeline.
		*/
		gfx::Material & material = package->materials[material_name];

		gfx::GraphicsPipelineStateInitializer initializer;

		initializer.shader = material.shaderInfo;

		auto materialHandle = materialHandles.get();

		if (render_system.createGraphicsPipeline(initializer)) {
			pipelineLookup[materialHandle] = initializer.pipeline;
			materials[materialHandle] = material;
			return true;
		}
		return false;

	}

	gfx::GraphicsPipelineHandle MaterialSystem::getPipeline(gfx::MaterialInstanceHandle handle)
	{
		
		return pipelineLookup[materialInstances[handle].materialHandle];
	}

	gfx::ResourceSetInitializer MaterialSystem::getResourceSet(gfx::MaterialInstanceHandle handle)
	{
		return materialInstances[handle].resourceSetInitializer;
	}

	MaterialSystem::MaterialSystem(RenderSystem& render_system) : render_system(render_system) {};
}