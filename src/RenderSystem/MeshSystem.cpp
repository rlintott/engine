#include <mag/MeshSystem.h>
#include <fstream>
#include <cereal/archives/binary.hpp>
#include <AssetSystem/Asset.h>
#include <mag/RenderSystem/RenderSystem.h>
#include <memory>
#include <Serialization/Asset.h>
#include <mag/log/Log.h>

namespace mag {
	MeshSystem::MeshSystem(RenderSystem& render_system) : render_system(render_system)
	{
	}

	void MeshSystem::LoadMesh(std::string asset_name, std::string mesh_name)
	{


		std::string filename = "./modules/" + asset_name;

		std::ifstream ifs(filename, std::ifstream::binary);
		std::shared_ptr<AssetPackage> package;

		if (!ifs.is_open()) {
			mag::log::error("Mesh file could not be opened at {}", filename);
			return;
		}

		{
			cereal::BinaryInputArchive archive(ifs);
			archive(package);
		}

		if (!mesh_name.empty()) {
			if (package->meshes.find(mesh_name) != package->meshes.end()) {
				for (auto& primitive : package->meshes[mesh_name].primitives) {
					auto result = render_system.uploadPrimitive(primitive);
					if (result) {
						records[asset_name].meshes[mesh_name].push_back(result.value());
					}
				}
			}
		}

		else {
			for (const auto& it : package->meshes) {
				for (auto& primitive : package->meshes[it.first].primitives) {
					auto result = render_system.uploadPrimitive(primitive);
					if (result) {
						records[asset_name].meshes[it.first].push_back(result.value());
					}
				}
			}
		}


	}
}