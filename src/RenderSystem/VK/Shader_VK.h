#pragma once
#include <vulkan/vulkan.hpp>
class Shader {
public:
	std::vector<vk::PipelineShaderStageCreateInfo> shaderStages;
	std::vector<vk::ShaderModule> modules;
};