#pragma once
#include <vulkan/vulkan.hpp>
namespace mag {
	namespace gfx {
		class RenderPass {
		private:
			vk::RenderPass renderPass;
			vk::Device * device;
		public:
			//void prepare(vk::UniqueDevice & device, vk::Format & format);
			RenderPass(vk::UniqueDevice & device, vk::Format & format, vk::Format depthFormat);
			~RenderPass();
			//operator vk::RenderPass() const { return renderPass; };
			vk::RenderPass getRenderPass();
		};
	}
}