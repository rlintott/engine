#pragma once
#include <mag/gfx/Mesh.h>
#include <vulkan/vulkan.hpp>
#include <vk_mem_alloc.h>

namespace mag {
	namespace gfx {

		struct VertexOffset {
			size_t offset;
			size_t size;
		};

		class PrimitiveData {
		public:

			PrimitiveData() {
				layout = VertexLayout::NoneBit;
				hasIndices = false;
				indexType = vk::IndexType::eUint16;
				num_elements = 0;
				num_frames_since_deleted = 0;
			}

			vk::Buffer vertexBuffer;
			VmaAllocation vertexBufferAllocation;

			vk::Buffer indexBuffer;
			VmaAllocation indexBufferAllocation;
			vk::IndexType indexType;
			bool hasIndices;
			uint32_t num_elements;

			gfx::VertexLayout layout;

			// offsets
			std::unordered_map<VertexAttribute, VertexOffset> attribute;

			// for deleting
			uint32_t num_frames_since_deleted;

		};
	}
}