#include "Setup_VK.h"
#include <iostream>
#include <set>
#include <limits>
#include <mag/log/Log.h>
namespace mag {

		bool checkValidationLayerSupport(const std::vector<const char*> & validationLayers) {
		uint32_t layerCount;
		vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

		std::vector<VkLayerProperties> availableLayers(layerCount);
		vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

		for (const char* layerName : validationLayers) {
			bool layerFound = false;

			for (const auto& layerProperties : availableLayers) {
				if (strcmp(layerName, layerProperties.layerName) == 0) {
					layerFound = true;
					break;
				}
			}

			if (!layerFound) {
				return false;
			}
		}

		return true;
	}

	vk::Instance createInstance(bool enableValidationLayers, VkDebugUtilsMessengerEXT & debugMessenger) {



		std::vector<const char*> validationLayers = {
			"VK_LAYER_LUNARG_standard_validation"
		};

		if (enableValidationLayers && !checkValidationLayerSupport(validationLayers)) {
			throw std::runtime_error("Validation layers are not available!");
		}



		// create instance
		vk::ApplicationInfo appInfo("VulkanApp", VK_MAKE_VERSION(1, 0, 0), "Mythscape", VK_MAKE_VERSION(1, 0, 0), VK_API_VERSION_1_1);

		//uint32_t glfwExtensionCount = 0;
		//const char** glfwExtensions;
		//glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		vk::InstanceCreateInfo createInfo;
		createInfo.setPApplicationInfo(&appInfo);
		auto extensions = getRequiredExtensions(enableValidationLayers);
		//for (int i = 0; i < glfwExtensionCount; i++) {
		//	extensions.push_back(glfwExtensions[i]);
		//}
		createInfo.setEnabledExtensionCount(extensions.size());
		createInfo.setPpEnabledExtensionNames(extensions.data());

		if (enableValidationLayers) {
			createInfo.setEnabledLayerCount(validationLayers.size());
			createInfo.setPpEnabledLayerNames(validationLayers.data());
		}
		else {
			createInfo.setEnabledLayerCount(0);
		}

		auto instance = vk::createInstance(createInfo);  // don't know how to check if success in vulkan hpp

		setupDebugMessenger(enableValidationLayers, instance, debugMessenger);
		return instance;

	}

	std::vector<const char*> getRequiredExtensions(bool enableValidationLayers) {
		uint32_t glfwExtensionCount = 0;
		const char** glfwExtensions;
		glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

		std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

		if (enableValidationLayers) {
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}

		return extensions;
	}

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData) {

		//std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;
		mag::log::warn("Vulkan Validation Layer: " + std::string(pCallbackData->pMessage));

		return VK_FALSE;
	}

	void setupDebugMessenger(bool enableValidationLayers, vk::Instance & instance, VkDebugUtilsMessengerEXT & debugMessenger) {
		if (!enableValidationLayers) return;

		//vk::DebugUtilsMessengerCreateInfoEXT createInfo;
		VkDebugUtilsMessengerCreateInfoEXT createInfo = {};

		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		createInfo.pfnUserCallback = debugCallback;
		createInfo.pUserData = nullptr; // Optional

		if (CreateDebugUtilsMessengerEXT(instance, &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
			throw std::runtime_error("failed to set up debug messenger!");
		}
	}

	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
		if (func != nullptr) {
			return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
		}
		else {
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
		if (func != nullptr) {
			func(instance, debugMessenger, pAllocator);
		}
	}

	vk::PhysicalDevice selectPhysicalDevice(vk::Instance & instance, vk::SurfaceKHR & surface, unsigned int rank) {
		auto physicalDevices = instance.enumeratePhysicalDevices();

		if (physicalDevices.size() == 0) {
			throw std::runtime_error("No physical devices with Vulkan support found!");
		}
		vk::PhysicalDevice result;
		unsigned int max_score = 0;
		for (auto device : physicalDevices) {
			unsigned int score = 0;
			if (device.getProperties().deviceType == vk::PhysicalDeviceType::eDiscreteGpu) {
				score += 1000;
			}

			score += device.getProperties().limits.maxImageDimension2D;

			if (!device.getFeatures().geometryShader) {
				score = 1;
			}

			if (isDeviceSuitable(surface, device) && score > max_score) {
				max_score = score;
				result = device;
			}
		}
		return result;
	}

	QueueFamilyIndices getQueueFamilyIndices(vk::PhysicalDevice & device) {
		uint32_t i = 0;
		QueueFamilyIndices indices;
		auto queueFamilies = device.getQueueFamilyProperties();
		for (const auto & queueFamily : queueFamilies) {
			if (queueFamily.queueCount > 0 && (queueFamily.queueFlags & vk::QueueFlagBits::eGraphics)) {
				indices.graphicsFamily = i;
				indices.presentFamily = i;
			}
			if (indices.graphicsFamily.has_value() && indices.presentFamily.has_value()) {
				break;
			}
			i++;
		}
		return indices;
	}

	// creates presentation queue
	std::vector<vk::DeviceQueueCreateInfo> createQueueInfos(const QueueFamilyIndices & indices)
	{
		std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
		std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

		float queuePriority = 1.0f;

		for (auto queueFamily : uniqueQueueFamilies) {
			queueCreateInfos.push_back(vk::DeviceQueueCreateInfo({}, queueFamily, 1, &queuePriority));
		}

		return queueCreateInfos;
	}

	bool checkDeviceExtensionSupport(vk::PhysicalDevice & device)
	{
		//uint32_t extensionCount;

		const std::vector<const char*> deviceExtensions = {
			VK_KHR_SWAPCHAIN_EXTENSION_NAME
		};

		auto availableExtensions = device.enumerateDeviceExtensionProperties();

		std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end()); //  populate with the extensions we are checking for

		for (const auto & extension : availableExtensions) {
			requiredExtensions.erase(extension.extensionName);
		}

		return requiredExtensions.empty();

	}

	vk::SurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats)
	{

		if (availableFormats.size() == 1 && availableFormats[0].format == vk::Format::eUndefined) {
			//return { VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
			vk::SurfaceFormatKHR fmt;
			fmt.format = vk::Format::eB8G8R8A8Unorm;
			fmt.colorSpace = vk::ColorSpaceKHR::eSrgbNonlinear;
			return fmt;
				//return  { vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear };
		}

		// if we can't choose any format we want, then do this
		for (const auto& availableFormat : availableFormats) {
			if (availableFormat.format == vk::Format::eB8G8R8A8Unorm && availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
				return availableFormat;
			}
		}

		// if that also fails, just choose the first format available
		return availableFormats[0];

	}

	vk::PresentModeKHR chooseSwapPresentMode(const std::vector<vk::PresentModeKHR>& availablePresentModes)
	{

		auto bestMode = vk::PresentModeKHR::eFifo; // only mode guaranteed to be available (except not b/c some drivers don't implement it)
		// triple buffering
		for (const auto & availablePresentMode : availablePresentModes) {
			if (availablePresentMode == vk::PresentModeKHR::eMailbox) {
				return availablePresentMode;
			}
			else if (availablePresentMode == vk::PresentModeKHR::eImmediate) {
				bestMode = availablePresentMode;
			}
		}


		// if not available, go with Fifo
		return bestMode;
	}

	vk::Extent2D chooseSwapExtent(GLFWwindow * window,  const vk::SurfaceCapabilitiesKHR & capabilities)
	{
		if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
			return capabilities.currentExtent;
		}
		else {
			int width, height;
			glfwGetFramebufferSize(window, &width, &height);
			VkExtent2D actualExtent = { (uint32_t)width, (uint32_t)height };

			actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
			actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

			return actualExtent;
		}
	}



	vk::UniqueSwapchainKHR createSwapChain(GLFWwindow * window, vk::SurfaceKHR & surface, vk::Device & device, vk::PhysicalDevice & physicalDevice, vk::Format & swapFormat, vk::Extent2D & swapExtent)
	{

		auto swapChainSupport = querySwapChainSupport(surface, physicalDevice);

		auto surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
		auto presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
		auto extent = chooseSwapExtent(window, swapChainSupport.capabilities);

		// we also have to decide how many images we would like to have in the swap chain (take an extra b/c sometimes have to wait for driver)
		uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
		// make sure we're not exceeding the max count
		if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
			imageCount = swapChainSupport.capabilities.maxImageCount;
		}

		auto indices = getQueueFamilyIndices(physicalDevice);

		uint32_t queueFamilyIndices[2] = { indices.graphicsFamily.value(), indices.presentFamily.value() };


		// create the swap chain

		vk::SwapchainCreateInfoKHR createInfo( vk::SwapchainCreateFlagsKHR(), surface, imageCount, surfaceFormat.format, surfaceFormat.colorSpace, extent, 1, vk::ImageUsageFlagBits::eColorAttachment, 
			vk::SharingMode::eExclusive, 0, nullptr, swapChainSupport.capabilities.currentTransform, vk::CompositeAlphaFlagBitsKHR::eOpaque, presentMode, true, nullptr );



		if (indices.graphicsFamily != indices.presentFamily) {
			createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
			createInfo.queueFamilyIndexCount = 2;
			createInfo.pQueueFamilyIndices = queueFamilyIndices;
		}
		//else {
		//	createInfo.imageSharingMode = vk::SharingMode::eExclusive;
		//	createInfo.queueFamilyIndexCount = 0;
		//	createInfo.pQueueFamilyIndices = nullptr;
		//	//createInfo.queueFamilyIndexCount = 1;
		//	//createInfo.pQueueFamilyIndices = queueFamilyIndices;
		//}

		createInfo.setPreTransform(swapChainSupport.capabilities.currentTransform); // we want the current transform (don't want to change/flip/rotate
		createInfo.setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque); // no blending with other windows in window system (ignore alpha channel)
		createInfo.setPresentMode(presentMode);
		createInfo.setClipped(VK_TRUE); // ignore obscured pixels when window is in front
		createInfo.setOldSwapchain(nullptr);

		//try {

		// before creating swap chain, query for WSI support ( validation complains without this, I don't even know what this means)
		if (physicalDevice.getSurfaceSupportKHR(queueFamilyIndices[0], surface) != VK_TRUE) {
			throw std::runtime_error("This physical device does not include WSI support!");
		}

		swapFormat = surfaceFormat.format;
		swapExtent = extent;

		return device.createSwapchainKHRUnique(createInfo);
		//VkSwapchainKHR swapChain;
		//if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
		//	throw std::runtime_error("failed to create swap chain!");
		//}
		//}
		//catch (vk::SystemError err)
		//{
		//	std::cout << "vk::SystemError: " << err.what() << std::endl;
		//}
		//return vk::UniqueSwapChainKHR;
	}

	std::vector<vk::ImageView> createImageViews(vk::UniqueSwapchainKHR & swapChain, vk::Device & device, std::vector<vk::Image> & swapChainImages, vk::Format & swapFormat, vk::Extent2D & swapExtent)
	{
		std::vector<vk::ImageView> swapChainImageViews; 
		swapChainImageViews.resize(swapChainImages.size());

		for (size_t i = 0; i < swapChainImages.size(); i++) {
			swapChainImageViews[i] = device.createImageView(vk::ImageViewCreateInfo({}, swapChainImages[i], vk::ImageViewType::e2D, swapFormat, vk::ComponentMapping(vk::ComponentSwizzle::eIdentity), vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1)));
		}

		return swapChainImageViews;
	}

	bool isDeviceSuitable(vk::SurfaceKHR & surface, vk::PhysicalDevice & device) {
		
		bool extensionsSupported = checkDeviceExtensionSupport(device);

		bool compatibleSwapChain = false;

		if (extensionsSupported) {
			SwapChainSupportDetails swapChainSupport = querySwapChainSupport(surface, device);
			compatibleSwapChain = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
		}

		auto indices = getQueueFamilyIndices(device);// need to update for sampler anistropy feature support
		if (indices.graphicsFamily.has_value() && indices.presentFamily.has_value() && extensionsSupported && compatibleSwapChain) {
			return true;
		}
		return false;
	}

	vk::UniqueDevice createLogicalDevice(const bool enableValidationLayers, vk::PhysicalDevice & device) {
		vk::UniqueDevice logicalDevice;
		try {

		auto indices = getQueueFamilyIndices(device);
		//vk::DeviceQueueCreateInfo deviceQueueCreateInfo({}, static_cast<uint32_t>(indices.graphicsFamily.value()), 1, &queuePriority);
		auto deviceQueueCreateInfos = createQueueInfos(indices);


		vk::PhysicalDeviceFeatures deviceFeatures = {}; // device.getFeatures();
		deviceFeatures.samplerAnisotropy = VK_TRUE;
		vk::DeviceCreateInfo deviceCreateInfo ({}, deviceQueueCreateInfos.size(), deviceQueueCreateInfos.data());
		deviceCreateInfo.setPEnabledFeatures(&deviceFeatures);

		// before creating device, also enable validation layer
		//deviceCreateInfo.enabledExtensionCount = 0;
		//auto extensions = getRequiredExtensions(enableValidationLayers);
		std::vector<const char*> extensions = {
			VK_KHR_SWAPCHAIN_EXTENSION_NAME
		};

		//extensions.push_back("VK_EXT_debug_utils");




		std::vector<const char*> validationLayers;

		if (enableValidationLayers) {

			validationLayers.push_back("VK_LAYER_LUNARG_standard_validation");


			deviceCreateInfo.setEnabledLayerCount(static_cast<uint32_t>(validationLayers.size()));
			deviceCreateInfo.setPpEnabledLayerNames(validationLayers.data());

		}
		else {
			deviceCreateInfo.setEnabledLayerCount(0);
			deviceCreateInfo.ppEnabledLayerNames = nullptr;
		}

		deviceCreateInfo.setEnabledExtensionCount(extensions.size());
		deviceCreateInfo.setPpEnabledExtensionNames(extensions.data());

		logicalDevice = device.createDeviceUnique(deviceCreateInfo);
		}
		catch (vk::SystemError err)
		{
			std::cout << "vk::SystemError: " << err.what() << std::endl;
		}
		return logicalDevice;

	}

	SwapChainSupportDetails querySwapChainSupport(vk::SurfaceKHR & surface, vk::PhysicalDevice & device) {
		SwapChainSupportDetails details;

		details.capabilities = device.getSurfaceCapabilitiesKHR(surface);
		details.formats = device.getSurfaceFormatsKHR(surface);
		details.presentModes = device.getSurfacePresentModesKHR(surface);

		return details;
	}




}