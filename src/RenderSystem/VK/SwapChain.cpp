#include "SwapChain.h"

namespace mag {
	namespace gfx {
		SwapChain::SwapChain(vk::PhysicalDevice & physicalDevice, vk::UniqueDevice & device, GLFWwindow * window, vk::SurfaceKHR & surface)
		{

			/*
				1. Create the vulkan swap chain
			*/
			auto swapChainSupport = querySwapChainSupport(surface, physicalDevice);

			auto surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
			auto presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
			auto extent = chooseSwapExtent(window, swapChainSupport.capabilities);

			// we also have to decide how many images we would like to have in the swap chain (take an extra b/c sometimes have to wait for driver)
			uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
			// make sure we're not exceeding the max count
			if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
				imageCount = swapChainSupport.capabilities.maxImageCount;
			}

			auto indices = getQueueFamilyIndices(physicalDevice);

			uint32_t queueFamilyIndices[2] = { indices.graphicsFamily.value(), indices.presentFamily.value() };


			// create the swap chain
			vk::SwapchainCreateInfoKHR SCcreateInfo(vk::SwapchainCreateFlagsKHR(), surface, imageCount, 
				surfaceFormat.format, surfaceFormat.colorSpace, extent, 1, vk::ImageUsageFlagBits::eColorAttachment,
				vk::SharingMode::eExclusive, 0, nullptr, swapChainSupport.capabilities.currentTransform, 
				vk::CompositeAlphaFlagBitsKHR::eOpaque, presentMode, true, nullptr);

			if (indices.graphicsFamily != indices.presentFamily) {
				SCcreateInfo.imageSharingMode = vk::SharingMode::eConcurrent;
				SCcreateInfo.queueFamilyIndexCount = 2;
				SCcreateInfo.pQueueFamilyIndices = queueFamilyIndices;
			}
			//else {
			//	createInfo.imageSharingMode = vk::SharingMode::eExclusive;
			//	createInfo.queueFamilyIndexCount = 0;
			//	createInfo.pQueueFamilyIndices = nullptr;
			//	//createInfo.queueFamilyIndexCount = 1;
			//	//createInfo.pQueueFamilyIndices = queueFamilyIndices;
			//}

			SCcreateInfo.setPreTransform(swapChainSupport.capabilities.currentTransform); // we want the current transform (don't want to change/flip/rotate
			SCcreateInfo.setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque); // no blending with other windows in window system (ignore alpha channel)
			SCcreateInfo.setPresentMode(presentMode);
			SCcreateInfo.setClipped(VK_TRUE); // ignore obscured pixels when window is in front
			SCcreateInfo.setOldSwapchain(nullptr);

			// before creating swap chain, query for WSI support ( validation complains without this, I don't even know what this means)
			if (physicalDevice.getSurfaceSupportKHR(queueFamilyIndices[0], surface) != VK_TRUE) {
				throw std::runtime_error("This physical device does not include WSI support!");
			}

			swapFormat = surfaceFormat.format;
			swapExtent = extent;

			swapChain =  device->createSwapchainKHRUnique(SCcreateInfo);
			
			/*
				Get swap chain images
			*/
			swapChainImages = device->getSwapchainImagesKHR(swapChain.get());
			imageViews = createImageViews(swapChain, device.get(), swapChainImages, swapFormat, swapExtent);

			/*
				Store device for swapchain destructor.
			*/
			this->device = &device.get();
		}
		unsigned int SwapChain::getNumImages()
		{
			return imageViews.size();
		}
		uint32_t SwapChain::getWidth()
		{
			return swapExtent.width;
		}
		uint32_t SwapChain::getHeight()
		{
			return swapExtent.height;
		}
		vk::Extent2D & SwapChain::getExtent()
		{
			return swapExtent;
		}
		vk::Format & SwapChain::getFormat()
		{
			return swapFormat;
		}
		vk::ImageView & SwapChain::getImageView(unsigned int index)
		{
			return imageViews[index];
		}
		vk::Framebuffer & SwapChain::getFramebuffer(unsigned int index)
		{
			return framebuffers[index];
		}
		vk::SwapchainKHR SwapChain::getSwapChain()
		{
			return swapChain.get();
		}
		//SwapChain::operator vk::SwapchainKHR* () const
		//{
		//	auto swap = swapChain.get();
		//	return &swap;
		//}
		SwapChain::~SwapChain()
		{
			
			if (!framebuffers.empty()) {
				destroyFramebuffers();
			}

			for (auto & view : imageViews) {
				device->destroyImageView(view);
			}

			imageViews.clear();
		}
		void SwapChain::createFramebuffers(vk::UniqueDevice & device, const vk::RenderPass & renderPass, vk::ImageView depthImageView)
		{
			framebuffers.resize(getNumImages());
			for (unsigned int i = 0; i < getNumImages(); i++) {
				std::vector<vk::ImageView> attachments = { getImageView(i), depthImageView };
				vk::FramebufferCreateInfo fbCI({}, renderPass, static_cast<uint32_t>(attachments.size()), attachments.data(), getWidth(), getHeight(), 1);
				framebuffers[i] = device->createFramebuffer(fbCI);
			}
		}
		void SwapChain::destroyFramebuffers()
		{
			for (auto & framebuffer : framebuffers) {
				device->destroyFramebuffer(framebuffer);
			}
			framebuffers.clear();
		}
	}
}