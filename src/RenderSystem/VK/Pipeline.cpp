#include "Pipeline.h"
namespace mag {
	namespace gfx {
		vkGraphicsPipeline::vkGraphicsPipeline(vk::Device * device, const vkGraphicsPipelineSetupInfo & info)
		{
			/*
				Graphics Pipeline Setup
			*/
			//this->device = &device.get();
			vk::PipelineVertexInputStateCreateInfo viCI({}, info.bindingDescriptions.size(), info.bindingDescriptions.data(), info.attributeDescriptions.size(), info.attributeDescriptions.data()); // vertex descriptions and vertex attributes

			vk::PipelineInputAssemblyStateCreateInfo paCI({}, vk::PrimitiveTopology::eTriangleList, false);

			vk::Viewport viewport(0.0f, 0.0f, info.width, info.height, 0.0f, 1.0f); // viewport == region of frame buffer output to

			vk::Rect2D scissor({ 0,0 }, info.extent);

			vk::PipelineViewportStateCreateInfo viewportStateCI({}, 1, &viewport, 1, &scissor);

			// Rasterizer
			vk::PipelineRasterizationStateCreateInfo rasterizerCI({}, false, false, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack, vk::FrontFace::eClockwise); // front face opposite of opengl (CCW)
			rasterizerCI.lineWidth = 1.0;

			// multisampling
			vk::PipelineMultisampleStateCreateInfo msCI({}, vk::SampleCountFlagBits::e1, false, 1.0f);

			// color blending (combining fragment with color already in the frame buffer)
			// the alpha blending settings basically
			vk::PipelineColorBlendAttachmentState colorBlendAttachment({}, vk::BlendFactor::eSrcAlpha, vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendOp::eAdd, vk::BlendFactor::eOne, vk::BlendFactor::eZero, vk::BlendOp::eAdd); // try r|G\B\A if necessary
			colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
			// below structure references the array of structs for all framebuffers and allows you to set blend constants 
			vk::PipelineColorBlendStateCreateInfo colorBlending({}, false, vk::LogicOp::eCopy, 1, &colorBlendAttachment);
			colorBlending.blendConstants[0] = 0.0f;
			colorBlending.blendConstants[1] = 0.0f;
			colorBlending.blendConstants[2] = 0.0f;
			colorBlending.blendConstants[3] = 0.0f;

			// ubo stuff
			//descriptorSetLayout = device->createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo({}, 1, &ubos[0].layoutBinding));

			// ubo
			//vk::PipelineLayoutCreateInfo pipelineLayoutCI({}, info.descriptorSetLayouts.size(), info.descriptorSetLayouts.data(), 0, nullptr);
			//pipelineLayout = device->createPipelineLayout(pipelineLayoutCI);

			vk::GraphicsPipelineCreateInfo pipelineInfo({}, 2, info.shader.shaderStages.data(), &viCI, &paCI, nullptr, &viewportStateCI, &rasterizerCI, &msCI, nullptr, &colorBlending, nullptr, info.pipelineLayout, info.renderPass);
			pipelineInfo.subpass = 0;
			pipelineInfo.basePipelineHandle = nullptr;
			pipelineInfo.basePipelineIndex = -1;

			pipeline = device->createGraphicsPipeline(nullptr, pipelineInfo);


		}
		vkGraphicsPipeline::vkGraphicsPipeline()
		{
		}
		//vkGraphicsPipeline::~vkGraphicsPipeline()
		//{
		//	//device->destroyPipelineLayout(pipelineLayout);
		//	device->destroyPipeline(pipeline);
		//}
		vk::Pipeline & vkGraphicsPipeline::get()
		{
			return pipeline;
		}
		//vk::DescriptorSet * vkGraphicsPipeline::get(gfx::ResourceSetInitializer & resourceBinding, unsigned int current_frame)
		//{
		//	for (auto && resourceSet : resourceSets) {
		//		if (resourceBinding == resourceSet.initializer) {
		//			return &resourceSet.descriptorSets[current_frame];
		//		}
		//	}
		//	return nullptr;
		//}
		//void vkGraphicsPipeline::AddDescriptorSet(gfx::GraphicsPipelineHandle handle, gfx::ResourceSetInitializer & resourceBinding, std::vector<vk::DescriptorSet>& descriptorSets)
		//{
		//	for (auto && resourceSet : resourceSets) {
		//		if (resourceBinding == resourceSet.initializer) {
		//			resourceSet.descriptorSets = descriptorSets;
		//			break;
		//		}
		//	}
		//}
	}
}