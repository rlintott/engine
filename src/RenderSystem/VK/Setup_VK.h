#pragma once
#include <vulkan/vulkan.hpp>
#include <GLFW/glfw3.h>
#include <optional>
// https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Validation_layers
namespace mag {
	class Window;
	class QueueFamilyIndices {
	public:
		std::optional<uint32_t> graphicsFamily;
		std::optional<uint32_t> presentFamily;

		//QueueFamilyIndices() {
		//	graphicsFamily = -1;
		//}
	};

	struct SwapChainSupportDetails {
		vk::SurfaceCapabilitiesKHR capabilities;
		std::vector<vk::SurfaceFormatKHR> formats;
		std::vector<vk::PresentModeKHR> presentModes;
	};

	SwapChainSupportDetails querySwapChainSupport(vk::SurfaceKHR & surface, vk::PhysicalDevice & device);

	bool checkValidationLayerSupport(const std::vector<const char*> & validationLayers);

	vk::Instance createInstance(bool enableValidationLayers, VkDebugUtilsMessengerEXT & debugMessenger); 

	std::vector<const char*> getRequiredExtensions(bool enableValidationLayers);

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData);

	void setupDebugMessenger(bool enableValidationLayers, vk::Instance & instance, VkDebugUtilsMessengerEXT & debugMessenger);

	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger);

	void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator);

	vk::PhysicalDevice selectPhysicalDevice(vk::Instance & instance, vk::SurfaceKHR & surface, unsigned int rank);

	bool isDeviceSuitable(vk::SurfaceKHR & surface, vk::PhysicalDevice & device);

	vk::UniqueDevice createLogicalDevice(bool enableValidationLayers, vk::PhysicalDevice & device);

	QueueFamilyIndices getQueueFamilyIndices(vk::PhysicalDevice & device);

	std::vector<vk::DeviceQueueCreateInfo> createQueueInfos(const QueueFamilyIndices & indices);

	bool checkDeviceExtensionSupport(vk::PhysicalDevice & device);

	vk::SurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats);

	vk::PresentModeKHR chooseSwapPresentMode(const std::vector<vk::PresentModeKHR> & availablePresentModes);


	vk::Extent2D chooseSwapExtent(GLFWwindow * window, const vk::SurfaceCapabilitiesKHR & capabilities);


	vk::UniqueSwapchainKHR createSwapChain(std::shared_ptr<Window> window, vk::SurfaceKHR & surface, vk::Device & device, vk::PhysicalDevice & physicaDevice, vk::Format & swapFormat, vk::Extent2D & swapExtent);



	std::vector<vk::ImageView> createImageViews(vk::UniqueSwapchainKHR & swapChain, vk::Device & device,  std::vector<vk::Image> & swapChainImages, vk::Format & swapFormat, vk::Extent2D & swapExtent);

}