#pragma once
#include <vk_mem_alloc.h>
#include <vulkan/vulkan.hpp>

namespace mag {

	namespace gfx {

		class vkUniformBuffer {
		public:
			//vk::Buffer buffer;
			std::vector<vk::Buffer> buffers;
			std::vector<vk::DescriptorBufferInfo> bufferInfos;
			std::vector<VmaAllocation> allocations;
			//VmaAllocation allocation;
			size_t length;
			//unsigned int num_buffers;

			//void * data;
			//vkUniformBuffer(size_t size, unsigned int num_buffers) {
			//	this->size = size;
			//	this->num_buffers = num_buffers;
			//}
		};
	}

}