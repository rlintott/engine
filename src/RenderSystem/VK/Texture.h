#pragma once
#include <vulkan/vulkan.hpp>
#include <vk_mem_alloc.h>
#include <atomic>
#include <mag/gfx/Texture.h>
namespace mag {
	namespace gfx {
		class vkTexture2D {
		public:
			//gfx::Texture2DHandle handle;
			vk::Image image;
			vk::ImageView imageView;
			VmaAllocation allocation;
			//std::atomic<bool> ready;
			unsigned int numChannels;
			//size_t height, width;

		};
	}
}