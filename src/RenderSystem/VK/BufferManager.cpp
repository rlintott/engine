#include "BufferManager.h"
#include <mag/types/Byte.h>
#include <mag/gfx/Mesh.h>
namespace mag {
	namespace gfx {
		BufferManager::BufferManager(VmaAllocator * pAllocator, unsigned int swapSize)
		{
			this->pAllocator = pAllocator;
			this->swapSize = swapSize;
		}
		void BufferManager::setCurrentImage(unsigned int index)
		{
			currentImageIndex = index;
		}
		void BufferManager::setSwapSize(unsigned int num_images)
		{
			if (num_images > swapSize) {
				// have not yet implemented elegant way to handle change in swap chain size
				throw std::runtime_error("\nNew swap size larger than old swap size!");
			}
		}
		bool BufferManager::createUniformBuffer(gfx::UniformBufferHandle handle, size_t size)
		{
			vkUniformBuffer uniformBuffer;
			uniformBuffer.length = size;


			for (unsigned int i = 0; i < swapSize; i++) {

				VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
				bufferInfo.size = size;
				bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

				VmaAllocationCreateInfo allocInfo = {};
				allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
				allocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
				//allocInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;


				//uniformBuffer.length = size * swapSize;
				//uniformBuffer.num_buffers = swapSize;
				//uniformBuffer.data = nullptr;
				vk::Buffer buffer;
				VmaAllocation allocation;

				//vmaCreateBuffer(*pAllocator, &bufferInfo, &allocInfo, (VkBuffer*)&uniformBuffer.buffer, &uniformBuffer.allocation, nullptr);
				vmaCreateBuffer(*pAllocator, &bufferInfo, &allocInfo, (VkBuffer*)&buffer, &allocation, nullptr);

				//for (int i = 0; i < swapSize; i++) {
					//uniformBuffer.bufferInfos.push_back(vk::DescriptorBufferInfo(uniformBuffer.buffer, i*uniformBuffer.length, uniformBuffer.length));
				//}

				uniformBuffer.buffers.push_back(buffer);
				uniformBuffer.bufferInfos.push_back(vk::DescriptorBufferInfo(buffer, 0, size));
				uniformBuffer.allocations.push_back(allocation);


			}

			uniformBuffers[handle] = uniformBuffer;

			return true;

		}
		void BufferManager::updateUniformBuffer(gfx::UniformBufferHandle handle, const void * srcData)
		{
			auto it = uniformBuffers.find(handle);
			void * dst;
			if (it != uniformBuffers.end()) {
				vmaMapMemory(*pAllocator, it->second.allocations[currentImageIndex], &dst);
				memcpy((mag::byte*)dst, srcData, it->second.length);
				vmaUnmapMemory(*pAllocator, it->second.allocations[currentImageIndex]);
			}
		}
		void BufferManager::destroyUniformBuffer(gfx::UniformBufferHandle handle)
		{
			auto it = uniformBuffers.find(handle);
			if (it != uniformBuffers.end()) {
				for (int i = 0; i < it->second.buffers.size(); i++) {
					vmaDestroyBuffer(*pAllocator, it->second.buffers[i], it->second.allocations[i]);
				}
				uniformBuffers.erase(it);

			}
		}
		gfx::vkUniformBuffer & BufferManager::getUniformBuffer(gfx::UniformBufferHandle handle)
		{
			return uniformBuffers[handle];
		}
		size_t BufferManager::createTransformBuffer(size_t minimum_size)
		{


			vkTransformBuffer buffer;
			
			VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
			bufferInfo.size = minimum_size * swapSize;//sizeof(math::mat4)*10000*swapSize; // approx 10000 single precision matrices * num swapchain images
			bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

			VmaAllocationCreateInfo allocInfo = {};
			allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
			allocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
			//allocInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;

			vmaCreateBuffer(*pAllocator, &bufferInfo, &allocInfo, (VkBuffer*)&buffer.buffer, &buffer.allocation, nullptr);

			buffer.num_buffers = swapSize;
			buffer.size = minimum_size;//sizeof(math::mat4) * 10000;

			transformBuffers.push_back(buffer);

			return transformBuffers.size() - 1;
		}
		size_t BufferManager::setTransformData(void * srcData, size_t length)
		{
			vkTransformBuffer * tbuffer = nullptr;
			for (auto & buffer : transformBuffers) {
				if (buffer.hasCapacity(length)) {
					tbuffer = &buffer;
					break;
				}
			}
			size_t index = 0;
			if (!tbuffer) {
				index = createTransformBuffer(length);
				tbuffer = &transformBuffers[index];
			}
			// copy into transform buffer
			size_t totalOffset = currentImageIndex * tbuffer->size + tbuffer->used;
			void * dst = nullptr;
			vmaMapMemory(*pAllocator, tbuffer->allocation, &dst);
			memcpy((mag::byte*)dst + currentImageIndex*tbuffer->size + tbuffer->used, srcData, length);
			vmaUnmapMemory(*pAllocator, tbuffer->allocation);
			//size_t localOffset = tbuffer->used;
			tbuffer->used += length;

			BufferOffset bo;
			//bo.index = index;
			bo.size = length;
			//bo.offset = localOffset;
			bo.totalOffset = totalOffset;
			bo.buffer = tbuffer->buffer;
			transformOffsets.push_back(bo);
			//transformOffsets[handle] = bo;
			return transformOffsets.size() - 1;
		}

		void BufferManager::clearTransforms()
		{
			transformOffsets.clear();
			for (auto & buffer : transformBuffers) {
				buffer.clear();
			}
		}

		void BufferManager::bindTransformBuffer(size_t transformBuffer, vk::CommandBuffer & commandBuffer)
		{
			auto & bo = transformOffsets[transformBuffer];

			commandBuffer.bindVertexBuffers(TransformLocation, bo.buffer, bo.totalOffset);
		}

		//std::optional<vk::Buffer> BufferManager::getTransformBuffer(gfx::TransformBufferHandle handle)
		//{

		//	auto it = transformOffsets.find(handle);
		//	if (it != transformOffsets.end()) {

		//	}
		//	return std::optional<vk::Buffer>();
		//}

		BufferManager::~BufferManager()
		{
			// eliminate all uniform buffers
			for (auto && it : uniformBuffers) {
				for (int i = 0; i < it.second.buffers.size(); i++) {
					vmaDestroyBuffer(*pAllocator, it.second.buffers[i], it.second.allocations[i]);
				}
			}

			// eliminate all transform buffers
			for (auto && it : transformBuffers) {
				vmaDestroyBuffer(*pAllocator, it.buffer, it.allocation);
			}
		}

	}

}
