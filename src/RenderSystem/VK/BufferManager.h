#pragma once
#include "UniformBuffer.h"
#include "TransformBuffer.h"
#include <mag/gfx/Buffer.h>
#include <unordered_map>
#include <mag/containers/HandleManager.h>
#include <optional>
namespace mag {
	namespace gfx {

		struct BufferOffset {
			//unsigned int index;
			//size_t offset;
			vk::Buffer buffer;
			size_t totalOffset;
			size_t size;
		};

		class BufferManager {
		private:

			unsigned int currentImageIndex;
			VmaAllocator * pAllocator;
			unsigned int swapSize; // number of swap chain images
			std::unordered_map<gfx::UniformBufferHandle, gfx::vkUniformBuffer, gfx::UniformBufferHandleHasher> uniformBuffers;

			// transform buffers
			std::vector<BufferOffset> transformOffsets;
			std::vector<gfx::vkTransformBuffer> transformBuffers;

		public:

			// manager
			BufferManager(VmaAllocator * pAllocator, unsigned int swapSize);
			void setCurrentImage(unsigned int index);
			void setSwapSize(unsigned int num_images);
			// uniform buffers
			bool createUniformBuffer(gfx::UniformBufferHandle handle, size_t size);
			void updateUniformBuffer(gfx::UniformBufferHandle handle, const void * srcData);
			void destroyUniformBuffer(gfx::UniformBufferHandle handle);
			gfx::vkUniformBuffer & getUniformBuffer(gfx::UniformBufferHandle handle);
			// transform buffers
			size_t createTransformBuffer(size_t minimum_size);
			size_t setTransformData(void * srcData, size_t length);
			void clearTransforms();
			void bindTransformBuffer(size_t transformBuffer, vk::CommandBuffer & commandBuffer);
			
			~BufferManager();

		};
	}
}