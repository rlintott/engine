#pragma once
#include <mag/gfx/Mesh.h>
#include "VertexBuffer.h"
#include <tsl/robin_map.h>
namespace mag {
	namespace gfx {
		/*
			Allocates or finds memory for vertex arrays of mesh primitives
		*/
		class VertexManager {
		private:
			VmaAllocator * pAllocator;
			vk::Buffer stagingBuffer;
			VmaAllocation stagingBufferAllocation;
			size_t stagingBufferSize;
			tsl::robin_map<MeshPrimitiveHandle, PrimitiveData, HandleHasher<MeshPrimitiveHandle>> primitives;
			void InsertRecord(PrimitiveData & primData, VertexAttribute attribute, const mag::byte * data, size_t offset, size_t size, void * dstBuffer);

			size_t GetTotalPrimitiveSize(const gfx::MeshPrimitive & primitive) const;
			void prepareStagingBuffer(size_t desiredSize);
			void copyBuffer(vk::Buffer src, vk::Buffer dst, vk::DeviceSize size, vk::CommandBuffer & commandBuffer);

			// deletion info
			tsl::robin_map<MeshPrimitiveHandle, PrimitiveData, HandleHasher<MeshPrimitiveHandle>> destroyedPrimitives;

			uint32_t max_frames;

		public:
			VertexManager(VmaAllocator * pAllocator, uint32_t max_frames_in_flight);
			~VertexManager();
			void uploadPrimitive(gfx::MeshPrimitiveHandle & handle, const gfx::MeshPrimitive & primitive, vk::CommandBuffer & commanndBuffer);
			void destroyPrimitive(gfx::MeshPrimitiveHandle handle);
			const PrimitiveData * get(gfx::MeshPrimitiveHandle handle) noexcept;
			void destroyPrimitives() noexcept;
		};
	}
}