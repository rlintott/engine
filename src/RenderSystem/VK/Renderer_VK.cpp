#include "Renderer_VK.h"
#include <GLFW/glfw3.h>
// #include <RenderSystem/WindowManager/Window.h>
#include "Buffer_VK.h"
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#include <xxhash.h>
#include <mag/WindowSystemGLFW.h>
#include <mag/log/Log.h>
namespace mag {

	//Renderer::~Renderer()
	//{

	//}

	void Renderer::cleanupSwapChain()
	{

		// wait for rendering to stop/idle
		presentQueue.waitIdle();

		// destroy fences and semaphores
		for (int i = 0; i < max_frames; i++) {
			device->destroyFence(inFlightFences[i]);
			device->destroySemaphore(renderFinished[i]);
			device->destroySemaphore(imageAvailable[i]);
		}

		for (auto& it : threadMap) {
			device->freeCommandBuffers(it.second.commandPool, it.second.commandBuffers);
			it.second.commandBuffers.clear();
		}

		device->freeCommandBuffers(mainCommandPool, primaryCommandBuffers);
		primaryCommandBuffers.clear();

		// clean up frame buffers

		// descriptor set layouts and pools
		device->destroyDescriptorSetLayout(descriptorSetLayout);
		device->destroyDescriptorPool(descriptorPool);

		// destroy the graphics pipeline

		// destroy render pass
		mainRenderPass.reset();
		// destroy depth buffer
		device->destroyImageView(depthImageView);
		vmaDestroyImage(allocator, depthImage, depthImageAllocation);

		// delete pipeline layouts
		device->destroyPipelineLayout(pipelineLayout);
		graphicsPipeline.reset();

		swapChain.reset(); // don't need to destroy manually?

	}

	void Renderer::recreateSwapChain() {
		// wait for rendering to stop/idle
		device->waitIdle();
		cleanupSwapChain();
		prepareSwapChain();
	}

	gfx::Texture2DHandle Renderer::InsertTexture(gfx::Texture2DHandle handle, gfx::Texture2DAsset* texture)
	{
		auto commandBuffer = beginSingleTimeCommands();
		textureManager->create2D(handle, texture, commandBuffer[0]);
		endSingleTimeCommands(commandBuffer);
		return handle;
	}

	PrimitiveResult<gfx::MeshPrimitiveHandle> Renderer::uploadPrimitive(gfx::MeshPrimitiveHandle handle, const gfx::MeshPrimitive& primitive)
	{
		// need to move this out of API-specific code -> do this validation before sending here
		for (auto& it : primitive.attributes) {
			if (it.second.num_elements == 0) {
				return { gfx::MeshPrimitiveHandle(), gfx::PrimitiveError::Error };
			}
		}
		//
		//mag::log::info("Creating cmd buffer");
		auto commandBuffer = beginSingleTimeCommands();
		//mag::log::info("uploading");
		vertexManager->uploadPrimitive(handle, primitive, commandBuffer[0]);
		//mag::log::info("deleting cmd buffer");
		endSingleTimeCommands(commandBuffer);
		//mag::log::info("returning");
		return { handle, gfx::PrimitiveError::Success };
	}

	void Renderer::destroyPrimitive(gfx::MeshPrimitiveHandle handle)
	{
		vertexManager->destroyPrimitive(handle);
	}

	void Renderer::RecordingSetup()
	{

		// wait for fence
		device->waitForFences(inFlightFences[current_frame], true, std::numeric_limits<uint64_t>::max());
		// signal resource managers so they can delete buffers


		// once we're done waiting (it's been signaled), reset the fence
		device->resetFences(inFlightFences[current_frame]);

		vk::CommandBuffer& commandBuffer = primaryCommandBuffers[current_frame];

		{
			vk::CommandBufferBeginInfo beginInfo({}, nullptr);
			beginInfo.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse;
			// begin command buffer
			commandBuffer.begin(beginInfo);
		}
		// begin render pass
		std::array<vk::ClearValue, 2> clearValues;
		clearValues[0].color.setFloat32({ 0.0f, 0.0f, 0.0f, 1.0f });
		clearValues[1].depthStencil = vk::ClearDepthStencilValue{ 0.0f, 0 }; // use 1.0f for depth when regular z buffer, 0.0f for reversed z


		vk::RenderPassBeginInfo renderPassBeginInfo(mainRenderPass->getRenderPass(), swapChain->getFramebuffer(current_frame), vk::Rect2D({ 0,0 }, swapChain->getExtent()), clearValues.size(), clearValues.data());

		commandBuffer.beginRenderPass(renderPassBeginInfo, vk::SubpassContents::eSecondaryCommandBuffers);

		// inheritance info
		vk::CommandBufferInheritanceInfo inheritanceInfo(mainRenderPass->getRenderPass(), {}, swapChain->getFramebuffer(current_frame));

		// begin secondary command buffers (for recording)
		{
			vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eRenderPassContinue | vk::CommandBufferUsageFlagBits::eSimultaneousUse, &inheritanceInfo);
			for (auto& it : threadMap) {
				it.second.commandBuffers[current_frame].begin(beginInfo);
			}
		}
	}

	void Renderer::RecordDrawCommands(std::vector<gfx::DrawCommand>& drawCommands)
	{

		auto workerID = std::this_thread::get_id();
		vk::CommandBuffer& commandBuffer = threadMap[workerID].commandBuffers[current_frame]; //primaryCommandBuffers[current_frame];

		/*
			Primitive Drawing Code
		*/
		vk::Viewport viewPort(0.0f, 0.0f, (float)status.width, (float)status.height, 0.0f, 1.0f);
		commandBuffer.setViewport(0, viewPort);
		commandBuffer.setScissor(0, vk::Rect2D({}, swapChain->getExtent()));
		for (auto& command : drawCommands) {

			// bind pipeline
			auto pl = pipelineManager->Get(command.pipeline);
			commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, pl->get());
			// bind descriptor sets
			auto rs = getResourceSet(workerID, command.resourceSetInitializer);
			// check if descriptor set exists for this pipeline - either in thread data or main storage
			// assume not null for now
			commandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, pl->pipelineLayout, 0, rs->descriptorSets[current_frame], nullptr);
			// bind the vertex buffers (vertices and transforms)  
			auto primitive = vertexManager->get(command.primitive);
			for (auto& it : primitive->attribute) {
				vk::DeviceSize offsets = { it.second.offset };
				commandBuffer.bindVertexBuffers(it.first, 1, &primitive->vertexBuffer, &offsets);
			}

			// bind transform (vertex) buffer
			bufferManager->bindTransformBuffer(command.transform, commandBuffer);

			// bind index buffer if needed and record draw command
			if (primitive->hasIndices) {
				commandBuffer.bindIndexBuffer(primitive->indexBuffer, 0, primitive->indexType);
				commandBuffer.drawIndexed(primitive->num_elements, command.count, 0, 0, 0);
			}
			else {
				commandBuffer.draw(primitive->num_elements, command.count, 0, 0);
			}
		}
	}

	void Renderer::RecordingFinish()
	{
		vk::CommandBuffer& commandBuffer = primaryCommandBuffers[current_frame];
		for (auto& it : threadMap) {
			it.second.commandBuffers[current_frame].end();
			commandBuffer.executeCommands(it.second.commandBuffers[current_frame]);
		}
		commandBuffer.endRenderPass();
		commandBuffer.end();
	}

	bool Renderer::createGraphicsPipeline(gfx::GraphicsPipelineStateInitializer& initializer)
	{
		gfx::vkGraphicsPipelineSetupInfo setupInfo;

		setupInfo.extent = swapChain->getExtent();
		setupInfo.height = static_cast<float>(swapChain->getHeight());
		setupInfo.width = static_cast<float>(swapChain->getWidth());
		setupInfo.renderPass = mainRenderPass->getRenderPass();



		return pipelineManager->createGraphicsPipeline(setupInfo, initializer);

	}


	void Renderer::prepareSwapChain() {

		swapChain = std::unique_ptr<gfx::SwapChain>(new gfx::SwapChain(physicalDevice, device, windowSystem->get(activeWindow), surface));

		/*
			create depth buffer image
		*/
		// we're just gonna force the format we want... sorry.
		vk::Format depthFormat = vk::Format::eD32SfloatS8Uint;
		vk::ImageCreateInfo imageInfo({}, vk::ImageType::e2D, depthFormat);
		imageInfo.extent.height = swapChain->getExtent().height;
		imageInfo.extent.width = swapChain->getExtent().width;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.tiling = vk::ImageTiling::eOptimal;
		imageInfo.usage = vk::ImageUsageFlagBits::eDepthStencilAttachment;

		VmaAllocationCreateInfo depthAllocationInfo = {};
		depthAllocationInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
		depthAllocationInfo.flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT;

		vmaCreateImage(allocator, (VkImageCreateInfo*)& imageInfo, &depthAllocationInfo, (VkImage*)& depthImage, &depthImageAllocation, nullptr);
		// create depth image view
		depthImageView = textureManager->createImageView(depthImage, depthFormat, vk::ImageAspectFlagBits::eDepth);
		// transition depth image
		auto single = beginSingleTimeCommands();
		textureManager->transitionImageLayout(single[0], depthImage, depthFormat, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal);
		endSingleTimeCommands(single);
		// create render pass
		mainRenderPass = std::unique_ptr<gfx::RenderPass>(new gfx::RenderPass(device, swapChain->getFormat(), depthFormat));
		// frame buffer setup
		swapChain->createFramebuffers(device, mainRenderPass->getRenderPass(), depthImageView);

		/*
			Command buffer generation.
		*/

		// command buffers
		for (auto& it : threadMap) {
			vk::CommandBufferAllocateInfo allocInfo(it.second.commandPool, vk::CommandBufferLevel::eSecondary, swapChain->getNumImages());
			it.second.commandBuffers = device->allocateCommandBuffers(allocInfo);
		}
		vk::CommandBufferAllocateInfo allocInfo(mainCommandPool, vk::CommandBufferLevel::ePrimary, swapChain->getNumImages());
		primaryCommandBuffers = device->allocateCommandBuffers(allocInfo);


		/*
			Fence and semaphore initialization.
		*/
		imageAvailable.resize(max_frames);
		renderFinished.resize(max_frames);
		inFlightFences.resize(max_frames);
		vk::FenceCreateInfo fenceCI;
		fenceCI.flags = vk::FenceCreateFlagBits::eSignaled;
		for (unsigned int i = 0; i < max_frames; i++) {
			imageAvailable[i] = device->createSemaphore(vk::SemaphoreCreateInfo());
			renderFinished[i] = device->createSemaphore(vk::SemaphoreCreateInfo());
			inFlightFences[i] = device->createFence(fenceCI);
		}

		current_frame = 0;

	}

	void Renderer::destroyDuplicateResourceSets()
	{
		
		for (auto it = discardResourceSets.begin(); it != discardResourceSets.end();) {
			if (it->discardFrames > max_frames) {
				device->destroyDescriptorPool(it->descriptorPool);
				it = discardResourceSets.erase(it);
			}
			else {
				it->discardFrames++;
				it++;
			}
		}
	}

	gfx::vkResourceSet Renderer::initializeResourceSet(gfx::ResourceSetInitializer& initializer)
	{
		// check if res set exists for this pipeline/initializer?
		unsigned int numImages = swapChain->getNumImages();

		if (!pipelineManager->hasPipeline(initializer.pipeline)) {
			//return false;
			throw std::runtime_error("Pipeline not found!");
		}
		auto pPipeline = pipelineManager->Get(initializer.pipeline);

		unsigned int numUniformBuffers = initializer.getResourceCount(gfx::ResourceBinding::UniformBuffer);
		unsigned int numTexture2D = initializer.getResourceCount(gfx::ResourceBinding::Texture2D);
		unsigned int numSamplers = initializer.getResourceCount(gfx::ResourceBinding::Sampler);

		std::vector<vk::DescriptorPoolSize> poolSizes;
		if (numUniformBuffers > 0) {
			poolSizes.push_back(vk::DescriptorPoolSize(vk::DescriptorType::eUniformBuffer, numUniformBuffers * numImages));
		}
		if (numSamplers > 0) {
			poolSizes.push_back(vk::DescriptorPoolSize(vk::DescriptorType::eSampler, numSamplers * numImages));
		}
		if (numTexture2D > 0) {
			poolSizes.push_back(vk::DescriptorPoolSize(vk::DescriptorType::eSampledImage, numTexture2D * numImages));
		}

		gfx::vkResourceSet resourceSet;
		resourceSet.descriptorPool = device->createDescriptorPool(vk::DescriptorPoolCreateInfo({}, numImages, poolSizes.size(), poolSizes.data()));

		// now we can actually create the descriptor sets!
		resourceSet.initializer = initializer;
		resourceSet.descriptorSets = device->allocateDescriptorSets(vk::DescriptorSetAllocateInfo(resourceSet.descriptorPool, numImages, pPipeline->descriptorSetLayout.data()));

		// now we just need to update the descriptor sets;


		for (size_t i = 0; i < numImages; i++) {
			std::vector<vk::WriteDescriptorSet> descriptorWrites;
			descriptorWrites.resize(numSamplers + numTexture2D + numUniformBuffers);

			for (unsigned int j = 0; j < initializer.resources.size(); j++) {

				if (initializer.resources[j].type == gfx::ResourceBinding::UniformBuffer) {
					auto& uniformBuffer = bufferManager->getUniformBuffer(initializer.resources[j].uniformBuffer);
					descriptorWrites[j].descriptorType = vk::DescriptorType::eUniformBuffer;
					descriptorWrites[j].dstBinding = initializer.resources[j].binding;
					descriptorWrites[j].pBufferInfo = &uniformBuffer.bufferInfos[i];// &bufferInfo;
				}



				descriptorWrites[j].descriptorCount = 1;
				descriptorWrites[j].dstSet = resourceSet.descriptorSets[i];
				descriptorWrites[j].dstArrayElement = 0;
			}
			device->updateDescriptorSets(descriptorWrites, 0);
		}

		return resourceSet;
	}

	bool Renderer::createUniformBuffer(gfx::UniformBufferHandle handle, size_t size)
	{
		return bufferManager->createUniformBuffer(handle, size);
	}

	void Renderer::setUniformBuffer(gfx::UniformBufferHandle handle, const void* src)
	{
		bufferManager->updateUniformBuffer(handle, src);
	}

	size_t Renderer::setTransformData(void* src, size_t size)
	{
		return bufferManager->setTransformData(src, size);
	}

	void Renderer::RegisterWorker(std::thread::id id)
	{
		threadMap[id].commandPool = device->createCommandPool(vk::CommandPoolCreateInfo(vk::CommandPoolCreateFlagBits::eResetCommandBuffer, queueIndices.graphicsFamily.value()));
	}

	gfx::vkResourceSet* Renderer::getResourceSet(std::thread::id workerID, gfx::ResourceSetInitializer& initializer)
	{
		// check main storage

		auto pRS = pipelineManager->getResourceSet(initializer);

		if (pRS) {
			return pRS;
		}

		// check local thread data

		auto it = threadMap[workerID].newResourceSets.find(initializer);
		if (it != threadMap[workerID].newResourceSets.end()) {
			return &it->second;
		}

		// create new set
		auto resSet = initializeResourceSet(initializer);
		threadMap[workerID].newResourceSets[initializer] = resSet;
		return &threadMap[workerID].newResourceSets[initializer];

		//return pipelineManager->getResourceSet(initializer);
	}

	void Renderer::mergeResourceSets()
	{
		for (auto& it : threadMap) {
			for (auto& res : it.second.newResourceSets) {
				// store if not found in main storage
				if (!pipelineManager->getResourceSet(res.first)) {
					pipelineManager->storeResourceSet(res.first, res.second);
				}
				else {
					// destroy this duplicate that never should have been made
					discardResourceSets.push_back(res.second);
				}
			}
			it.second.newResourceSets.clear();
		}

		destroyDuplicateResourceSets();
	}

	Renderer::Renderer(std::vector<std::thread::id>& threadIDs, WindowHandle window, std::shared_ptr<WindowSystem> windowSystem) {

		instance = createInstance(enableValidationLayers, debugMessenger);
		this->windowSystem = std::dynamic_pointer_cast<WindowSystemGLFW>(windowSystem);
				SetWindow(window);

		// create Surface
		if (glfwCreateWindowSurface(instance, this->windowSystem->get(activeWindow), nullptr, (VkSurfaceKHR*)& surface) != VK_SUCCESS) {
			throw std::runtime_error("Could not create window surface!");
		}
		//devices
		physicalDevice = selectPhysicalDevice(instance, surface, 1);
		device = createLogicalDevice(enableValidationLayers, physicalDevice);

		//queues
		queueIndices = getQueueFamilyIndices(physicalDevice);
		graphicsQueue = device->getQueue(queueIndices.graphicsFamily.value(), 0);
		presentQueue = device->getQueue(queueIndices.presentFamily.value(), 0);


		// create allocator
		VmaAllocatorCreateInfo allocatorInfo = {};
		allocatorInfo.physicalDevice = physicalDevice;
		allocatorInfo.device = device.get();
		vmaCreateAllocator(&allocatorInfo, &allocator);

		// command pools
		createCommandPools(threadIDs);

		// managers
		textureManager = std::unique_ptr<gfx::TextureManager>(new gfx::TextureManager(&device.get(), &allocator));
		vertexManager = std::unique_ptr<gfx::VertexManager>(new gfx::VertexManager(&allocator, max_frames));
		pipelineManager = std::unique_ptr<gfx::PipelineManager>(new gfx::PipelineManager(&device.get()));

		// swap chain creation
		prepareSwapChain();
		bufferManager = std::unique_ptr<gfx::BufferManager>(new gfx::BufferManager(&allocator, swapChain->getNumImages()));
		bufferManager->setCurrentImage(current_frame);
		bufferManager->clearTransforms();

		status.resized = true;
		glfwGetFramebufferSize(this->windowSystem->get(activeWindow), &status.width, &status.height);

	}

	void Renderer::ShutDown() {

		device->waitIdle();

		// clean up resource sets, need a better way to do this
		for (auto & res: discardResourceSets) {
			device->destroyDescriptorPool(res.descriptorPool);
		}
		discardResourceSets.clear();

		cleanupSwapChain();
		pipelineManager.reset();
		bufferManager.reset();
		vertexManager.reset();
		textureManager.reset();

		vmaDestroyAllocator(allocator);

		destroyCommandPools();


		vkDestroySurfaceKHR(instance, surface, nullptr);
		device.reset();

		if (enableValidationLayers) {
			DestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr);
		}

	}

	Renderer::~Renderer()
	{
		ShutDown();
	}

	RendererStatus Renderer::Update() {


		// acquire image from swapchain
		auto imageIndex = device->acquireNextImageKHR(swapChain->getSwapChain(), std::numeric_limits<uint64_t>::max(), imageAvailable[current_frame], nullptr);

		// submit the queue
		const vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };
		presentQueue.submit(vk::SubmitInfo(1, &imageAvailable[current_frame], waitStages, 1, &primaryCommandBuffers[current_frame], 1, &renderFinished[current_frame]), inFlightFences[current_frame]);

		// presentation
		auto swapChainObj = swapChain->getSwapChain();
		presentQueue.presentKHR(vk::PresentInfoKHR(1, &renderFinished[current_frame], 1, &swapChainObj, &imageIndex.value));
		// update index for next frame
		current_frame = (current_frame + 1) % max_frames;

		// resource management
		bufferManager->setCurrentImage(current_frame);
		bufferManager->clearTransforms();
		mergeResourceSets();
		vertexManager->destroyPrimitives();

		return status;
	}

	void Renderer::SetWindow(WindowHandle window) {
		WindowUserData * wud = static_cast<WindowUserData*>(glfwGetWindowUserPointer(windowSystem->get(window)));
		wud->renderer = this;
		glfwSetFramebufferSizeCallback(windowSystem->get(window), &framebufferResizeCallback);
		this->activeWindow = window;
	}

	void Renderer::framebufferResizeCallback(GLFWwindow* window, int width, int height) {
		auto wud = reinterpret_cast<WindowUserData*>(glfwGetWindowUserPointer(window));
		auto renderer = wud->renderer;
		renderer->status.resized = true;
		renderer->status.width = width;
		renderer->status.height = height;
		// need to wait for window to get unminimized!
		while (width == 0 || height == 0) {
			glfwGetFramebufferSize(window, &width, &height);
			glfwWaitEvents();
		}
		renderer->recreateSwapChain();
	}
}