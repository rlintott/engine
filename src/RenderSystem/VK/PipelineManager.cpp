#include "PipelineManager.h"
#include <mag/math/Matrix.h>
namespace mag {
	namespace gfx {
		void PipelineManager::destroyPipeline(gfx::vkGraphicsPipeline & pipeline)
		{
			device->destroyPipeline(pipeline.pipeline);
			device->destroyDescriptorSetLayout(pipeline.descriptorSetLayout[0]);
			device->destroyPipelineLayout(pipeline.pipelineLayout);
		}
		vkGraphicsPipeline * PipelineManager::Get(gfx::GraphicsPipelineHandle handle)
		{
			return graphicsPipelines[handle].get();
		}
		PipelineManager::PipelineManager(vk::Device * device)
		{
			this->device = device;
		}
		PipelineManager::~PipelineManager()
		{

			// destroy all descriptor pools also

			for (auto & set : resourceSets) {
				device->destroyDescriptorPool(set.second.descriptorPool);
			}
			resourceSets.clear();

			// destroy all pipelines
			for (auto & it : graphicsPipelines) {
				if (it.second) {
					//device->destroy(it.second->get());
					destroyPipeline(*it.second);
				}
			}
			graphicsPipelines.clear();



		}
		bool PipelineManager::createGraphicsPipeline(const gfx::vkGraphicsPipelineSetupInfo & setupInfo, const GraphicsPipelineStateInitializer & initializer)
		{
			//vkGraphicsPipelineSetupInfo info;
			//graphicsPipelines[handle] = std::make_unique<vkGraphicsPipeline>(vkGraphicsPipeline(device,setupInfo));

		// shader modules
			std::vector<vk::ShaderModule> shaderModules;


			// shader stages
			std::vector<vk::PipelineShaderStageCreateInfo> shaderStageInfo;
			if (initializer.shader.vertCode.size() > 0) {
				shaderModules.push_back(
					device->createShaderModule(vk::ShaderModuleCreateInfo({}, (initializer.shader.vertCode.end() - initializer.shader.vertCode.begin()) * sizeof(mag::spvbyte), initializer.shader.vertCode.data()))
				);
				shaderStageInfo.push_back(vk::PipelineShaderStageCreateInfo({}, vk::ShaderStageFlagBits::eVertex, shaderModules.back(), "main"));
			}
			else {
				return false;
			}

			if (initializer.shader.fragCode.size() > 0) {
				shaderModules.push_back(
					device->createShaderModule(vk::ShaderModuleCreateInfo({}, (initializer.shader.fragCode.end() - initializer.shader.fragCode.begin()) * sizeof(mag::spvbyte), initializer.shader.fragCode.data()))
				);
				shaderStageInfo.push_back(vk::PipelineShaderStageCreateInfo({}, vk::ShaderStageFlagBits::eFragment, shaderModules.back(), "main"));
			}
			else {
				return false;
			}

			// vertex info
			std::vector<vk::VertexInputAttributeDescription> attributeDescriptions;
			std::vector<vk::VertexInputBindingDescription> bindingDescriptions;
			for (auto & attribute : initializer.shader.vertexAttributes) {
				switch (attribute.first) {
				case VertexAttribute::Position:
					attributeDescriptions.push_back(vk::VertexInputAttributeDescription(attribute.second, attribute.first, vk::Format::eR32G32B32Sfloat, 0));
					bindingDescriptions.push_back(vk::VertexInputBindingDescription(attribute.first, sizeof(mag::vec3), vk::VertexInputRate::eVertex));
					break;
				case VertexAttribute::Normal:
					attributeDescriptions.push_back(vk::VertexInputAttributeDescription(attribute.second, attribute.first, vk::Format::eR32G32B32Sfloat, 0));
					bindingDescriptions.push_back(vk::VertexInputBindingDescription(attribute.first, sizeof(mag::vec3), vk::VertexInputRate::eVertex));
					break;
				case VertexAttribute::UV0:
					attributeDescriptions.push_back(vk::VertexInputAttributeDescription(attribute.second, attribute.first, vk::Format::eR32G32Sfloat, 0));
					bindingDescriptions.push_back(vk::VertexInputBindingDescription(attribute.first, sizeof(mag::vec2), vk::VertexInputRate::eVertex));
					break;
				case VertexAttribute::Tangent:
					attributeDescriptions.push_back(vk::VertexInputAttributeDescription(attribute.second, attribute.first, vk::Format::eR32G32B32Sfloat, 0));
					bindingDescriptions.push_back(vk::VertexInputBindingDescription(attribute.first, sizeof(mag::vec3), vk::VertexInputRate::eVertex));
					break;
				case VertexAttribute::Transform:
					for (int i = 0; i < 4; i++) {
						attributeDescriptions.push_back(vk::VertexInputAttributeDescription(attribute.second + i, TransformLocation, vk::Format::eR32G32B32A32Sfloat, i * sizeof(mag::vec4)));
					}
					bindingDescriptions.push_back(vk::VertexInputBindingDescription(attribute.second, sizeof(mag::mat4), vk::VertexInputRate::eInstance));
					break;
				default:
					break;
				}
			}

			// descriptor set layout: uniform buffer / texture info -> pipeline layout
			std::vector<vk::DescriptorSetLayoutBinding> layoutBindings;
			for (auto & binding : initializer.shader.uniformBuffers) {
				layoutBindings.push_back(vk::DescriptorSetLayoutBinding(binding.second.binding, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eAllGraphics));
			}

			for (auto & binding : initializer.shader.textures) {
				layoutBindings.push_back(vk::DescriptorSetLayoutBinding(binding.second, vk::DescriptorType::eSampledImage, 1, vk::ShaderStageFlagBits::eAllGraphics));
			}

			for (auto & binding : initializer.shader.samplers) {
				layoutBindings.push_back(vk::DescriptorSetLayoutBinding(binding.second, vk::DescriptorType::eSampler, 1, vk::ShaderStageFlagBits::eAllGraphics));
			}

			auto descriptorSetLayout = device->createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo({}, static_cast<uint32_t>(layoutBindings.size()), layoutBindings.data()));
			auto pipelineLayout = device->createPipelineLayout(vk::PipelineLayoutCreateInfo({}, 1, &descriptorSetLayout));

			gfx::vkGraphicsPipeline pipeline;
			pipeline.descriptorSetLayout.resize(3, descriptorSetLayout);
			pipeline.bindingDescriptions = bindingDescriptions;
			pipeline.attributeDescriptions = attributeDescriptions;
			pipeline.layoutBindings = layoutBindings;


			/*
				Pipeline creation.
			*/

			vk::PipelineVertexInputStateCreateInfo viCI({}, static_cast<uint32_t>(bindingDescriptions.size()), bindingDescriptions.data(), static_cast<uint32_t>(attributeDescriptions.size()), attributeDescriptions.data()); // vertex descriptions and vertex attributes

			vk::PipelineInputAssemblyStateCreateInfo paCI({}, vk::PrimitiveTopology::eTriangleList, false);

			vk::Viewport viewport(0.0f, 0.0f, setupInfo.width, setupInfo.height, 0.0f, 1.0f); // viewport == region of frame buffer output to
			vk::Rect2D scissor({ 0,0 }, setupInfo.extent);

			vk::PipelineViewportStateCreateInfo viewportStateCI({}, 1, &viewport, 1, &scissor);

			// Rasterizer
			vk::PipelineRasterizationStateCreateInfo rasterizerCI({}, false, false, vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack, vk::FrontFace::eClockwise); // front face opposite of opengl (CCW)
			rasterizerCI.lineWidth = 1.0;

			// multisampling
			vk::PipelineMultisampleStateCreateInfo msCI({}, vk::SampleCountFlagBits::e1, false, 1.0f);

			// color blending (combining fragment with color already in the frame buffer)
			// the alpha blending settings basically
			vk::PipelineColorBlendAttachmentState colorBlendAttachment({}, vk::BlendFactor::eSrcAlpha, vk::BlendFactor::eOneMinusSrcAlpha, vk::BlendOp::eAdd, vk::BlendFactor::eOne, vk::BlendFactor::eZero, vk::BlendOp::eAdd); // try r|G\B\A if necessary
			colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
			// below structure references the array of structs for all framebuffers and allows you to set blend constants 
			vk::PipelineColorBlendStateCreateInfo colorBlending({}, false, vk::LogicOp::eCopy, 1, &colorBlendAttachment);
			colorBlending.blendConstants[0] = 0.0f;
			colorBlending.blendConstants[1] = 0.0f;
			colorBlending.blendConstants[2] = 0.0f;
			colorBlending.blendConstants[3] = 0.0f;

			vk::DynamicState dynamicStates[] = { vk::DynamicState::eViewport, vk::DynamicState::eLineWidth, vk::DynamicState::eScissor };
			vk::PipelineDynamicStateCreateInfo dynamicStateCreateInfo({}, 3, &dynamicStates[0]);
		
			vk::PipelineDepthStencilStateCreateInfo depthStencil({}, true, true, vk::CompareOp::eGreaterOrEqual, false); // use eGreaterirEqual for reversed z buffer (more precision)
			depthStencil.minDepthBounds = 0.0f; // optional
			depthStencil.maxDepthBounds = 1.0f; // optional
			depthStencil.stencilTestEnable = false;

			vk::GraphicsPipelineCreateInfo pipelineInfo({}, static_cast<uint32_t>(shaderStageInfo.size()), shaderStageInfo.data(), &viCI, &paCI, nullptr, &viewportStateCI, &rasterizerCI, &msCI, &depthStencil, &colorBlending, &dynamicStateCreateInfo, pipelineLayout, setupInfo.renderPass);
			pipelineInfo.subpass = 0;
			pipelineInfo.basePipelineHandle = nullptr;
			pipelineInfo.basePipelineIndex = -1;

			pipeline.pipeline = device->createGraphicsPipeline(nullptr, pipelineInfo);
			pipeline.pipelineLayout = pipelineLayout;
			graphicsPipelines[initializer.pipeline] = std::make_unique<vkGraphicsPipeline>(pipeline);

			// resource binding?

			// destroy shader modules
			for (auto & shaderModule : shaderModules) {
				device->destroyShaderModule(shaderModule);
			}

			return true;
		}
		void PipelineManager::storeResourceSet(const gfx::ResourceSetInitializer & initializer, vkResourceSet & set)
		{
			resourceSets[initializer] = vkResourceSet(set);
		}
		vkResourceSet * PipelineManager::getResourceSet(const gfx::ResourceSetInitializer & initializer)
		{
			auto it = resourceSets.find(initializer);
			if (it != resourceSets.end()) {
				return &it->second;
			}
			return nullptr;
		}
		bool PipelineManager::hasPipeline(gfx::GraphicsPipelineHandle gpHandle)
		{

			return (graphicsPipelines.find(gpHandle) != graphicsPipelines.end());
		}
		void PipelineManager::removePipeline(gfx::GraphicsPipelineHandle handle)
		{
			auto it = graphicsPipelines.find(handle);
			if (it != graphicsPipelines.end()) {
				destroyPipeline(*it->second);
				graphicsPipelines.erase(it);
			}
		}
	}
}