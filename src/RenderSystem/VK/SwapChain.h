#pragma once
#include <RenderSystem/VK/Setup_VK.h>
namespace mag {
	class Window;
	namespace gfx {

		class SwapChain {
		private:
			vk::UniqueSwapchainKHR swapChain;
			std::vector<vk::Image> swapChainImages;
			std::vector<vk::ImageView> imageViews;
			vk::Format swapFormat;
			vk::Extent2D swapExtent;
			std::vector<vk::Framebuffer> framebuffers;
			vk::Device * device;

		public:
				SwapChain(vk::PhysicalDevice & physicalDevice, vk::UniqueDevice & device, GLFWwindow * window, vk::SurfaceKHR & surface);
				unsigned int getNumImages();
				uint32_t getWidth();
				uint32_t getHeight();
				vk::Extent2D & getExtent();
				vk::Format & getFormat();

				vk::ImageView & getImageView(unsigned int index);
				vk::Framebuffer & getFramebuffer(unsigned int index);
				vk::SwapchainKHR getSwapChain();
				//vk::SwapchainKHR * getSwapChain();
				//operator vk::SwapchainKHR* () const;

				~SwapChain();

				void createFramebuffers(vk::UniqueDevice & device, const vk::RenderPass & renderPass, vk::ImageView depthImageView);
				void destroyFramebuffers();
		};

	}
}