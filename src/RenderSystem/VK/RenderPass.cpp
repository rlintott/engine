#include "RenderPass.h"

namespace mag {
	namespace gfx {
		RenderPass::RenderPass(vk::UniqueDevice & device, vk::Format & format, vk::Format depthFormat)
		{
			this->device = &device.get();
			// define render passes after creating pipeline layout
			vk::AttachmentDescription colorAttachment({}, format, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare, vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR);
			vk::AttachmentDescription depthAttachment({}, depthFormat, vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eDontCare, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal);
			// subpasses, just one for now
			vk::AttachmentReference colorAttachmentRef(0, vk::ImageLayout::eColorAttachmentOptimal);
			vk::AttachmentReference depthAttachmentRef(1, vk::ImageLayout::eDepthStencilAttachmentOptimal);

			vk::SubpassDescription subpass({}, vk::PipelineBindPoint::eGraphics);
			subpass.colorAttachmentCount = 1;
			subpass.pColorAttachments = &colorAttachmentRef;
			subpass.pDepthStencilAttachment = &depthAttachmentRef;
			
			std::array<vk::AttachmentDescription, 2> attachments = { colorAttachment, depthAttachment };

			vk::RenderPassCreateInfo renderPassInfo({}, attachments.size(), attachments.data(), 1, &subpass);

			// subpass dependencies
			vk::SubpassDependency dep(VK_SUBPASS_EXTERNAL, 0, vk::PipelineStageFlagBits::eColorAttachmentOutput, vk::PipelineStageFlagBits::eColorAttachmentOutput, (vk::AccessFlags)0, vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite);

			renderPassInfo.dependencyCount = 1;
			renderPassInfo.pDependencies = &dep;
			renderPass = device->createRenderPass(renderPassInfo);
		}
		RenderPass::~RenderPass()
		{
			device->destroyRenderPass(renderPass);
		}
		vk::RenderPass RenderPass::getRenderPass()
		{
			return renderPass;
		}
	}
}