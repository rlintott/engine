#pragma once
#include <vulkan/vulkan.hpp>
#include <mag/gfx/PipelineState.h>
#include "Pipeline.h"
#include <memory>
#include <unordered_map>
#include <mag/gfx/ResourceSet.h>

namespace mag {
	namespace gfx {
		class PipelineManager {
		private:
			vk::Device * device;
			std::unordered_map<gfx::GraphicsPipelineHandle, std::unique_ptr<vkGraphicsPipeline>, HandleHasher<gfx::GraphicsPipelineHandle>> graphicsPipelines;
			//std::unordered_map<gfx::ResourceSetHandle, vkResourceSet, HandleHasher<ResourceSetHandle>> resourceSets;
			std::unordered_map < gfx::ResourceSetInitializer, vkResourceSet, ResourceSetInitializerHasher> resourceSets;
			void destroyPipeline(gfx::vkGraphicsPipeline & pipeline);
		public:
			vkGraphicsPipeline * Get(gfx::GraphicsPipelineHandle handle);
			PipelineManager(vk::Device * device);
			~PipelineManager();
			bool createGraphicsPipeline(const gfx::vkGraphicsPipelineSetupInfo & setupInfo, const GraphicsPipelineStateInitializer & info);
			//bool createResourceSet(const gfx::ResourceSetHandle resHandle, const gfx::GraphicsPipelineHandle gpHandle, gfx::ResourceSetInitializer & initializer);
			//void insertResourceSet(const gfx::ResourceSetHandle, vkResourceSet & resourceSet);
			void storeResourceSet(const gfx::ResourceSetInitializer & initializer, vkResourceSet & set);
			vkResourceSet * getResourceSet(const gfx::ResourceSetInitializer & initializer);
			bool hasPipeline(gfx::GraphicsPipelineHandle gpHandle);
			void removePipeline(gfx::GraphicsPipelineHandle handle);
			//void destroy(const GraphicsPipelineHandle handle);
		};
	}
}