#pragma once
#include <vulkan/vulkan.hpp>
#include <vk_mem_alloc.h>
namespace mag {
	class UniformBufferObject {
	public:
		vk::DescriptorSetLayoutBinding layoutBinding;
		VkBuffer buffer;
		vk::DeviceMemory memory;
		VkBufferCreateInfo bufferInfo;
		VmaAllocationCreateInfo allocInfo;
		VmaAllocator allocator;
		VmaAllocation allocation;
		size_t size;
		void Init(unsigned int binding, size_t bufferSize, VmaAllocator allocator) {
			layoutBinding = vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eUniformBuffer, 1);
			layoutBinding.setStageFlags(vk::ShaderStageFlagBits::eAllGraphics);
			layoutBinding.setPImmutableSamplers(nullptr);
			size = bufferSize;

			bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
			bufferInfo.size = bufferSize;
			bufferInfo.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
			// allocation info
			allocInfo = {};
			allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
			allocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;


			this->allocator = allocator;
			// allocate
			vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &buffer, &allocation, nullptr);
		}

		void Set(void* source, size_t size) {
			void * data = nullptr;
			vmaMapMemory(allocator, allocation, &data);
			memcpy(data, source, size);
			vmaUnmapMemory(allocator, allocation);
		}

		void Deinit() {
			vmaDestroyBuffer(allocator, buffer, allocation);
		}

		//~UniformBufferObject() {

		//}

	};
}