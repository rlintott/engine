#include "TextureManager.h"

namespace mag {
	namespace gfx {
		void TextureManager::resizeStagingBuffer(size_t length)
		{

			/*
				Staging buffer
			*/
			if (stagingBufferSize >= length) {
				return;
			}

			if (stagingBufferSize != 0) {
				// deallocate staging buffer
				vmaDestroyBuffer(*allocator, stagingBuffer, stagingBufferAllocation);

			}

			size_t newBufferSize = 0;
			if (stagingBufferSize < length) {
				if (length < 100) {
					newBufferSize = 100;
				}
				else {
					newBufferSize = length * 2;
				}
			}

			stagingBufferSize = newBufferSize;

			vk::BufferCreateInfo bufferInfo({}, stagingBufferSize, vk::BufferUsageFlagBits::eTransferSrc);
			VmaAllocationCreateInfo aci = {};
			aci.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
			aci.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
			VkBufferCreateInfo bufferCreateInfo = bufferInfo;
			vmaCreateBuffer(*allocator, &bufferCreateInfo, &aci, &stagingBuffer, &stagingBufferAllocation, nullptr);

		}
		TextureManager::TextureManager(vk::Device * pDevice, VmaAllocator * pAllocator)
		{
			device = pDevice;
			allocator = pAllocator;
			//lastTexture2DHandle.value = 0;
			//this->commandPool = commandPool;
			//this->graphicsQueue = graphicsQueue;
			stagingBufferSize = 0;
			resizeStagingBuffer(10000000);

		}
		vkTexture2D * TextureManager::create2D(gfx::Texture2DHandle handle, gfx::Texture2DAsset * data, vk::CommandBuffer & singleTimeCommandBuffer)
		{
			// prepare texture obj
			std::shared_ptr<vkTexture2D> tex(new vkTexture2D());
			//tex->ready = false;
			//tex->handle.value = lastTexture2DHandle.value++;
			/*
				Update staging buffer and copy image data into it.
			*/
			resizeStagingBuffer(data->data.size());
			void * buffer = nullptr;
			vmaMapMemory(*allocator, stagingBufferAllocation, &buffer);
			memcpy(buffer, data->data.data(), data->data.size());
			vmaUnmapMemory(*allocator, stagingBufferAllocation);

			/*
				Create vulkan image;
			*/
			vk::ImageCreateInfo imageInfo;
			imageInfo.imageType = vk::ImageType::e2D;//VK_IMAGE_TYPE_2D;
			imageInfo.extent.width = static_cast<uint32_t>(data->width);
			imageInfo.extent.height = static_cast<uint32_t>(data->height);
			imageInfo.extent.depth = 1;
			imageInfo.mipLevels = 1;
			imageInfo.arrayLayers = 1;
			imageInfo.format = vk::Format::eR8G8B8A8Unorm;//VK_FORMAT_R8G8B8A8_UNORM;
			imageInfo.tiling = vk::ImageTiling::eOptimal; // VK_IMAGE_TILING_OPTIMAL;
			imageInfo.initialLayout = vk::ImageLayout::eUndefined; // VK_IMAGE_LAYOUT_UNDEFINED;
			imageInfo.samples = vk::SampleCountFlagBits::e1; // VK_SAMPLE_COUNT_1_BIT;
			imageInfo.sharingMode = vk::SharingMode::eExclusive; // VK_SHARING_MODE_EXCLUSIVE;
			imageInfo.usage = vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled;//VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;


			/*
				GPU-only texture buffer.
			*/
			VmaAllocationCreateInfo allocCreateInfo = {};
			allocCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

			/*
				Create and allocate the image.
			*/
			vmaCreateImage(*allocator, (VkImageCreateInfo*)&imageInfo, &allocCreateInfo, (VkImage*)&tex->image, &tex->allocation, nullptr);

			/*
				Transition the image and copy the buffer.
			*/

			transitionImageLayout(singleTimeCommandBuffer, tex->image, vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);
			copyBufferToImage(singleTimeCommandBuffer, stagingBuffer, tex->image, data->width, data->height);
			transitionImageLayout(singleTimeCommandBuffer, tex->image, vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);

			/*
				Create image view
			*/

			tex->imageView = createImageView(tex->image, vk::Format::eR8G8B8A8Unorm, vk::ImageAspectFlagBits::eColor);

			texture2D.emplace(handle,tex);

			return tex.get();
		}

		void TextureManager::destroy(gfx::Texture2DHandle handle)
		{
			destroyTexture(handle);
			texture2D.erase(handle);
		}

		//void TextureManager::DoSingleTimeCommands(vk::CommandBuffer & singleTimeCommandBuffer)
		//{

		//}

		void TextureManager::transitionImageLayout(vk::CommandBuffer & commandBuffer, vk::Image image, vk::Format format, vk::ImageLayout oldLayout, vk::ImageLayout newLayout)
		{
			vk::ImageMemoryBarrier barrier;
			barrier.oldLayout = oldLayout;
			barrier.newLayout = newLayout;

			barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
			barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;

			barrier.image = image;

			if (newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal) {
				barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
				// if has stencil component
				if (format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD24UnormS8Uint) {
					barrier.subresourceRange.aspectMask |= vk::ImageAspectFlagBits::eStencil;
				}
			}
			else {
				barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			}

			barrier.subresourceRange.baseMipLevel = 0;
			barrier.subresourceRange.levelCount = 1;
			barrier.subresourceRange.baseArrayLayer = 0;
			barrier.subresourceRange.layerCount = 1;
			barrier.srcAccessMask = (vk::AccessFlags)0;
			barrier.dstAccessMask = (vk::AccessFlags)0;

			// part 2

			vk::PipelineStageFlags sourceStage;
			vk::PipelineStageFlags destinationStage;

			if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal) {
				barrier.srcAccessMask = {};
				barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

				sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
				destinationStage = vk::PipelineStageFlagBits::eTransfer;
			}
			else if (oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal) {
				barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
				barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

				sourceStage = vk::PipelineStageFlagBits::eTransfer;
				destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
			}
			else if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal) {
				barrier.srcAccessMask = vk::AccessFlagBits(0);
				barrier.dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite;

				sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
				destinationStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;
			}

			else {
				throw std::invalid_argument("unsupported layout transition!");
			}

			commandBuffer.pipelineBarrier(sourceStage, destinationStage, vk::DependencyFlags(0), nullptr, nullptr, barrier);
		}

		void TextureManager::copyBufferToImage(vk::CommandBuffer & commandBuffer, vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height)
		{
			vk::BufferImageCopy region;
			region.bufferOffset = 0;
			region.bufferRowLength = 0;
			region.bufferImageHeight = 0;


			region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
			region.imageSubresource.mipLevel = 0;
			region.imageSubresource.baseArrayLayer = 0;
			region.imageSubresource.layerCount = 1;

			region.imageOffset = vk::Offset3D{ 0, 0, 0 };
			region.imageExtent = vk::Extent3D{
				width,
				height,
				1
			};

			commandBuffer.copyBufferToImage(buffer, image, vk::ImageLayout::eTransferDstOptimal, region);

		}

		vk::ImageView TextureManager::createImageView(vk::Image & image, vk::Format format, vk::ImageAspectFlagBits aspectFlags)
		{
			vk::ImageViewCreateInfo viewInfo({}, image, vk::ImageViewType::e2D, format);
			viewInfo.subresourceRange.aspectMask = aspectFlags;//vk::ImageAspectFlagBits::eColor;
			viewInfo.subresourceRange.baseMipLevel = 0;
			viewInfo.subresourceRange.levelCount = 1;
			viewInfo.subresourceRange.baseArrayLayer = 0;
			viewInfo.subresourceRange.layerCount = 1;

			return device->createImageView(viewInfo);
		}

		void TextureManager::destroyTexture(gfx::Texture2DHandle handle)
		{
			if (texture2D.find(handle) != texture2D.end()) {
				auto tex = texture2D[handle];
				if (tex != nullptr) {
					device->destroyImageView(tex->imageView);
					vmaDestroyImage(*allocator, tex->image, tex->allocation);
				}
			}
		}

		TextureManager::~TextureManager() {
			if (stagingBufferSize != 0) {
				vmaDestroyBuffer(*allocator, stagingBuffer, stagingBufferAllocation);
			}

			// remove all texture allocations
			for (auto & it : texture2D) {
				destroyTexture(it.first);
			}
			texture2D.clear();
			device = nullptr;
		}
		gfx::vkTexture2D * TextureManager::get(gfx::Texture2DHandle handle)
		{
			if (texture2D.find(handle) != texture2D.end()) {
				return texture2D[handle].get();
			}
			return nullptr;
		}
	}
}