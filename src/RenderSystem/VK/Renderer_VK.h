#pragma once
#include <vulkan/vulkan.hpp>
#include "Setup_VK.h"
#include <mag/gfx/Mesh.h>
#include <vk_mem_alloc.h>
#include "UBO.h"
#include "SwapChain.h"
#include "RenderPass.h"
#include "Pipeline.h"
#include "VertexBuffer.h"
#include "VertexManager.h"
#include "TextureManager.h"
#include "BufferManager.h"
#include "PipelineManager.h"
#include <mag/gfx/DrawCommand.h>
#include <thread>
#include <mag/types/Result.h>
#include <mag/Window.h>
#include <memory>
namespace mag {

	class WindowSystem;
	class WindowSystemGLFW;

	struct RendererStatus {
		bool resized;
		int height;
		int width;
	};

	class ThreadData {
	public:
		vk::CommandPool commandPool;
		std::vector<vk::CommandBuffer> commandBuffers;
		std::unordered_map<gfx::ResourceSetInitializer, gfx::vkResourceSet, gfx::ResourceSetInitializerHasher> newResourceSets;
	};

	class Window;
	class Renderer {
	private:
		vk::Instance instance;
		vk::PhysicalDevice physicalDevice;
		vk::UniqueDevice device;
		VkDebugUtilsMessengerEXT debugMessenger;
		vk::Queue graphicsQueue;
		vk::SurfaceKHR surface;
		vk::Queue presentQueue;

		vk::PipelineLayout pipelineLayout;
		std::vector<gfx::vkResourceSet> discardResourceSets;

		std::vector<vk::CommandBuffer> primaryCommandBuffers;
		vk::CommandPool mainCommandPool;
		std::unordered_map<std::thread::id, ThreadData> threadMap;

		unsigned int max_frames = 3;
		unsigned int current_frame = 0;

		// queue indices
		QueueFamilyIndices queueIndices;

		// semaphores
		std::vector<vk::Semaphore> imageAvailable, renderFinished;
		std::vector<vk::Fence> inFlightFences;

		// std::shared_ptr<Window> activeWindow;
		WindowHandle activeWindow;
		std::shared_ptr<WindowSystemGLFW> windowSystem;

		// swap chain
		std::unique_ptr<gfx::SwapChain> swapChain;
		void cleanupSwapChain();
		void prepareSwapChain();
		
		void destroyDuplicateResourceSets();

		// set up validation for debugging
		#ifdef NDEBUG
		const bool enableValidationLayers = false;
		#else 
		const bool enableValidationLayers = true;
		#endif

		static void framebufferResizeCallback(GLFWwindow* window, int width, int height);

		vk::Sampler textureSampler;

		//gfx::vkTexture2D * pTexture;

		// Managers
		std::unique_ptr<gfx::TextureManager> textureManager;
		std::unique_ptr<gfx::VertexManager> vertexManager;
		std::unique_ptr<gfx::BufferManager> bufferManager;
		std::unique_ptr<gfx::PipelineManager> pipelineManager;
		vk::Image depthImage;
		vk::ImageView depthImageView;
		VmaAllocation depthImageAllocation;

		// Vulkan Memory Allocator
		VmaAllocator allocator;

		// texture stuff
		std::vector<vk::CommandBuffer> beginSingleTimeCommands() {
			vk::CommandBufferAllocateInfo allocInfo(mainCommandPool, vk::CommandBufferLevel::ePrimary, 1);

			auto cmdbufs = device->allocateCommandBuffers(allocInfo);

			vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

			cmdbufs[0].begin(beginInfo);
			return cmdbufs;
		}

		void endSingleTimeCommands(std::vector<vk::CommandBuffer> cmdbufs) {
			cmdbufs[0].end();

			vk::SubmitInfo submitInfo;
			submitInfo.commandBufferCount = 1;
			submitInfo.pCommandBuffers = cmdbufs.data();
			graphicsQueue.submit(submitInfo, nullptr);
			graphicsQueue.waitIdle();
			device->freeCommandBuffers(mainCommandPool, cmdbufs);
		}

		void createCommandPools(std::vector<std::thread::id> & threadIDs) {
			// command buffer pool
			vk::CommandPoolCreateInfo  poolInfo(vk::CommandPoolCreateFlagBits::eResetCommandBuffer, queueIndices.graphicsFamily.value());
			for (auto & id : threadIDs) {
				RegisterWorker(id);
			}

			// main thread
			mainCommandPool = device->createCommandPool(poolInfo);
		}

		void destroyCommandPools() {

			device->destroyCommandPool(mainCommandPool);

			for (auto & it : threadMap) {
				device->destroyCommandPool(it.second.commandPool);
			}
		}

		void copyBuffer(vk::Buffer src, vk::Buffer dst, vk::DeviceSize size) {
			auto cmdBufs = beginSingleTimeCommands();
			vk::BufferCopy copyRegion = {};
			copyRegion.size = size;
			cmdBufs[0].copyBuffer(src, dst, copyRegion);
			endSingleTimeCommands(cmdBufs);
		}



		void createTextureSampler() {
			vk::SamplerCreateInfo samplerInfo({}, vk::Filter::eLinear, vk::Filter::eLinear);
			samplerInfo.addressModeU = vk::SamplerAddressMode::eRepeat;
			samplerInfo.addressModeV = vk::SamplerAddressMode::eRepeat;
			samplerInfo.addressModeW = vk::SamplerAddressMode::eRepeat;

			samplerInfo.anisotropyEnable = VK_TRUE;
			samplerInfo.maxAnisotropy = 16;
			samplerInfo.borderColor = vk::BorderColor::eIntOpaqueBlack;
			samplerInfo.unnormalizedCoordinates = VK_FALSE;

			samplerInfo.compareEnable = VK_FALSE;
			samplerInfo.compareOp = vk::CompareOp::eAlways;

			// mip mapping stuff
			samplerInfo.mipmapMode = vk::SamplerMipmapMode::eLinear;
			samplerInfo.mipLodBias = 0.0f;
			samplerInfo.minLod = 0.0f;
			samplerInfo.maxLod = 0.0f;

			textureSampler = device->createSampler(samplerInfo);
		}

		// mouse
		//vec4 cameraPos;

		// ubo stuff
		std::vector<UniformBufferObject> ubos;
		vk::DescriptorSetLayout descriptorSetLayout;
		vk::DescriptorPool descriptorPool;
		std::vector<vk::DescriptorSet> descriptorSets;
		std::unique_ptr<gfx::RenderPass> mainRenderPass;
		std::unique_ptr<gfx::vkGraphicsPipeline> graphicsPipeline;

		// Resource Set management
		gfx::vkResourceSet * getResourceSet(std::thread::id workerID, gfx::ResourceSetInitializer & initializer);
		void mergeResourceSets();

		RendererStatus status;
		void RegisterWorker(std::thread::id);

	public:
		Renderer(std::vector<std::thread::id>& threadIDs, WindowHandle window, std::shared_ptr<WindowSystem> windowSystem);
		~Renderer();
		//void StartUp(std::vector<std::thread::id> & threadIDs, WindowHandle window, std::shared_ptr<WindowSystem> windowSystem);
		void ShutDown();

		RendererStatus Update();
		void SetWindow(WindowHandle window);
		void recreateSwapChain();
		gfx::Texture2DHandle InsertTexture(gfx::Texture2DHandle handle, gfx::Texture2DAsset * texture);

		PrimitiveResult<gfx::MeshPrimitiveHandle> uploadPrimitive(gfx::MeshPrimitiveHandle handle, const gfx::MeshPrimitive & primitive);
		void destroyPrimitive(gfx::MeshPrimitiveHandle handle);
		// Recording and Drawing
		void RecordingSetup();
		void RecordDrawCommands(std::vector<gfx::DrawCommand> & drawCommands);
		void RecordingFinish();
		bool createGraphicsPipeline(gfx::GraphicsPipelineStateInitializer & initializer);
		gfx::vkResourceSet initializeResourceSet(gfx::ResourceSetInitializer & initializer);
		bool createUniformBuffer(gfx::UniformBufferHandle handle, size_t size);
		void setUniformBuffer(gfx::UniformBufferHandle handle, const void * src);
		size_t setTransformData(void * src, size_t size);
	};
}