#pragma once
#include <vulkan/vulkan.hpp>
#include <RenderSystem/VK/Shader_VK.h>
#include <mag/gfx/PipelineState.h>
#include <mag/gfx/ResourceSet.h>
namespace mag {
	namespace gfx {

		class vkResourceSet {
		public:
			ResourceSetInitializer initializer;
			std::vector<vk::DescriptorSet> descriptorSets;

			vk::DescriptorPool descriptorPool;

			unsigned int discardFrames = 0;
		};

		class vkGraphicsPipelineSetupInfo {
		public:
			Shader shader;
			float height, width;
			vk::Extent2D extent;
			std::vector<vk::DescriptorSetLayout> descriptorSetLayouts;
			vk::RenderPass renderPass;

			// vertex stuff
			std::vector<vk::VertexInputBindingDescription> bindingDescriptions;
			std::vector<vk::VertexInputAttributeDescription> attributeDescriptions;
			vk::PipelineLayout pipelineLayout;

			GraphicsPipelineStateInitializer initializer;
		};

		class vkGraphicsPipeline {
			friend class PipelineManager;
		private:
			vk::Pipeline pipeline;
		public:
			vkGraphicsPipeline(vk::Device * device, const vkGraphicsPipelineSetupInfo & info);
			vkGraphicsPipeline();
			//std::vector<vkResourceSet> resourceSets;
			std::vector<vk::DescriptorSetLayout> descriptorSetLayout;
			vk::PipelineLayout pipelineLayout;

			std::vector<vk::VertexInputAttributeDescription> attributeDescriptions;
			std::vector<vk::VertexInputBindingDescription> bindingDescriptions;
			std::vector<vk::DescriptorSetLayoutBinding> layoutBindings;
			//~vkGraphicsPipeline();
			vk::Pipeline & get();
			//vk::DescriptorSet * get(gfx::ResourceSetInitializer & resourceBinding, unsigned int current_frame);
			//void AddDescriptorSet(gfx::GraphicsPipelineHandle handle, gfx::ResourceSetInitializer & resourceBinding, std::vector<vk::DescriptorSet> & descriptorSets);
		};
	}
}