#pragma once
#include <vulkan/vulkan.hpp>
#include <mag/gfx/Texture.h>
#include "Texture.h"
#include <vk_mem_alloc.h>
#include <unordered_map>
namespace mag {
	namespace gfx {
		class TextureManager {
		private:
			VmaAllocator * allocator;
			std::unordered_map<gfx::Texture2DHandle, std::shared_ptr<vkTexture2D>, HandleHasher<Texture2DHandle>> texture2D;
			VkBuffer stagingBuffer;
			VmaAllocation stagingBufferAllocation; 
			void resizeStagingBuffer(size_t length);
			size_t stagingBufferSize;
			vk::Device * device;
			//vk::Queue * graphicsQueue;
			//std::vector<vk::CommandPool> commandPools;
			//vk::CommandPool commandPool;
			// Functions
			//gfx::Texture2DHandle lastTexture2DHandle;
			void copyBufferToImage(vk::CommandBuffer & commandBuffer, vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height);
			void destroyTexture(gfx::Texture2DHandle handle);
		public:
			TextureManager(vk::Device * pDevice, VmaAllocator * newAllocator);
			vkTexture2D * create2D(gfx::Texture2DHandle handle, gfx::Texture2DAsset * data, vk::CommandBuffer & singleTimeCommandBuffer);
			void destroy(gfx::Texture2DHandle handle);
			//void DoSingleTimeCommands(vk::CommandBuffer & commandBuffer);
			~TextureManager();
			vk::ImageView createImageView(vk::Image & image, vk::Format format, vk::ImageAspectFlagBits aspectFlags);
			void transitionImageLayout(vk::CommandBuffer & commandBuffer, vk::Image image, vk::Format format, vk::ImageLayout oldLayout, vk::ImageLayout newLayout);

			gfx::vkTexture2D * get(gfx::Texture2DHandle handle);
		};
	}
}