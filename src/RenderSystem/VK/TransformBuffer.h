#pragma once
#include <vulkan/vulkan.hpp>
#include <vk_mem_alloc.h>
#include <mag/types/Handle.h>
namespace mag {
	namespace gfx {

		class vkTransformBuffer {
		public:

			vkTransformBuffer() {
				size = 0;
				used = 0;
				num_buffers = 0;
			}

			vk::Buffer buffer;
			VmaAllocation allocation;

			// size of memory for a single buffer
			size_t size;
			// amount used for a single buffer (only refers to current writeable buffer)
			size_t used;

			unsigned int num_buffers;

			bool hasCapacity(size_t desired_size) {
				return ((size - used) >= desired_size);
			}

			size_t getTotalSize() {
				return size*num_buffers;
			}

			void clear() {
				used = 0;
			}

		};

		//struct TransformBufferRecord {
		//	size_t offset;
		//};

	}
}