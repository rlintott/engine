#include "VertexManager.h"
namespace mag {
	namespace gfx {

		void VertexManager::InsertRecord(PrimitiveData & primData, VertexAttribute attribute, const mag::byte * data, size_t offset, size_t size, void * dstBuffer)
		{
			primData.attribute[attribute].offset = offset;
			primData.attribute[attribute].size = size;
			// copy to staging buffer
			memcpy(static_cast<mag::byte*>(dstBuffer)+offset, data, size);
			
		}

		size_t VertexManager::GetTotalPrimitiveSize(const gfx::MeshPrimitive & primitive) const
		{
			size_t size = 0;
			for (auto & it : primitive.attributes) {
				size += it.second.data.size();
			}
			return size;
		}

		void VertexManager::prepareStagingBuffer(size_t desiredSize)
		{
			/*
				Staging buffer
			*/
			if (stagingBufferSize >= desiredSize) {
				return;
			}

			if (stagingBufferSize != 0) {
				// deallocate staging buffer
				vmaDestroyBuffer(*pAllocator, stagingBuffer, stagingBufferAllocation);

			}

			size_t newBufferSize = 0;
			if (stagingBufferSize < desiredSize) {
				if (desiredSize < 100) {
					newBufferSize = 100;
				}
				else {
					newBufferSize = desiredSize * 2;
				}
			}

			stagingBufferSize = newBufferSize;

			vk::BufferCreateInfo bufferInfo({}, stagingBufferSize, vk::BufferUsageFlagBits::eTransferSrc);
			VmaAllocationCreateInfo aci = {};
			aci.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
			aci.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
			
			VkBufferCreateInfo bufferCreateInfo = bufferInfo;
			vmaCreateBuffer(*pAllocator, &bufferCreateInfo, &aci, (VkBuffer*)&stagingBuffer, &stagingBufferAllocation, nullptr);
		}

		void VertexManager::copyBuffer(vk::Buffer src, vk::Buffer dst, vk::DeviceSize size, vk::CommandBuffer & commandBuffer)
		{
			vk::BufferCopy copyRegion = {};
			copyRegion.size = size;
			commandBuffer.copyBuffer(src, dst, copyRegion);
		}

		VertexManager::VertexManager(VmaAllocator * pAllocator, uint32_t max_frames_in_flight)
		{
			this->pAllocator = pAllocator;
			stagingBufferSize = 0;
			max_frames = max_frames_in_flight;
		}
		VertexManager::~VertexManager()
		{
			// clear all vertex buffers
			for (auto & it : primitives) {
				if (it.second.hasIndices) {
					vmaDestroyBuffer(*pAllocator, it.second.indexBuffer, it.second.indexBufferAllocation);
				}
				vmaDestroyBuffer(*pAllocator, it.second.vertexBuffer, it.second.vertexBufferAllocation);
			}
			// clear staging buffer
			vmaDestroyBuffer(*pAllocator, stagingBuffer, stagingBufferAllocation);
		}
		void VertexManager::uploadPrimitive(gfx::MeshPrimitiveHandle & handle, const gfx::MeshPrimitive & primitive, vk::CommandBuffer & commandBuffer)
		{
			auto itPrimitive = primitives.find(handle);
			if (itPrimitive != primitives.end()) {
				// destroy buffer and remove from primitives map
				destroyPrimitive(handle);
			}

			// populate map with new primitive data at this handle
			primitives[handle].layout = VertexLayout::NoneBit;
			itPrimitive = primitives.find(handle);
			auto& primitiveData = itPrimitive.value();



			prepareStagingBuffer(GetTotalPrimitiveSize(primitive));

			size_t requiredSize = 0;
			void * ptr;
			vmaMapMemory(*pAllocator, stagingBufferAllocation, &ptr);

			// get/store vertex input attribute descriptions
			// std::vector<vk::VertexInputAttributeDescription> desc;
			std::vector<vk::VertexInputBindingDescription> bindingDesc;
			for (auto & itAttribute : primitive.attributes) {

				size_t offset = requiredSize;
				size_t attributeSize = itAttribute.second.data.size();
				requiredSize += attributeSize;

				switch (itAttribute.first) {
				case VertexAttribute::Position:
					primitiveData.layout |= VertexLayout::PositionBit;
					InsertRecord(primitiveData, itAttribute.first, itAttribute.second.data.data(), offset, attributeSize, ptr);
					// desc.emplace_back((0, 0, vk::Format::eR32G32B32Sfloat, 0));
					bindingDesc.emplace_back(vk::VertexInputBindingDescription(0, sizeof(mag::vec3), vk::VertexInputRate::eVertex));
					break;
				case VertexAttribute::Normal:
					primitiveData.layout |= VertexLayout::NormalBit;
					InsertRecord(primitiveData, itAttribute.first, itAttribute.second.data.data(), offset, attributeSize, ptr);
					// desc.emplace_back((1, 1, vk::Format::eR32G32B32Sfloat, 0));
					bindingDesc.emplace_back(vk::VertexInputBindingDescription(1, sizeof(mag::vec3), vk::VertexInputRate::eVertex));
					break;
				case VertexAttribute::Tangent:
					primitiveData.layout |= VertexLayout::TangentBit;
					InsertRecord(primitiveData, itAttribute.first, itAttribute.second.data.data(), offset, attributeSize, ptr);
					// desc.emplace_back((3, 3, vk::Format::eR32G32B32Sfloat, 0));
					bindingDesc.emplace_back(vk::VertexInputBindingDescription(3, sizeof(mag::vec3), vk::VertexInputRate::eVertex));
					break;
				case VertexAttribute::UV0:
					primitiveData.layout |= VertexLayout::UV0Bit;
					InsertRecord(primitiveData, itAttribute.first, itAttribute.second.data.data(), offset, attributeSize, ptr);
					// desc.emplace_back((2, 2, vk::Format::eR32G32B32Sfloat, 0));
					bindingDesc.emplace_back(vk::VertexInputBindingDescription(2, sizeof(mag::vec3), vk::VertexInputRate::eVertex));
					break;
				default:
					throw std::runtime_error("\nUnrecognized attribute type!");
				}

			}

			vmaUnmapMemory(*pAllocator, stagingBufferAllocation);
			{
				// allocate vertex buffer and copy to it from staging buffer
				VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
				bufferInfo.size = requiredSize;
				bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
				VmaAllocationCreateInfo allocInfo = {};
				allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
				//allocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
				vmaCreateBuffer(*pAllocator, &bufferInfo, &allocInfo, (VkBuffer*)&(primitiveData.vertexBuffer), &(primitiveData.vertexBufferAllocation), nullptr);
			}
			copyBuffer(stagingBuffer, itPrimitive->second.vertexBuffer, requiredSize, commandBuffer);

			if (primitive.indices.num_elements > 0) {
				primitiveData.hasIndices = true;
				{

					// set format for index buffer

					if (primitive.indices.format == gfx::ushort) {
						primitiveData.indexType = vk::IndexType::eUint16;
						primitiveData.num_elements = static_cast<uint32_t>(primitive.indices.num_elements); // / sizeof(uint16_t);
					}
					else if (primitive.indices.format == gfx::uint) {
						primitiveData.indexType = vk::IndexType::eUint32;
						primitiveData.num_elements = static_cast<uint32_t>(primitive.indices.num_elements);
					}

					void * data;
					vmaMapMemory(*pAllocator, stagingBufferAllocation, &data);

					memcpy(data, primitive.indices.data.data(), primitive.indices.data.size());

					// copy and create index buffer
					VkBufferCreateInfo bufferInfo = { VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO };
					bufferInfo.size = requiredSize;
					bufferInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
					VmaAllocationCreateInfo allocInfo = {};
					allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
					allocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
					vmaCreateBuffer(*pAllocator, &bufferInfo, &allocInfo, (VkBuffer*)&(primitiveData.indexBuffer), &(primitiveData.indexBufferAllocation), nullptr);

					vmaUnmapMemory(*pAllocator, stagingBufferAllocation);
				}
				copyBuffer(stagingBuffer, itPrimitive->second.indexBuffer, requiredSize, commandBuffer);
			}
			else {
				primitiveData.num_elements = primitive.attributes.begin()->second.num_elements;
			}
		}
		void VertexManager::destroyPrimitive(gfx::MeshPrimitiveHandle handle)
		{
			auto it = primitives.find(handle);
			if (it != primitives.end()) {
				destroyedPrimitives[handle] = it.value();
				primitives.erase(handle);
			}
		}
		const PrimitiveData * VertexManager::get(gfx::MeshPrimitiveHandle handle) noexcept
		{
			auto it = primitives.find(handle);
			if (it != primitives.end()) {
				return &it->second;
			}
			return nullptr;
		}
		void VertexManager::destroyPrimitives() noexcept
		{
			// for each destroyedPrimitive
			std::vector<gfx::MeshPrimitiveHandle> destroyedHandles;
			for (auto it = destroyedPrimitives.begin(); it != destroyedPrimitives.end(); it++) {
				// actually destroy them if they are not in use
				if (it->second.num_frames_since_deleted > max_frames) {
					if (it->second.hasIndices) {
						vmaDestroyBuffer(*pAllocator, it->second.indexBuffer, it->second.indexBufferAllocation);
					}

					vmaDestroyBuffer(*pAllocator, it->second.vertexBuffer, it->second.vertexBufferAllocation);

					destroyedHandles.push_back(it->first);
				}
				it.value().num_frames_since_deleted++;
			}

			for (auto handle : destroyedHandles) {
				destroyedPrimitives.erase(handle);
			}

		}
	}
}