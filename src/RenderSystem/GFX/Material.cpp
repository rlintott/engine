#include <mag/gfx/Material.h>

namespace mag {
	namespace gfx {
		Material::Material() {
			castShadows = true;
			receiveShadows = true;
			name = "Material";
			method = RenderingMethod::Opaque;
		}
	}
}