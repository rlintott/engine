#include <mag/gfx/Mesh.h>

namespace mag {
	namespace gfx {
		VertexLayout & operator |= (VertexLayout & a, const VertexLayout & b) {
			//return (VertexLayout)(a | b);
			a = (VertexLayout)(a | b);
			return a;
		}
	}
}