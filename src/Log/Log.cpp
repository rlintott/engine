#include <mag/log/Log.h>

namespace mag {
	namespace log {

		void set_level(Level level) {
			switch (level) {
			case Level::Debug:
				spdlog::set_level(spdlog::level::debug);
				break;
			default:
				spdlog::set_level(spdlog::level::info);
			}
		}

	}
}