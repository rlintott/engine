#include <mag/utility/Hash.h>
#include <xxhash.h>
namespace mag {
	size_t byteHash(const void* input, size_t len)
	{
		return  XXH64(input, len, 0);
	}
}