#pragma once
#include <entt/entt.hpp>
namespace mag {
	typedef entt::identifier<> Entity;
	class EntitySystem {
	public:
		/*
			Members
		*/
		entt::registry registry;
	};
}