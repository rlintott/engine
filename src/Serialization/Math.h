#pragma once
namespace mag {
		template<typename Archive> void serialize(Archive& archive, mag::vec3& v3)
		{
			archive(cereal::make_nvp("x", v3.x), cereal::make_nvp("y", v3.y), cereal::make_nvp("z", v3.z));
		}

		template<typename Archive> void serialize(Archive& archive, mag::vec4& v)
		{
			archive(cereal::make_nvp("x", v.x), cereal::make_nvp("y", v.y), cereal::make_nvp("z", v.z), cereal::make_nvp("w", v.w));
		}
}

//namespace glm {
//	template<typename Archive> void serialize(Archive& archive, glm::vec4& v)
//	{
//		archive(cereal::make_nvp("x", v.x), cereal::make_nvp("y", v.y), cereal::make_nvp("z", v.z), cereal::make_nvp("w", v.w));
//	}
//}