#pragma once
#include <mag/gfx/Material.h>
//#include <cereal/types/base_class.hpp>
//#include <cereal/types/polymorphic.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>

namespace mag {
	namespace gfx {



		template<typename Archive> void serialize(Archive& archive, gfx::UBOInfo & val)
		{
			archive(val.binding, val.size);
		}

		template<typename Archive> void serialize(Archive& archive, gfx::ShaderInfo & val)
		{
			archive(val.shaderCode, val.vertCode, val.fragCode, val.uniformBuffers, val.textures, val.samplers, val.vertexAttributes);
		}

		template<typename Archive> void serialize(Archive& archive, gfx::MaterialParameterInfo & val)
		{
			archive(val.name, val.type);
		}

		template<typename Archive> void serialize(Archive& archive, gfx::Material & val)
		{
			archive(val.name, val.drawMode, val.method, val.castShadows, val.receiveShadows, val.shaderInfo, val.parameters, val.textureSlots, val.vert_normal, val.vert_uv0, val.vert_tangent);
		}

		// Absolutely necessary even though empty!
		//template<typename Archive> void serialize(Archive& archive, gfx::MaterialParameter& val)
		//{
		//}

		//template<typename Archive> void serialize(Archive& archive, gfx::MPColor & val)
		//{
		//	archive(val.value);
		//}

		template<typename Archive> void serialize(Archive& archive, gfx::MaterialInstanceCreateInfo & val)
		{
			archive(cereal::make_nvp("name",val.name), cereal::make_nvp("package",val.package), cereal::make_nvp("material",val.material), cereal::make_nvp("texture2D",val.texture2D));
		}
	}
}

//CEREAL_REGISTER_TYPE(mag::gfx::MPColor)
//CEREAL_REGISTER_POLYMORPHIC_RELATION(mag::gfx::MaterialParameter, mag::gfx::MPColor)