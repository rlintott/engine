#pragma once
#include <mag/gfx/Texture.h>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>
namespace mag {
	namespace gfx {

		template<typename Archive> void serialize(Archive& archive, gfx::Texture2DAsset & val)
		{
			archive(val.name, val.data, val.numChannels, val.height, val.width);
		}


	}
}

