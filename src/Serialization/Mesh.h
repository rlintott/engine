#pragma once
#include <mag/gfx/Mesh.h>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/vector.hpp>
namespace mag {
	namespace gfx {

		template<typename Archive> void serialize(Archive& archive, gfx::VertexAttributeData& val)
		{
			archive(val.format, val.data, val.num_elements);
		}

		template<typename Archive> void serialize(Archive& archive, gfx::MeshPrimitive& val)
		{
			archive(val.attributes, val.indices, val.materialID);
		}

		template<typename Archive> void serialize(Archive& archive, gfx::Mesh & val)
		{
			archive(val.name, val.primitives);
		}







	}
}

