#pragma once
#include <AssetSystem/Asset.h>
//#include <cereal/cereal.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <Serialization/gfx.h>
namespace mag {
	template<typename Archive> void serialize(Archive& archive, mag::AssetPackage & val)
	{
			archive(val.meshes, val.materials, val.material_instances, val.texture2D);
	}
}
