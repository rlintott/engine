#include "TransformSystem.h"
#include <mag/EntitySystem.h>
#include <mag/components/MeshInstance.h>
#include <mag/JobSystem.h>
#include <mag/utility/Subrange.h>
#include <mag/JobGraph.h>
#include <mag/log/Log.h>
namespace mag {
	TransformSystem::TransformSystem(std::shared_ptr<EntitySystem> pEntitySystem, std::shared_ptr<JobSystem> pJobSystem) : entitySystem(pEntitySystem), jobSystem(pJobSystem)
	{
	}
	void TransformSystem::FixedUpdate(FrameConstants frameConstants)
	{

		auto jobGraph = jobSystem->createGraph();
		auto view = entitySystem->registry.view<Transform>();

		jobGraph->parallelFor(
			view.begin(),
			view.end(),
			[view](unsigned int& entity) {
				auto& transform = view.get(entity);

				// update last transform (for interpolation)
				transform.lastPosition = transform.position;
				transform.lastRotation = transform.rotation;
				transform.lastScale = transform.scale;

				// update worldspace transform
				transform.position = transform.localPosition;
				transform.rotation = transform.localRotation;
				transform.scale = transform.localScale;
			}
		);

		jobSystem->submit();
		jobGraph->wait();

	}
}