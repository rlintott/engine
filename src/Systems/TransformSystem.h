#pragma once
#include <mag/components/Transform.h>
#include <mag/FrameConstants.h>
#include <memory>
namespace mag {
	class EntitySystem;
	class JobSystem;
	class TransformSystem {
	private:
		std::shared_ptr<EntitySystem> entitySystem;
		std::shared_ptr<JobSystem> jobSystem;
	public:
		TransformSystem(std::shared_ptr<EntitySystem> pEntitySystem, std::shared_ptr<JobSystem> pJobSystem);
		void FixedUpdate(FrameConstants frameConstants);
	};
}