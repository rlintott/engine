#include <mag/JobSystem.h>
#include <mag/JobGraph.h>
#include <mag/log/Log.h>
namespace mag {

	JobSystem::JobSystem() {
		terminated = false;
		workGroup = new WorkGroup();

		int num_cores = std::thread::hardware_concurrency(); // includes physical & logical cores
		int num_workers = num_cores - 2;

		//if (num_workers < 0) {
		//	num_workers = 2;
		//}

		//int num_workers = 3;

		workGroup->Initialize(num_workers);
	}

	JobSystem::~JobSystem()
	{
		if (!terminated)
		{
			terminate();
		}
	}

	std::shared_ptr<JobGraph> JobSystem::createGraph()
	{
		auto graph = std::shared_ptr<JobGraph>(new JobGraph(GetNumWorkers()));
		jobGraphs.push_back(graph);
		return graph;
	}

	void JobSystem::terminate()
	{
		if (workGroup) {
			workGroup->Deinitialize();
		}
		terminated = true;
		mag::log::info("All workers shut down!");
	}

	void JobSystem::submit()
	{
		// submit job graphs to worker threads
		workGroup->addJobGraphs(jobGraphs);
		// prune old graphs
		incomplete_graphs.remove_if([](const auto& graph) { return graph->isCompleted(); });
		// copy new graphs (preserve lifetime)
		incomplete_graphs.insert(incomplete_graphs.end(), jobGraphs.begin(), jobGraphs.end());
		jobGraphs.clear();
	}

	size_t JobSystem::GetNumWorkers() const noexcept
	{
		return workGroup->workers.size();
	}
	
}