#include <mag/Worker.h>
#include <mag/JobSystem.h>
#include <iostream>
#include <mag/JobGraph.h>
#include <mag/log/Log.h>
//#include <array>
//#include <vector>
#include <list>
namespace mag {

	Worker::Worker(WorkerQueue& queue): queue(queue)
	{
		stop = false;
		thread = std::thread(&Worker::Update, this);
		log::info("Worker spawned!");
	}

	// infinite loop
	void Worker::Update()
	{

		std::list<std::shared_ptr<JobNode>> incomplete_jobs;

		while (!stop) {

			std::shared_ptr<JobNode> new_job;
			if (queue.try_pop(new_job))
			{
				incomplete_jobs.push_back(new_job);
			}

			incomplete_jobs.remove_if([](auto& job) {
				job->traverse();
				return job->finished();
			});


		}

		log::info("Worker terminated.");
	}
	void Worker::Stop()
	{
		stop = true;
		thread.join();
	}


	//void Worker::addJobGraph(std::shared_ptr<JobGraph> & jobGraph)
	//{
	//	while(!HasSpace());
	//	buffer.push(jobGraph);
	//}


	//bool Worker::HasSpace()
	//{

	//	return !buffer.full();

	//}
	std::thread::id Worker::getThreadID()
	{
		return thread.get_id();
	}
	WorkGroup::WorkGroup(): queue(100)
	{
	}
	void WorkGroup::Initialize(unsigned int num_workers)
	{

		for (int i = 0; i < num_workers; i++) {
			workers.push_back(std::unique_ptr<Worker>(new Worker(queue)));
		}

	}
	void WorkGroup::Deinitialize()
	{
		// we need to shutdown every thread
		// we can do this by setting a bool to break/exit their loops
		// then wait for each thread to "join", clear the worker vector, and clean up any other memory
		for (auto & worker : workers) {
			worker->Stop();
		}
		workers.clear();
	}
	void WorkGroup::addJobGraphs(std::vector<std::shared_ptr<JobGraph>>& jobGraphs)
	{

		for (auto & graph: jobGraphs)
		{
			for (auto& job : graph->jobs)
			{
				queue.push(job);
			}
		}

		//if (jobGraphs.size() > 0) {
		//	for (auto && graph : jobGraphs) {
		//		for (auto && worker : workers) {
		//			worker->addJobGraph(graph);
		//		}
		//	}
		//}
	}
	

}