#pragma once
#include <thread>
#include <vector>
//#include <mag/containers/RingBuffer.h>
#include <memory>
#include <atomic>
#include <rigtorp/MPMCQueue.h>
namespace mag {

	class JobGraph;
	class JobNode;
	using WorkerQueue = rigtorp::MPMCQueue<std::shared_ptr<JobNode>>;
	class Worker {
	private:
		std::thread thread;
		std::atomic<bool> stop;
		static constexpr size_t listBufferSize = 16;
		static constexpr size_t localStackSize = 4;

		//RingBuffer<std::shared_ptr<JobGraph>, listBufferSize> buffer;
		WorkerQueue& queue;

	public:
		Worker(WorkerQueue& queue);
		void Update();
		void Stop();

		//void addJobGraph(std::shared_ptr<JobGraph> & jobGraph);
		//bool HasSpace();
		std::thread::id getThreadID();

	};

	class WorkGroup {
	private:
		WorkerQueue queue;
	public:
		std::vector<std::unique_ptr<Worker>> workers;
		WorkGroup();
		void Initialize(unsigned int num_workers);
		void Deinitialize();
		void addJobGraphs(std::vector<std::shared_ptr<JobGraph>> & jobGraphs);

	};


}