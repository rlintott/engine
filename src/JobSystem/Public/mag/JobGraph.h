#pragma once
#include <atomic>
#include <vector>
#include <memory>
#include <functional>
namespace mag {

	class JobGraph;

	class JobNode {
	private:

		JobGraph * jobGraph;
		size_t ID;

		std::atomic<bool> completed;
		std::atomic<bool> claimed;
		std::atomic<size_t> numRequiredCompleted;

		std::function<void()> func;
		std::vector<size_t> required;
		std::vector<size_t> dependents;

		void attempt();
	public:
		JobNode(JobGraph * jobGraph, size_t ID, std::function<void()> func);
		void requires(std::shared_ptr<JobNode> & node);
		void traverse();
		bool finished();
	};

	class JobGraph {
		friend class JobNode;
		friend class JobSystem;
		friend class WorkGroup;
	private:
		std::atomic<size_t> numCompleted;
		std::vector <std::shared_ptr<JobNode>> jobs;
		size_t num_workers;
		JobGraph(size_t num_workers);
	public:

		bool isCompleted() const noexcept;

		void execute();

		void wait();

		/*
			Create job graph node from task.
		*/
		template <typename Function>
		std::shared_ptr<JobNode> add(Function task)
		{
			std::shared_ptr<JobNode> node(new JobNode(this, jobs.size(), task));
			jobs.push_back(node);
			return node;

		}

		/*
			First version of jobsystem specific parallel-for
		*/
		template <typename IteratorType, typename Function>
		void parallelFor(IteratorType begin, IteratorType end, Function task, std::initializer_list<std::shared_ptr<JobNode>> required = {}, std::initializer_list<std::shared_ptr<JobNode>> dependents = {}) {
			size_t size = std::distance(begin, end);
			auto subranges = calculateSubranges(begin, end, size, num_workers);
			
			for (auto subrange : subranges) {
				// queue up the task for this subrange
				auto job = add(
					[subrange, task]() {
						for (auto it = subrange.begin; it != subrange.end; it++) {
							auto & element = const_cast<typename std::iterator_traits<IteratorType>::value_type &>(*it);
							task(element);
						}
					}
				);

				// set job pre-requisites
				for (auto req : required) {
					job->requires(req);
				}

				// make dependents require this job
				for (auto& dependent : dependents) {
					dependent->requires(job);
				}
			}

		}
	};


}