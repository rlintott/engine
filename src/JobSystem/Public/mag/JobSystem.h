#pragma once
#include <vector>
#include <atomic>
#include <mag/Worker.h>
#include <functional>
#include <deque>
#include <list>
#include <mag/utility/Subrange.h>
namespace mag {

	class JobGraph;

	class JobSystem {
	private:
		bool terminated;
	public:
		std::vector<std::shared_ptr<JobGraph>> jobGraphs;
		std::list<std::shared_ptr<JobGraph>> incomplete_graphs;
		WorkGroup * workGroup;

		JobSystem();
		~JobSystem();

		std::shared_ptr<JobGraph> createGraph();
		void terminate();
		void submit();
		size_t GetNumWorkers() const noexcept;
	};

}