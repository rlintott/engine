#include <mag/JobGraph.h>
#include <mag/log/Log.h>
namespace mag {

	JobNode::JobNode(JobGraph * jobGraph, size_t ID, std::function<void()> func)
	{
		this->jobGraph = jobGraph;
		this->ID = ID;
		this->func = func;

		claimed = false;
		completed = false;
		numRequiredCompleted = 0;

	}

	void JobNode::requires(std::shared_ptr<JobNode> & node)
	{
		required.push_back(node->ID);
		node->dependents.push_back(ID);
	}

	void JobNode::traverse()
	{
		if (claimed) {
			return;
		}

		//if (numRequiredCompleted < required.size()) { // || required.size() > 0) {
		//	for (const auto & reqID : required) {
		//		jobGraph->jobs[reqID]->traverse();
		//	}
		//}
		//else
		if (numRequiredCompleted >= required.size())
		{
			attempt();
		}
		
		//if (numRequiredCompleted >= required.size()) {
		//	attempt();
		//}
	}

	bool JobNode::finished()
	{
		return completed;
	}

	void JobNode::attempt()
	{
		bool expected = false;
		if (claimed.compare_exchange_weak(expected, true)) {
			//mag::log::info("doing job!");
			func();
			completed = true;
			for (auto & dependent : dependents) {
				jobGraph->jobs[dependent]->numRequiredCompleted++;
			}
			jobGraph->numCompleted++;
		}
	}
	JobGraph::JobGraph(size_t num_workers)
	{
		numCompleted = 0;
		this->num_workers = num_workers;
	}
	bool JobGraph::isCompleted() const noexcept
	{
		return numCompleted == jobs.size();
	}
	void JobGraph::execute()
	{

		for (auto && it : jobs) {
			it->traverse();
		}

	}
	void JobGraph::wait()
	{
		while (!isCompleted());
	}

}