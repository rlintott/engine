#pragma once
#include <string>
#include <spdlog/spdlog.h>
namespace mag {
	/**
	 * @brief Contains logging functions.
	 * 
	 */
	namespace log {

		enum class Level {
			Debug
		};

		/**
		 * @brief Set the log level.
		 * 
		 * @param level 
		 */
		void set_level(Level level);
		
		/**
		 * @brief Log information.
		 * 
		 * @tparam MessageType 
		 * @tparam Args 
		 * @param message 
		 * @param args 
		 */
		template<typename MessageType, typename... Args>
		void info(const MessageType& message, const Args& ... args) {
			spdlog::info(message, args...);
		}

		/**
		 * @brief Log debug messages.
		 * 
		 * @tparam MessageType 
		 * @tparam Args 
		 * @param message 
		 * @param args 
		 */
		template<typename MessageType, typename... Args>
		void debug(const MessageType& message, const Args& ... args) {
			spdlog::debug(message, args...);
		}

		/**
		 * @brief Log warning messages.
		 * 
		 * @tparam MessageType 
		 * @tparam Args 
		 * @param message 
		 * @param args 
		 */
		template<typename MessageType, typename... Args>
		void warn(const MessageType& message, const Args& ... args) {
			spdlog::warn(message, args...);
		}

		/**
		 * @brief Log error messages.
		 * 
		 * @tparam MessageType 
		 * @tparam Args 
		 * @param message 
		 * @param args 
		 */
		template<typename MessageType, typename... Args>
		void error(const MessageType& message, const Args& ... args) {
			spdlog::error(message, args...);
		}
	}
}