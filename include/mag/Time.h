#include <chrono>
namespace mag {
	namespace Time {

		class Duration {
		private:
			std::chrono::duration<double> duration;
		public:
			Duration(const std::chrono::duration<double> & difference) {
				duration = difference;
			}
			//Duration & operator=(const std::chrono::duration<double> rhs) {

			//}

			operator double() const {
				return duration.count();
			}
		};

		class Point {
		private:
			std::chrono::high_resolution_clock::time_point timePoint;
		public:

			Point & operator=(const std::chrono::high_resolution_clock::time_point & rhs) {
				timePoint = rhs;
				return *this;
			};

			Duration  operator-(const Point & rhs) {
				return Duration(timePoint - rhs.timePoint);
			};

		};

		

		Point GetTime() {
			Point tp;
			tp = std::chrono::high_resolution_clock::now();
			return tp;
		}
	}
}