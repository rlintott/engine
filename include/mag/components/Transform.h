#pragma once
#include <mag/math/Vector.h>
#include <mag/math/Quaternion.h>
namespace mag {
	struct Transform {
		// local
		dvec3 localPosition;
		dquat localRotation;
		dvec3 localScale;

		// world
		dvec3 position;
		dquat rotation;
		dvec3 scale;

		// previous transform
		dvec3 lastPosition;
		dquat lastRotation;
		dvec3 lastScale;
	};

	//struct Parent {
	//	entt::identifier<> entity;
	//};
}