#pragma once
namespace mag {
	class Camera {
	public:

		Camera(): nearPlane(0.1f), farPlane(1000.0f) {}
		Camera(float paramNearPlane, float paramFarPlane) : nearPlane(paramNearPlane), farPlane(paramFarPlane) {};

		double nearPlane;
		double farPlane;
	};
}