#pragma once
#include <mag/gfx/Mesh.h>
#include <mag/gfx/Material.h>
namespace mag {
	struct MeshInstance {
		gfx::MeshPrimitiveHandle primitive;
		gfx::MaterialInstanceHandle materialInstance;
	};
}