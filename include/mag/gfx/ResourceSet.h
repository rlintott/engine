#pragma once
#include "Buffer.h"
#include "Texture.h"
#include "Sampler.h"
#include "Handles.h"
#include <mag/utility/Hash.h>
namespace mag {
	namespace gfx {


		constexpr unsigned int MAX_BUFFER_BINDINGS = 8;
		constexpr unsigned int MAX_TEXTURE_BINDINGS = 8;
		constexpr unsigned int MAX_SAMPLER_BINDINGS = 8;

		template<class T, size_t capacity> class CapacityArray {
		private:
			size_t length;
			T data[capacity];
		public:

			CapacityArray() {
				length = 0;
			}


			//size_t capacity() {
			//	return capacity;
			//}

			size_t size() const {
				return length;
			}

			//T * data() {
			//	return data.data();
			//}

			const T * ptr() const {
				return &data[0];
			}

			bool operator== (const CapacityArray<T, capacity> & rhs) const {
				if (length != rhs.length) {
					return false;
				}

				for (size_t i = 0; i < length; i++) {
					if (data[i] != rhs.data[i]) {
						return false;
					}
				}

				return true;
			}

			T & operator[] (size_t idx) {
				return data[idx];
			}

			void push_back(const T & val) {
				if (length < capacity) {
					data[length] = val;
					length++;
				}
			}

		};


		struct UniformBufferBinding {
			UniformBufferHandle handle;
			unsigned int binding;

			bool operator== (const UniformBufferBinding & rhs) const {
				return (handle == rhs.handle && binding == rhs.binding);
			}

			bool operator!= (const UniformBufferBinding & rhs) const {
				return !( *this == rhs);
			}
		};

		class TextureBinding {
		public:
			gfx::Texture2DHandle handle;
			unsigned int binding;

			bool operator== (const TextureBinding & rhs) const {
				return (handle == rhs.handle && binding == rhs.binding);
			}

			bool operator!= (const TextureBinding & rhs) const {
				return !(*this == rhs);
			}
		};

		class SamplerBinding {
		public:
			gfx::SamplerHandle handle;
			unsigned int binding;

			bool operator== (const SamplerBinding & rhs) const {
				return (handle == rhs.handle && binding == rhs.binding);
			}

			bool operator!= (const SamplerBinding & rhs) const {
				return !(*this == rhs);
			}
		};

		class ResourceBinding {
		public:

			ResourceBinding() {
				binding = 0;
				uniformBuffer.value = 0;
				sampler.value = 0;
				texture2D.value = 0;
				type = Invalid;
			}

			enum ResourceType {
				Invalid,
				Sampler,
				Texture2D,
				UniformBuffer
			} type;

			unsigned int binding;

			gfx::SamplerHandle sampler;
			gfx::Texture2DHandle texture2D;
			gfx::UniformBufferHandle uniformBuffer;

			bool operator== (const ResourceBinding & rhs) const {
				if (type != rhs.type || binding != rhs.binding) {
					return false;
				}

				if (type == Sampler) {
					return sampler == rhs.sampler;
				}
				else if (type == Texture2D) {
					return texture2D == rhs.texture2D;
				}
				else if (type == UniformBuffer) {
					return uniformBuffer == rhs.uniformBuffer;
				}
				else {
					return true;
				}

			}

			bool operator!= (const ResourceBinding & rhs) const {
				return !(*this == rhs);
			}

		};

		static_assert
			( sizeof(ResourceBinding) == 
				sizeof(ResourceBinding::type) + sizeof(ResourceBinding::sampler) + sizeof(ResourceBinding::texture2D) + sizeof(ResourceBinding::uniformBuffer) + sizeof(ResourceBinding::binding)
				, "ResourceBinding is padded!");


		/*
			This is basically a description of a descriptor set, passed from the RenderSystem to the Renderer for each draw command.
		*/
		constexpr size_t MAX_RESOURCE_BINDINGS = 8 * 3;
		class ResourceSetInitializer {
		public:
			gfx::GraphicsPipelineHandle pipeline;
			CapacityArray<ResourceBinding, MAX_RESOURCE_BINDINGS> resources;

			//// uniform buffer bindings
			//CapacityArray<UniformBufferBinding, MAX_BUFFER_BINDINGS> uniformBuffers;
			//// texture bindings
			//CapacityArray<TextureBinding, MAX_TEXTURE_BINDINGS> textureBindings;
			//// sampler bindings
			//CapacityArray<SamplerBinding, MAX_SAMPLER_BINDINGS> samplerBindings;
			//// array of textures binding (for later)
			unsigned int getResourceCount(ResourceBinding::ResourceType type) {
				unsigned int count = 0;
				for (unsigned int i = 0; i < resources.size(); i++) {
					if (resources[i].type == type) {
						count++;
					}
				}
				return count;
			}
			bool operator==(const ResourceSetInitializer & rhs) const {
				return (
					//uniformBuffers == rhs.uniformBuffers &&
					//textureBindings == rhs.textureBindings &&
					//samplerBindings == rhs.samplerBindings
					pipeline == rhs.pipeline &&
					resources == rhs.resources
				);
			}

			//size_t hash() const {
			//	size_t length = 0;
			//	myth::byte buffer[sizeof(ResourceSetInitializer)];
			//	memcpy(&buffer[0])
			//}

		};

		static_assert(sizeof(ResourceSetInitializer) == sizeof(ResourceSetInitializer::pipeline) + sizeof(ResourceBinding)*MAX_RESOURCE_BINDINGS + sizeof(size_t), "ResourceSetInitializer is padded!");

		//template<typename T>
		struct ResourceSetInitializerHasher {
			size_t operator()(const ResourceSetInitializer & set) const {
				//return XXH64(set.resources.ptr(), sizeof(ResourceBinding)*set.resources.size(), 0);
				return byteHash(&set, sizeof(ResourceBinding));
			}
		};

		//class ResourceSetHandle : public Handle<uint64_t> {};
	}
}