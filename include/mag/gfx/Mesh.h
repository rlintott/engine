#pragma once
#include <mag/math/Vector.h>
#include <string>
#include <vector>
#include <mag/types/Byte.h>
#include <mag/gfx/Material.h>
#include <mag/types/gfx.h>
#include <stdexcept>
#include <mag/types/Handle.h>
#include <mag/types/Result.h>
#include <mag/types/Error.h>
//#include <tsl/robin_map.h>
#include <optional>
namespace mag {
	namespace gfx {

		class MeshPrimitiveHandle : public Handle<uint64_t> {};



		VertexLayout & operator |= (VertexLayout & a, const VertexLayout & b);



		class VertexAttributeData {
		public:

			VertexAttributeData() {
				//type = VertexAttributeType::custom;
				format = gfx::DataType::vec3;
				num_elements = 0;
			}

			//VertexAttributeType type;
			gfx::DataType format;
			//std::unique_ptr<myth::byte[]> data;
			std::vector<unsigned char> data;
			size_t num_elements;

			size_t elementSize() {
				switch (format) {
				case (gfx::vec3):
					return sizeof(mag::vec3);
				case (gfx::vec2):
					return sizeof(mag::vec2);
				case (gfx::scalar):
					return sizeof(float);
				case(gfx::uvec4):
					return sizeof(mag::uvec4);
				case(gfx::uint):
					return sizeof(mag::uint);
				case(gfx::ushort):
					return sizeof(uint16_t);
				default:
					//return sizeof(math::vec3);
					throw std::runtime_error("\nInvalid vertex attribute format!");
				}
			}

			void Set(gfx::DataType format, mag::byte * pdata, size_t byteLength) {
				this->format = format;
				num_elements = byteLength / elementSize();
				data.assign(pdata, pdata + byteLength);
			}

			bool empty() {
				if (num_elements == 0) {
					return true;
				}
				return false;
			}

			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(format, data, num_elements);
			//}

		};
		class MeshPrimitive {
		public:

			//MeshPrimitive() {
			//	//position.format = gfx::vec3;
			//	//normal.format = gfx::vec3;
			//	//indices.format = gfx::uint;
			//	//tangent.format = gfx::vec3;
			//	//uv0.format = gfx::vec2;
			//}

/*			math::vec3 position;
			math::vec4 rotation;
			math::vec3 scale*/
			//unsigned int parent;


			//VertexAttribute position;
			//VertexAttribute normal;
			VertexAttributeData indices;
			//VertexAttribute uv0;
			//VertexAttribute tangent;
			
			std::unordered_map<gfx::VertexAttribute, VertexAttributeData> attributes;
			//std::array<std::optional<gfx::VertexAttributeData>, gfx::VertexAttribute::Count> attributes;
			//tsl::robin_map<gfx::VertexAttribute, VertexAttributeData> attributes;


			//std::vector<VertexAttribute> customAttributes;
			//std::vector<math::vec3> position;
			//std::vector<math::vec3> normal;
			//std::vector<unsigned int> index;
			//std::vector<unsigned int> boneID;
			//std::vector<float>
			unsigned int materialID; // material id stored in asset

			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(attributes, indices, materialID);
			//}
		};

		class Mesh {
		public:

			std::string name;
			std::vector<MeshPrimitive> primitives;

			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(name, primitives);
			//}
		};

		enum class PrimitiveError { Success, Error};

	}

	template<typename T>
	using PrimitiveResult = result::Result<T, error::Status<gfx::PrimitiveError>>;
}