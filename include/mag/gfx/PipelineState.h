#pragma once
#include <string>
#include "Mesh.h"
#include "Handles.h"

namespace mag {
	namespace gfx {

		/*
			This pipeline state class is used to described the renderer state for the draw command.
			The internal renderer doesn't know what a material is. Instead when we submit a draw command
			we include a pipeline state
		*/

		class GraphicsPipelineStateInitializer  {
		public:

			/*	
				Internal renderer pipeline state ID.
				Use this when you already have a pipeline object (API Specific)
				stored in the renderer.
			*/
			//unsigned int PipelineID;
			gfx::GraphicsPipelineHandle pipeline;
			/*
				Depth settings
			*/
			bool depthTest;
			/*
				Stencil settings
			*/

			/*
				Shader stage settings
			*/
			gfx::ShaderInfo shader;

		};

	}
}