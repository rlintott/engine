#pragma once
#include <string>
#include <mag/types/Byte.h>
#include <vector>
namespace mag {
	namespace gfx {
		class SamplerHandle {
		public:
			uint64_t value;
			bool operator==(const SamplerHandle &b) const {
				return value == b.value;
			}

			template<class Archive>
			void serialize(Archive & ar)
			{
				ar(value);
			}
		};

		struct SamplerHandleHasher {
			size_t operator()(const SamplerHandle & handle) const {
				return std::hash<uint64_t>()(handle.value);
			}
		};



	}
}