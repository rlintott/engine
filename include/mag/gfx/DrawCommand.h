#pragma once
#include "PipelineState.h"
#include "Mesh.h"
#include "ResourceSet.h"
#include <mag/utility/Hash.h>
namespace mag {
	namespace gfx {
		class DrawCommand {
		public:
			
			DrawCommand() {
				count = 0;
			}

			/*
				Engine representation of pipeline state.
				Contains settings to recreate pipeline state inside renderer.
			*/
			GraphicsPipelineHandle pipeline;

			/*
				Resource Bindings. Maps resources to shader inputs, like a description of a descriptor set.
			*/
			ResourceSetInitializer resourceSetInitializer;

			/*
				Mesh information. We have one draw command per primitive.
			*/ 
			MeshPrimitiveHandle primitive;

			/*
				The instance count.
			*/
			uint32_t count;

			/*
				Transform Information
			*/
			size_t transform;


			bool operator==(const DrawCommand & rhs) const {
				return (pipeline == rhs.pipeline && resourceSetInitializer == rhs.resourceSetInitializer && primitive == rhs.primitive);
			}

			size_t hash() const {
				return byteHash(this, sizeof(GraphicsPipelineHandle) + sizeof(ResourceSetInitializer) + sizeof(MeshPrimitiveHandle));
			}

		};


	}
}