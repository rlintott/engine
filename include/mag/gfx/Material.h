#pragma once
#include <string>
#include <vector>
#include <memory>
#include <unordered_map>
#include "Texture.h"
#include <mag/types/gfx.h>
#include "ResourceSet.h"
#include "Buffer.h"
#include <mag/types/Byte.h>
#include <stdexcept>
namespace mag {

	namespace gfx {

		struct MaterialHandle {
			uint64_t value;
			bool operator==(const MaterialHandle &b) const {
				return value == b.value;
			}

		};

		struct MaterialHandleHasher {
			size_t operator()(const MaterialHandle & handle) const {
				return std::hash<uint64_t>()(handle.value);
			}
		};

		struct MaterialInstanceHandle {
			uint64_t value;
			bool operator==(const MaterialInstanceHandle &b) const {
				return value == b.value;
			}
		};

		struct MaterialInstanceHandleHasher {
			size_t operator()(const MaterialInstanceHandle & handle) const {
				return std::hash<uint64_t>()(handle.value);
			}
		};


		enum RenderingMethod {
			Opaque,
			Translucent,
			Masked,
			Depth
		};

		enum DrawMode {
			Front,
			Back,
			Both
		};



		//enum TextureSlot {
		//	e0,
		//	e1,
		//	e2,
		//	e3,
		//	e4,
		//	e5,
		//	e6,
		//	e7,
		//	e8,
		//	e9,
		//	e10,
		//	e11,
		//	e12
		//};


		enum InterpolationQualifier {
			smooth,
			flat
		};

		class VertexInputAttribute {
		public:
			std::string name;
			//size_t size;
			DataType type;

			VertexInputAttribute(std::string name, gfx::DataType type) {
				this->name = name;
				this->type = type;
			}
		};

		class UBOInfo {
		public:
			unsigned int binding;
			unsigned int size;

			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(binding, size);
			//}
			//std::unordered_map<std::string, 
		};
		class ShaderInfo {
		public:
			/*
				Raw shader code.
			*/
			std::string shaderCode;

			/*
				SPIR-V byte code.
			*/
			std::vector<spvbyte> vertCode;
			std::vector<spvbyte> fragCode;

			/*
				Shader bindings.
			*/
			std::unordered_map<std::string, UBOInfo> uniformBuffers;
			std::unordered_map<std::string, unsigned int> textures;
			std::unordered_map<std::string, unsigned int> samplers;
			std::unordered_map<gfx::VertexAttribute, unsigned int> vertexAttributes;
			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(shaderCode, vertCode, fragCode, uniformBuffers, textures, samplers, vertexAttributes);
			//}
		};



		//// classes to hold material properties
		//class MaterialParameter {
		//public:
		//	// name of the parameter
		//	std::string name;

		//	DataType type;

		//	/*
		//	Size of material parameter data in bytes.
		//	*/
		//	size_t size() {
		//		switch (type) {
		//		case scalar:
		//			return sizeof(math::vec4);
		//		case vec2:
		//			return sizeof(math::vec4);
		//		case vec3:
		//			return sizeof(math::vec4);
		//		case vec4:
		//			return sizeof(math::vec4);
		//		default:
		//			return 0;
		//		}
		//	}

		//};

		/*
			Does not contain any instance data, this information will be stored in the Material class.
		*/
		//class Texture2DInput {
		//public:
		//	std::string name;
		//	TextureSlot slot;
		//};
		class MaterialParameterInfo {
		public:
			std::string name;
			gfx::DataType type;

			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(name, type);
			//}
		};
		/*
		Material needs lots of features.
		It describes the shading model (surface, unlit, simplelit (?), skin)
		This class represents an API agnostic spec for rendering the material in the engine.
		May include information such as shading model, rasterizer settings, (pretty much anything described in a vulkan pipeline object).

		Materials don't care what vertex format is given. If it's missing vertex data it gets supplied dummy data.
		If the mesh supplies inputs we don't need, we don't use them.

		*/
		class Material {
		public:

			Material();

			// name of material
			std::string name;



			// Face Culling
			DrawMode drawMode;

			// blend mode
			RenderingMethod method;

			// depth testing settings

			// stencil testing settings

			// shadow settings
			bool castShadows;
			bool receiveShadows;


			/*
				need some method (spir-v reflection?) to pull out the material parameters from the glsl shaders.
				implement later.
			*/

			ShaderInfo shaderInfo;
			//RendererMaterialPathHandle pathHandle;


			/*
				Material input definitions (per material instance).
			*/
			std::vector<MaterialParameterInfo> parameters;
			std::unordered_map<std::string, uint32_t> textureSlots;
			//std::vector<VertexInputAttribute> attributes;

			/*
				Material vertex attributes
			*/
			// assume we always have position
			bool vert_normal;
			bool vert_uv0;
			bool vert_tangent;

			/*
				
			*/

			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(name, drawMode, method, castShadows, receiveShadows, shaderInfo, parameters, textureSlots, vert_normal, vert_uv0, vert_tangent);
			//}


		};




		/*
			Classes for serialization of material parameters.
		*/

		class MaterialParameter {
		public:
			virtual ~MaterialParameter() {};
			//gfx::DataType type;
			virtual gfx::DataType getType() {
				throw std::runtime_error("\nAttempted to get type of base Material Parameter class!");
			};//return gfx::DataType::scalar; };

		};

		class MPColor: public MaterialParameter {
		public:

			//MPColor() { type = gfx::DataType::vec4; };
			gfx::DataType getType() override { return gfx::DataType::vec4; };
			float value[4];

			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(value);
			//}
		};


		/*
			Used to create a MaterialInstance object at runtime.
			Contains all info required to do that. 
		*/
		class MaterialInstanceCreateInfo {
		public:
			// name of the material instance to create (may become obsolete)
			std::string name;
			// name/identifier of the material
			std::string material;
			// name/id of asset package
			std::string package;
			
			// material parameters
			//std::unordered_map<std::string, std::shared_ptr<gfx::MaterialParameter>> parameters;
			std::unordered_map<std::string, std::string> texture2D;


			/*
			Serialization
			*/
			//template<class Archive>
			//	void serialize(Archive & ar)
			//	{
			//		ar(CEREAL_NVP(name), CEREAL_NVP(package), CEREAL_NVP(material), CEREAL_NVP(parameters), CEREAL_NVP(texture2D));
			//	}
		};

		class MaterialInstance {
		public:
			/*
				Data for loading.
			*/
			std::string name; 
			MaterialHandle materialHandle;
			ResourceSetInitializer resourceSetInitializer;
			//ResourceSetHandle resourceHandle;
			// some how need to store parameter data - (void *) type erasure?


			/*
				Runtime instance data.
			*/

			// uniform data
			std::vector<mag::byte> parameters;
			// texture data
			std::vector<Texture2DHandle> texture2D;

			// buffer information
			gfx::UniformBufferHandle uniformBuffer;




			//MaterialInstanceData instanceData;
			//std::unique_ptr<myth::byte[]> parameterData;

			// tells renderer to update data on GPU side
			//bool updated;

			//void Set(std::string name, math::vec3 & v) {
			//	size_t offset = 0;
			//	for (auto & parameter : instanceData.parameters) {
			//		if (parameter.name == name) {
			//			if (parameter.type == vec3) {
			//				// write at offset
			//				auto value = glm::vec4(v, 1.0);
			//				std::copy(&value.x, &value.x + 4, instanceData.values.get());
			//				return;
			//			}
			//		}
			//		offset += parameter.size();
			//	}
			//}

		};

		/*
			Class for storing material parameters that are generated when importing? maybe not needed
		*/
	}
}

//CEREAL_REGISTER_TYPE(myth::gfx::MPColor)
//CEREAL_REGISTER_POLYMORPHIC_RELATION(myth::gfx::MaterialParameter, myth::gfx::MPColor)