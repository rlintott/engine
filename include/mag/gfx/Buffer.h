#pragma once

namespace mag {
	namespace gfx {

		struct UniformBufferHandle {
			uint64_t value;
			bool operator==(const UniformBufferHandle & b) const {
				return value == b.value;
			}

			template<class Archive>
			void serialize(Archive & ar)
			{
				ar(value);
			}
		};

		struct UniformBufferHandleHasher {
			size_t operator()(const UniformBufferHandle & handle) const {
				return std::hash<uint64_t>()(handle.value);
			}
		};

		class TransformBufferHandle : public Handle<uint64_t> {};


		//struct TransformBufferHandle {
		//	uint64_t value;
		//	bool operator==(const TransformBufferHandle & b) const {
		//		return value == b.value;
		//	}

		//	template<class Archive>
		//	void serialize(Archive & ar)
		//	{
		//		ar(value);
		//	}
		//};

		//struct TransformBufferHandleHasher {
		//	size_t operator()(const TransformBufferHandle & handle) const {
		//		return std::hash<uint64_t>()(handle.value);
		//	}
		//};

	}
}