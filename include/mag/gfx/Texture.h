#pragma once
#include <string>
#include <mag/types/Byte.h>
#include <vector>
#include <mag/types/Handle.h>
namespace mag {
	namespace gfx {
		//class Texture2DHandle {
		//public:
		//	uint64_t value;
		//	bool operator==(const Texture2DHandle &b) const {
		//		return value == b.value;
		//	}

		//	template<class Archive>
		//	void serialize(Archive & ar)
		//	{
		//		ar(value);
		//	}
		//};

		//struct Texture2DHandleHasher {
		//	size_t operator()(const Texture2DHandle & handle) const {
		//		return std::hash<uint64_t>()(handle.value);
		//	}
		//};

		class Texture2DHandle : public Handle<uint64_t> {};

		/*
			Class used to store textures.
			Consumed by renderer to create actual texture on the GPU.
		*/
		class Texture2DAsset {
		public:
			std::string name;
			std::vector<mag::byte> data;
			unsigned int numChannels;
			unsigned int height;
			unsigned int width;

			//template<class Archive>
			//void serialize(Archive & ar)
			//{
			//	ar(name, data, numChannels, height, width);
			//}

		};



	}
}