#pragma once
#include <memory>
#include <functional>
#include <mag/FrameConstants.h>
namespace mag {

	class WindowSystem;
	class RenderSystem;
	class InputSystem;
	class JobSystem;
	class EntitySystem;
	class ResourceManager;
	// ECS
	class MeshSystem;
	class TransformSystem;


/**
 * @brief Base class, can be passed to mag::App. Initializes important game engine systems like rendering, jobs, and input.
 * 
 */
	class Game {
	protected:

		// systems & managers
		std::shared_ptr<JobSystem> job_system;
		std::shared_ptr<WindowSystem> window_system;
		std::shared_ptr<InputSystem> input_system;
		std::shared_ptr<EntitySystem> entity_system;
		std::shared_ptr<RenderSystem> render_system;

		// ECS
		std::shared_ptr<TransformSystem> transform_system;

		// methods
		std::function<void()> exit;

	public:

		Game();
		virtual ~Game();

		/**
		 * @brief Called internally by myth::App. Updates client engine systems according to fixed timestep. Do not override.
		 * 
		 * @param frameConstants A set of constants generated every fixed frame and modified every render frame.
		 */
		void engineFixedUpdate(FrameConstants frameConstants);

		/*
			Called internally by myth::App. Updates client rendering system. Do not override.
		*/

		/**
		 * @brief Internal render update function called by myth::App. Do not override.
		 * 
		 * @param frameConstants 
		 */
		void engineRenderUpdate(FrameConstants frameConstants);

		/*
			Fixed update function called by myth::App each timestep. Override to update custom game systems.
		*/

		/**
		 * @brief User defined function. Override to update custom systems at a fixed rate.
		 * 
		 * @param frameConstants 
		 */
		virtual void fixedUpdate(FrameConstants frameConstants);

		/**
		 * @brief User defined render update function called by myth::App. Override to update custom game render systems.
		 * 
		 * @param frameConstants 
		 */
		virtual void renderUpdate(FrameConstants frameConstants);

		/**
		 * @brief Set the Exit Function object. Do not use unless you are writing a replacement for myth::App.
		 * 
		 * @param function 
		 */
		void setExitFunction(std::function<void()> function);
	};
}