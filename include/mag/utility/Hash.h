#pragma once
#include <stddef.h>
namespace mag {
	/*
		Hashes len bytes at input to generate a 64-bit hash.
	*/
	size_t byteHash(const void* input, size_t len);
}