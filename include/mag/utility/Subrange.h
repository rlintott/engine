#pragma once
#include <iterator>
#include <vector>
namespace mag {

	template<class IteratorType>
	class IteratorSubrange {
	public:
		IteratorType begin;
		IteratorType end;
	};


	template<class IteratorType>
	std::vector<IteratorSubrange<IteratorType>> calculateSubranges(IteratorType begin, IteratorType end, size_t size, size_t numSubranges)
	{

		if (size > 0 && numSubranges > 0) {

			if (numSubranges >= size) {
				return {{begin, end}};
			}
			else {
				size_t maxBinSize = size / numSubranges;
				auto startIt = begin;
				auto endIt = begin;
				std::vector<IteratorSubrange<IteratorType>> subranges;
				for (size_t i = 0; i < numSubranges - 1; i++) {
					std::advance(endIt, maxBinSize);
					subranges.push_back({ startIt, endIt });
					std::advance(startIt, maxBinSize);
				}
				// for the last subrange
				subranges.push_back({ startIt, end });
				return subranges;
			}

		}

		IteratorSubrange<IteratorType> subrange;
		subrange.begin = begin;
		subrange.end = end;
		return { subrange };

	}

}