#pragma once
#include <iostream>
#include <functional>
#include <mag/Time.h>
#include <mag/FrameConstants.h>
#include <mag/log/Log.h>
/**
 * @brief Game Engine Framework
 * 
 */
namespace mag {

	/**
	 * @brief Application class which can execute at a fixed rate.
	 * 
	 * @tparam GameClass Your custom game class.
	 */
	template <typename GameClass>
	class App {
	private:
		std::unique_ptr<GameClass> game;
	public:

		App(): game(new GameClass())
		{
			log::info("App constructed!");
		}

		/**
		 * @brief Runs the app.
		 * 
		 * @param updateRate Specifies the number of fixed-timestep frames per second.
		 */
		void run(const unsigned int updateRate = 50)
		{
			/*
				Startup
			*/
			bool exit = false;

			/*
				Timer variables
			*/
			auto lastTime = Time::GetTime();
			double accumulator = 0.0;
			const double fixedDeltaTime = 1.0 / static_cast<double>(updateRate);
			constexpr double maxSimulationDelta = 1.0;

			/*
				Frame Constants Config
			*/
			FrameConstants frameConstants = { fixedDeltaTime, fixedDeltaTime, 0.0};

			/*
				Exit function
			*/
			std::function<void()> exitFunction = [&exit]() {
				exit = true;
			};

			game->setExitFunction(exitFunction);

			while (!exit) {

				// accumulate delta time
				auto newTime = mag::Time::GetTime();
				auto frameTime = newTime - lastTime;
				lastTime = newTime;
				accumulator += frameTime;

				// clamp accumulator
				if (accumulator > maxSimulationDelta) {
					accumulator = maxSimulationDelta;
				}

				// timestep integration
				while (accumulator > fixedDeltaTime) {
					game->engineFixedUpdate(frameConstants);
					game->fixedUpdate(frameConstants);
					accumulator -= fixedDeltaTime;
				}

				// 0.0-1.0 blend factor
				frameConstants.alpha = accumulator / fixedDeltaTime;

				game->renderUpdate(frameConstants);
				game->engineRenderUpdate(frameConstants);

			}
		}
	};
}