#pragma once
#include <glm/gtc/constants.hpp>
namespace mag {
	namespace constants {
		constexpr double pi = glm::pi<double>();
	}
}