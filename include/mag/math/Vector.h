#pragma once
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
//#include <glm/gtx/normal.hpp>
#include <glm/geometric.hpp>
#include <glm/gtx/norm.hpp>
namespace mag {
	using vec2 = glm::vec2;
	using vec3 = glm::vec3;
	using vec4 = glm::vec4;

	using dvec2 = glm::dvec2;
	using dvec3 = glm::dvec3;
	using dvec4 = glm::dvec4;

	using ivec2 = glm::ivec2;
	using ivec3 = glm::ivec3;
	using ivec4 = glm::ivec4;

	using uvec2 = glm::uvec2;
	using uvec3 = glm::uvec3;
	using uvec4 = glm::uvec4;

	using uint = uint32_t;

	namespace math {

		template<typename Vec>
		Vec normalize(const Vec& v) {
			return glm::normalize(v);
		}

		template<typename Vec>
		auto length(const Vec& v) {
			return glm::length(v);
		}

		template<typename Vec>
		auto length2(const Vec& v) {
			return glm::length2(v);
		}


	}

	//template<typename ... Args>
	//auto & length<Args> = glm::length<Args>;
	//template<typename ... Args>
	//auto & normalize<Args> = glm::normalize<Args>;
	//using length = glm::length;
	//using normalize = glm::normalize;
}