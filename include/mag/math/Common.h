#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
namespace mag {
	namespace math {

		template<typename T>
		auto get_ptr(const T& value) {
			return glm::value_ptr(value);
		}

		template<typename T, typename S>
		auto lerp(const T& first, const T& second, S alpha) {
			return glm::mix(first, second, alpha);
		}
	}
}