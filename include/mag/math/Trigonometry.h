#pragma once
#include <glm/glm.hpp>
namespace mag {
	namespace math {
		inline double sin(double angle_radians) {
			return glm::sin(angle_radians);
		}

		inline double cos(double angle_radians) {
			return glm::cos(angle_radians);
		}

		inline double tan(double angle_radians) {
			return glm::tan(angle_radians);
		}

		inline double asin(double val) {
			return glm::asin(val);
		}

		inline double acos(double val) {
			return glm::acos(val);
		}

		inline double atan(double val) {
			return glm::atan(val);
		}

	}
}