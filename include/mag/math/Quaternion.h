#pragma once
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
//#include <glm/gtx/norm.hpp>
#include <glm/gtc/quaternion.hpp>
namespace mag {

	using quat = glm::quat;
	using dquat = glm::dquat;

	namespace math {
		inline dquat to_quat(const dvec3& vec) {
			return glm::quatLookAtLH(vec, glm::dvec3(0, 1, 0));
		}

		template<typename T, typename S>
		auto slerp(const T& first, const T& second, S alpha) {
			return glm::slerp(first, second, alpha);
		}
	}

}