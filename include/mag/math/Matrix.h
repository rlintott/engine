#pragma once
#include <glm/glm.hpp>
#include <mag/math/Vector.h>
#include <glm/ext/matrix_transform.hpp>
#include <glm/ext/matrix_clip_space.hpp>
namespace mag {

	using mat3 = glm::mat3;
	using mat4 = glm::mat4;

	using dmat3 = glm::dmat3;
	using dmat4 = glm::dmat4;

	namespace math {

		template<typename Matrix, typename Vector>
		auto translate(const Matrix& matrix, const Vector& vector) {
			return glm::translate(matrix, vector);
		}

		template<typename Matrix, typename Vector>
		auto rotate(const Matrix& matrix, const Vector& vector) {
			return glm::rotate(matrix, vector);
		}

		template<typename Matrix, typename Vector>
		auto scale(const Matrix& matrix, const Vector& vector) {
			return glm::scale(matrix, vector);
		}

		inline dmat4 perspectiveVulkan(const double fov_radians, const double aspect_ratio, const double near_plane, const double far_plane) {
			return glm::perspectiveRH_ZO(fov_radians, aspect_ratio, far_plane, near_plane);
		}

		inline dmat4 InfinitePerspectiveVulkan(const double fov_radians, const double aspect_ratio, const double near_plane) {
			assert(glm::abs(aspect_ratio - std::numeric_limits<double>::epsilon()) > static_cast<double>(0));

			double const tanHalfFovy = tan(fov_radians / static_cast<double>(2));

			dmat4 Result(static_cast<double>(0));
			Result[0][0] = static_cast<double>(1) / (aspect_ratio * tanHalfFovy);
			Result[1][1] = static_cast<double>(1) / (tanHalfFovy);
			Result[2][2] = 0;// near_plane / (far_plane - near_plane);
			Result[2][3] = -static_cast<double>(1);
			Result[3][2] = static_cast<double>(near_plane);// -(near_plane * far_plane) / (near_plane - far_plane);
			return Result;
		}
		inline dmat4 viewVulkan(const dvec3& position, const dvec3& direction) {
			return glm::lookAt(position, position + direction, dvec3(0, -1, 0));
		}

	}
}