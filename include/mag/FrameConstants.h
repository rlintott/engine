#pragma once
namespace mag {
	/**
	 * @brief Per-frame time constants.
	 * 
	 */
	class FrameConstants {
	public:
		/**
		 * @brief The scaled fixed timestep in seconds.
		 * 
		 */
		double fixedDelta;
		/**
		 * @brief The unscaled fixed timestep in seconds.
		 * 
		 */
		double unscaledFixedDelta;
		/**
		 * @brief The blending factor [0-1].
		 * 
		 */
		double alpha;

	};
}