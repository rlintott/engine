#pragma once
#include <stdint.h>
namespace mag {
	namespace gfx {
		enum VertexAttribute {
			Position = 0,
			Normal = 1,
			UV0 = 2,
			Tangent = 3,
			Transform = 10,
			Count
		};
		//constexpr int maxAttributes = 4;
		constexpr unsigned int TransformLocation = 10;
		enum VertexLayout : uint64_t {
			NoneBit = 0,
			PositionBit = 1,
			NormalBit = 2,
			UV0Bit = 4,
			TangentBit = 8,

		};

		enum DataType {
			scalar,
			vec2,
			vec3,
			vec4,
			uint,
			uvec2,
			uvec3,
			uvec4,
			byte,
			ushort
		};
	}
}