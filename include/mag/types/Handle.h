#pragma once
#include <mag/containers/HandleManager.h>
namespace mag {

	//typedef uint64_t handle_id_type;
	template<typename T = uint64_t>
	class Handle {
		//friend class HandleManager<Handle<T>>;
	//private:
	public:

		T value;


		bool operator==(const Handle &b) const {
			return value == b.value;
		}

		size_t hash() const {
			return std::hash<T>()(value);
		}
	};


	template<typename T>
	struct HandleHasher {
		size_t operator()(const T & handle) const {
			return handle.hash();
		}
	};


}