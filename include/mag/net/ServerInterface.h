#pragma once
#include <mag/types/Handle.h>
#include <mag/net/Message.h>
#include <mag/FrameConstants.h>

namespace mag {
	namespace net {
		class ServerHandle : public Handle<uint64_t> {};
		class ClientHandle : public Handle<uint64_t> {};

		class ServerInterface {
		public:
			virtual ~ServerInterface() {};
			virtual void poll(const FrameConstants frame_constants) = 0;
			virtual void send(const ClientHandle client, const Message& message,  const Mode mode) = 0;
			virtual void broadcast(const Message& message) = 0;
			virtual void drop(const ClientHandle client) = 0;
		};
	}
}