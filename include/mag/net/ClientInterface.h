#pragma once
#include <mag/net/ServerInterface.h>
#include <mag/FrameConstants.h>
namespace mag {
	namespace net {
		class ClientInterface {
		public:
			virtual ~ClientInterface() {};

			virtual void connect(const ServerHandle server) = 0;
			virtual void disconnect() = 0;
			virtual void poll(const FrameConstants frame_constants) = 0;
			virtual void send(const Message& message, const Mode mode) = 0;
		};
	}
}