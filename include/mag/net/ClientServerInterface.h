#pragma once
#include <mag/net/Message.h>
#include <mag/types/Handle.h>

namespace mag {
	class FrameConstants;
	namespace net {
		class ServerHandle : public Handle<uint64_t> {};
		class ClientHandle : public Handle<uint64_t> {};

		enum class Mode {
			Reliable,
			Unreliable
		};

		/*
			Hybrid interface class. Some networking libraries use global memory which prevents separate instantiations for client and server classes.
		*/
		class ClientServerInterface {
		public:
			
			virtual ~ClientServerInterface() {};
			
			/*
				Check for new packets received.
			*/
			virtual void poll(const FrameConstants frame_constants) = 0;
			
			/*
				Send message to a client.
			*/
			virtual void send_client(const ClientHandle client, const Message& message, const Mode mode) = 0;
			
			/*
				Send message to a server.
			*/
			virtual void send_server(const Message& message, const Mode mode) = 0;

			/*
				Send message to all clients.
			*/
			virtual void broadcast(const Message& message, const Mode mode) = 0;

			/*
				Forefully disconnect client.
			*/
			virtual void drop(const ClientHandle client) = 0;

			/*
				Disconnect from server.
			*/
			virtual void disconnect() = 0;

		};
	}
}