#pragma once
#include <mag/net/ServerInterface.h>
//#include <mag/net/yojimbo/yojimbo.h>
#include <mag/log/Log.h>
// #include <yojimbo/yojimbo.h>
#include <mag/net/pc/Common.h>
namespace mag {
	namespace net {
		namespace pc {
			class Server : public ServerInterface {
			private:
				GameAdapter& adapter;
				std::unique_ptr<yojimbo::Server> server;
				double time;

			public:
				
				Server(GameAdapter& game_adapter) : adapter(game_adapter) {
					time = 0.0;
					GameConnectionConfig config;
					server = std::unique_ptr<yojimbo::Server>(new yojimbo::Server(yojimbo::GetDefaultAllocator(), privateKey, yojimbo::Address("127.0.0.1", 25000), config, adapter, time));
					server->Start(max_clients);

					if (!server->IsRunning()) {
						log::error("Yojimbo server is not running!");
					}
					else {
						log::debug("Yojimbo server is running!");
					}
				};

				~Server() {
					server->DisconnectAllClients();
					server->Stop();
				}

				virtual void poll(const FrameConstants frame_constants) override {
					time += frame_constants.fixedDelta;
					server->AdvanceTime(frame_constants.fixedDelta);
					server->ReceivePackets();
					server->SendPackets();
				};

				virtual void send(const ClientHandle client, const Message& message, const Mode mode) override {
				
				};

				virtual void broadcast(const Message& message) override {
				
				};
				
				virtual void drop(const ClientHandle client) override {
				
				};
			};
		}
	}
}