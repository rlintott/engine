#pragma once
#include <mag/net/yojimbo/yojimbo.h>
#include <mag/log/Log.h>
namespace mag {
	namespace net {

		static constexpr int key_bytes = 256;
		static constexpr int server_port = 25000;
		static constexpr int max_clients = 8;
		uint8_t privateKey[key_bytes];

		enum ConnectionChannel {
			Reliable,
			Unreliable
		};

		struct GameConnectionConfig : yojimbo::ClientServerConfig {
			GameConnectionConfig() {
				numChannels = 2;
				channel[ConnectionChannel::Reliable].type = yojimbo::CHANNEL_TYPE_RELIABLE_ORDERED;
				channel[ConnectionChannel::Unreliable].type = yojimbo::CHANNEL_TYPE_UNRELIABLE_UNORDERED;
			}
		};

		class GameAdapter : public yojimbo::Adapter {

		private:
			//std::shared_ptr<yojimbo::Server> m_server;
			static int log_func(const char* message, ...) {
				static constexpr int buffer_size = 512;
				char buffer[buffer_size];
				va_list args;
				va_start(args, message);
				vsnprintf(buffer, buffer_size, message, args);
				va_end(args);
				log::info(buffer);
				return 0;
			};
		public:
			//explicit GameAdapter(std::shared_ptr<yojimbo::Server> server) : m_server(server) {}

			GameAdapter() {
				if (!InitializeYojimbo()) {
					log::debug("Yojimbo failed to initialized!");
					return;
				}

				log::debug("Yojimbo successfully initialized!");

				yojimbo_set_printf_function(log_func);
				yojimbo_log_level(YOJIMBO_LOG_LEVEL_INFO);
			}

			~GameAdapter() {
				ShutdownYojimbo();
			}

			yojimbo::MessageFactory* CreateMessageFactory(yojimbo::Allocator& allocator) override {
				return YOJIMBO_NEW(allocator, yojimbo::MessageFactory, allocator, 2);
			}

			//void OnServerClientConnected(int clientIndex) override {
			//	if (m_server) {
			//		m_server->ClientConnected(clientIndex);
			//	}
			//}

			//void OnServerClientDisconnected(int clientIndex) override {
			//	if (m_server) {
			//		m_server->ClientDisconnected(clientIndex);
			//	}
			//}
		};
	}
}