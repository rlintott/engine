#pragma once
#include <mag/net/ClientInterface.h>
//#include <mag/net/yojimbo/yojimbo.h>
#include <mag/log/Log.h>
#include <mag/net/pc/Common.h>
#include <memory>
namespace mag {
	namespace net {
		namespace pc {
			class Client : public ClientInterface {
			private:
				GameAdapter& adapter;
				std::unique_ptr<yojimbo::Client> client;
				double time;
			public:

				Client(GameAdapter& game_adapter): adapter(game_adapter) {
					time = 0.0;
					GameConnectionConfig config;
					uint64_t client_id = 0;
					client = std::unique_ptr<yojimbo::Client>(new yojimbo::Client(yojimbo::GetDefaultAllocator(), yojimbo::Address("0.0.0.0"), config, adapter, time));

					yojimbo::Address serverAddress("127.0.0.1", server_port);

					client->InsecureConnect(privateKey, client_id, serverAddress);
				}

				~Client() {
					client->Disconnect();
				}

				virtual void connect(const ServerHandle server) override {
					
				};

				virtual void disconnect() override {
				
				};

				virtual void poll(const FrameConstants frame_constants) override {
					time += frame_constants.fixedDelta;
					client->AdvanceTime(time);
					client->ReceivePackets();
					client->SendPackets();
				};

				virtual void send(const Message& message, const Mode mode) override {
				
				};
			};
		}
	}
}