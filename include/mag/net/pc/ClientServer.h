#pragma once
#include <mag/net/ClientServerInterface.h>
#include "../yojimbo/yojimbo.h"
#include <mag/log/Log.h>
#include <mag/FrameConstants.h>
#include <mag/net/pc/Common.h>
namespace mag {
	namespace net {

		class ClientServer : public ClientServerInterface {
		private:
			std::shared_ptr<yojimbo::Server> server;
			std::shared_ptr<yojimbo::Client> client;
			GameAdapter adapter;
			static constexpr int key_bytes = 256;
			static constexpr int server_port = 25000;
			static constexpr int max_clients = 8;

			static int log_func(const char* message, ...) {
				static constexpr int buffer_size = 512;
				char buffer[buffer_size];
				va_list args;
				va_start(args, message);
				vsnprintf(buffer, buffer_size, message, args);
				va_end(args);
				log::info(buffer);
				return 0;
			};

		public:

			ClientServer() {

				if (!InitializeYojimbo()) {
					log::debug("Yojimbo failed to initialized!");
					return;
				}

				log::debug("Yojimbo successfully initialized!");
	
				yojimbo_set_printf_function(log_func);
				yojimbo_log_level(YOJIMBO_LOG_LEVEL_INFO);


				std::memset(privateKey, 0, key_bytes);
				double time = 0.0;
				GameConnectionConfig config;

				/*
					Server initialization
				*/
				server = std::shared_ptr<yojimbo::Server>(new yojimbo::Server(yojimbo::GetDefaultAllocator(), privateKey, yojimbo::Address("127.0.0.1", 25000), config, adapter, time));
				server->Start(max_clients);

				if (!server->IsRunning()) {
					log::error("Yojimbo server is not running!");
				}
				else {
					log::debug("Yojimbo server is running!");
				}

				/*
					Client initialization
				*/
				uint64_t client_id = 0;
				client = std::shared_ptr<yojimbo::Client>(new yojimbo::Client(yojimbo::GetDefaultAllocator(), yojimbo::Address("0.0.0.0"), config, adapter, time));

				yojimbo::Address serverAddress("127.0.0.1", server_port);

				client->InsecureConnect(privateKey, client_id, serverAddress);
			}

			~ClientServer() {
				client->Disconnect();
				client.reset();
				server->DisconnectAllClients();
				server.reset();
				ShutdownYojimbo();
			}

			virtual void poll(const FrameConstants frame_constants) override {
				client->AdvanceTime(frame_constants.time);
				client->ReceivePackets();
				client->SendPackets();

				server->AdvanceTime(frame_constants.time);
				server->ReceivePackets();
				server->SendPackets();
			};

			virtual void send_client(const ClientHandle client, const Message& message, const Mode mode) override {};

			virtual void send_server(const Message& message, const Mode mode) override {};

			virtual void broadcast(const Message& message, const Mode mode) override {};

			virtual void drop(const ClientHandle client) override {};

			virtual void disconnect() override {};

		};
	}
}