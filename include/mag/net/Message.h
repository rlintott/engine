#pragma once

namespace mag {
	namespace net {

		enum class Mode {
			Unreliable,
			Reliable
		};

		/*
			Inherit from this class and implement your own messaging class.
			Possible implementations may use a byte array or polymorphic serialization for various message data structures.
		*/
		class Message {
		public:
			virtual ~Message() {};
		};
	}
}