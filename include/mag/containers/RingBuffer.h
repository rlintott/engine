#pragma once
#include <array>
#include <atomic>
namespace mag {

	// Single Producer - Single Consumer Atomic Ring Buffer
	template <class T, size_t size> class RingBuffer {
		static_assert(size > 2);
	private:
		std::atomic<size_t> write_index;
		std::atomic<size_t> read_index;
		std::array<T, size> data;

	public:

		RingBuffer() {
			write_index = 0;
			read_index = 0;
		}



		void push(T & value) {
			data[write_index] = value;
			if ((write_index + 1) % size != read_index) {
				write_index = (write_index + 1) % size;
			}
		}


		T read() {
			auto retval = data[read_index];
			if ((read_index + 1) % size != write_index) {
				read_index = (read_index + 1) % size;
			}

			return retval;
		}


		// returns true if next push will overwrite front value
		bool full() {
			return (write_index == read_index);
		}


	};

}