# Contributing

## General Info
Currently I am working on this framework in my spare time to use for some personal projects. Those projects determine which features I implement and prioritize. If you are interested in contributing to a feature or patch, or have any questions, feel free to reach out.
## Coding Style
This project was started before a coding style was chosen. Gradually it will be refactored for some consistency.
In general:
### Types
- Class names and enumerated types can use `PascalCase`.
### Variables
- Use `snake_case`.
### Functions
- Use `camelCase` where the first letter is lower case.
- Parameters and signatures should be const whenever possible.
- Prefer working with references over pointers.
### Namespaces
- Namespace everything under `mag`.
- Short length, lowercase, and letters only. If name is made of multiple words, try rewriting as an acronym or abbreviation instead.
- Nested namespaces, definitions, and constants should be indented according to their level.
- Avoid nesting more than 3 namespaces. One to two levels is preferred.
- Avoid `using namespace`.
### Other
- Minimize use of exceptions. Use default values, optionals, or result/error objects instead. There are Result and Error types available in the library.
- Minimize use of macros if possible.
## Documenting
Please refer [here](BUILDING.MD) for documentation generation instructions.
If you are using VS Code, [this extension](https://marketplace.visualstudio.com/items?itemName=cschlosser.doxdocgen) handles autocomplete for the doxygen syntax.