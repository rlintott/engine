# MAG Toolkit
### Game Engine Framework
![MAG Toolkit](https://protoframe.dev/content/images/2019/07/magtoolkit.png)
## What is it?
MAG Toolkit is a work-in-progress framework consisting of lightweight systems that can be used to develop games in C++ using the Vulkan API.
It is not currently suitable for use in any production applications. There is no editor, but one could be built on top of the framework.
Currently, the framework can be built and run on Windows and Linux. Mac OS has not been tested.
I aim to develop this framework along side some personal projects.

If you are already developing your own engine, there are ways to [alias](https://en.cppreference.com/w/cpp/language/namespace) the namespaces.
## Features
*  Input System
*  Multithreaded Job Graph System
*  Dynamic Mesh Instancing
*  Material System
*  Asset Conversion Tool
## Design Considerations
- Minimize compile times
- Minimize code required when adding support for a new platform

For building and documentation instructions see [here](BUILDING.md).